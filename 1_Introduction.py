#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import inspect
import os

# Path to current file
path = inspect.getfile(inspect.currentframe())
# Path to directory that file is in
directory = os.path.realpath(os.path.dirname(path))
# Change directory 
os.chdir(directory)

import thesisutils as tu

#%% One Event
# equation for a line
def line(x,m,b):
    return m*x+b

path = 'filepath/Lab 2 - Keyser/ANA_03_Artemis/7.1/DNA/-0.1_-0.05_(1000,50)/Unfolded_w_identifable_peaks/'

eventno = 27

df = tu.eventLoader(path,eventno)

p_fill_l = 830
p_fill_r = 906

arrow_origin_x = 100   # us
arrow_origin_y = -0.35 # nA
arrow_up       = 0.2   # nA
arrow_right    = 300   # us

offx = 25   # needed to get arrows to overlap
offy = 0.01 # needed to get arrows to overlap


xs = np.arange(p_fill_l,p_fill_r)

# calculate slope
m = (df[p_fill_r]-df[p_fill_l])/(p_fill_r-p_fill_l)
# calculate y-intercept
b = df[p_fill_l]-m*p_fill_l

fig = plt.figure(figsize=(2.25,1), dpi=300)
plt.plot(df, color = tu.palette[3],linewidth=0.5)
ax = plt.gca()
ax.fill_between(xs,df[p_fill_l:p_fill_r],line(xs,m,b),color = tu.palette[2])
ax.axis('off')


#ax.arrow(arrow_origin_x,arrow_origin_y,0,arrow_up, length_includes_head =True)
ax.annotate('', xy=(arrow_origin_x, arrow_origin_y-offy), xycoords='data', xytext=(arrow_origin_x, arrow_origin_y-offy+arrow_up), 
                        arrowprops=dict(arrowstyle="<-", color='k',linewidth=1.5,relpos=(0,0)))
ax.annotate(f'{arrow_up} nA', xy=(arrow_origin_x+arrow_right/4, arrow_origin_y+arrow_up/2), xycoords='data',
            fontsize = 8)
ax.annotate('', xy=(arrow_origin_x-offx, arrow_origin_y), xycoords='data', xytext=(arrow_origin_x+arrow_right-offx, arrow_origin_y), 
                        arrowprops=dict(arrowstyle="<-", color='k',linewidth=1.5, relpos=(0,0)))
ax.annotate(f'{arrow_right} us', xy=(arrow_origin_x+arrow_right, arrow_origin_y-offy), xycoords='data',
            fontsize = 8)



#plt.savefig('filepath/Lab 2 - Keyser/MPhil Thesis/1-1xTrace.png', dpi=300)

#%% 4x Trace

def line(x,m,b):
    return m*x+b

path = 'filepath/Lab 2 - Keyser/ANA_03_Artemis/7.1/DNA/-0.1_-0.05_(1000,50)/Unfolded_w_identifable_peaks/'

eventno = 27

df = tu.eventLoader(path,eventno)

# How far to plot on each axis
ax0_xmax = 140
ax1_xmax = 500
ax2_xmax = 906

e_fill_l = 145
e_fill_r = 1351 # 1200, 1351

e_xs = np.arange(e_fill_l,e_fill_r)
# calculate slope
e_m = (df[e_fill_r]-df[e_fill_l])/(e_fill_r-e_fill_l)
# calculate y-intercept
e_b = df[e_fill_l]-e_m*e_fill_l

# Peak bounds to fill in
p_fill_l = 830
p_fill_r = 906
min_idx = df[p_fill_l:p_fill_r].idxmin()
max_idx = df[p_fill_l:p_fill_r].idxmax()

xs = np.arange(p_fill_l,p_fill_r)
# calculate slope
m = (df[p_fill_r]-df[p_fill_l])/(p_fill_r-p_fill_l)
# calculate y-intercept
b = df[p_fill_l]-m*p_fill_l

# Arrow Paremeters
arrow_origin_x = 100   # us
arrow_origin_y = -0.35 # nA
arrow_up       = 0.2   # nA
arrow_right    = 300   # us

offx = 15   # needed to get arrows to overlap
offy = 0.01 # needed to get arrows to overlap

# Create figure 
fig=plt.figure(figsize=(6.26,6.26), dpi=300)
# Create container
#gs = gridspec.GridSpec(4,2, hspace = 0.2, left = 0.01, right = 0.99, height_ratios=(1,2,1,2))
gs = gridspec.GridSpec(4,2, hspace = 0.2, left = 0.01, right = 0.99, bottom = 0.3, height_ratios=(1,2,1,2))
# Add axes to containers
# Current Traces
ax0 = fig.add_subplot(gs[1,0])
ax1 = fig.add_subplot(gs[1,1])
ax2 = fig.add_subplot(gs[3,0])
ax3 = fig.add_subplot(gs[3,1])
# Schematic
ax4 = fig.add_subplot(gs[0,0])
ax5 = fig.add_subplot(gs[0,1])
ax6 = fig.add_subplot(gs[2,0])
ax7 = fig.add_subplot(gs[2,1])
# Peak Depth
ax8 = fig.add_axes([ax7.get_position().xmin, 0, 0.3, 0.2])
#ax8 = fig.add_subplot(gs[4,0:])

# Set up current trace axes
for ax in [ax0,ax1,ax2,ax3]:
    ax.set_ylim(-0.42,0.05)
    ax.set_xlim(0,len(df))
    ax.axis('off')
    
# Plot current traces
ax0.plot(df[0:ax0_xmax], color = tu.palette[3])
ax1.plot(df[0:ax1_xmax], color = tu.palette[3])
ax2.plot(df[0:ax2_xmax], color = tu.palette[3])
ax3.plot(df, color = tu.palette[3])
# Plot ECD
ax3.fill_between(e_xs,df[e_fill_l:e_fill_r],np.ones(e_fill_r-e_fill_l)*df[e_fill_l],color = tu.palette[2])
#ax3.fill_between(e_xs,df[e_fill_l:e_fill_r],line(e_xs,e_m,e_b),color = tu.palette[2])
#ax3.fill_between(xs,df[p_fill_l:p_fill_r],line(xs,m,b),color = tu.palette[2])

# Plot primary current drop
ax1.annotate('', xy=(200, df[200]), xycoords='data', xytext=(200, 0.05), 
             arrowprops=dict(arrowstyle="<->", color='k',linewidth=1.5, relpos=(0,0)))
ax1.annotate('Primary Current Drop', xy=(300, -0.05), xycoords='data', fontsize = 8)
# Plot secondary current drop
ax2.annotate('', xy=(min_idx, df[min_idx]), xycoords='data', xytext=(min_idx, line(min_idx,m,b)+0.03), 
             arrowprops=dict(arrowstyle="<->", color='k',linewidth=1.5, relpos=(0,0)))
ax2.annotate('Secondary \nCurrent Drop', xy=(max_idx+120, (df[min_idx]+df[max_idx])/2), xycoords='data', fontsize = 8)
# Plot translocation time
ax3.annotate('', xy=(150, -0.06), xycoords='data', xytext=(1160, -0.06), 
             arrowprops=dict(arrowstyle="<->", color='k',linewidth=1.5, relpos=(0,0)))
ax3.annotate('Translocation Time', xy=(len(df)/2,-0.04), xycoords='data', horizontalalignment = 'center', fontsize = 8)
# Plot scale bars
for ax in [ax3]:
    ax.annotate('', xy=(arrow_origin_x, arrow_origin_y-offy), xycoords='data', xytext=(arrow_origin_x, arrow_origin_y-offy+arrow_up), 
                            arrowprops=dict(arrowstyle="<-", color='k',linewidth=1.5,relpos=(0,0)))
    ax.annotate(f'{arrow_up} nA', xy=(arrow_origin_x+arrow_right/4, arrow_origin_y+arrow_up/2), xycoords='data',
                fontsize = 8)
    ax.annotate('', xy=(arrow_origin_x-offx, arrow_origin_y), xycoords='data', xytext=(arrow_origin_x+arrow_right-offx, arrow_origin_y), 
                            arrowprops=dict(arrowstyle="<-", color='k',linewidth=1.5, relpos=(0,0)))
    ax.annotate(f'{arrow_right} us', xy=(arrow_origin_x+arrow_right, arrow_origin_y-offy), xycoords='data',
                fontsize = 8)
# Annotate peaks    
ax3.annotate('4',(0.615,-0.1), xycoords = 'axes fraction', fontsize = 8)
ax3.annotate('3',(0.685,-0.1), xycoords = 'axes fraction', fontsize = 8)
ax3.annotate('2',(0.735,-0.1), xycoords = 'axes fraction', fontsize = 8)
ax3.annotate('1',(0.795,-0.1), xycoords = 'axes fraction', fontsize = 8)
    
    
ax7.annotate('', xy=(0.5,0.95), xycoords='axes fraction', xytext=(0.2,0.95), 
             arrowprops=dict(arrowstyle="<-", color='k',linewidth=1.5, relpos=(0,0)))
ax7.annotate('Translocation Direction', xy=(0.2,1.1), xycoords='axes fraction',
            fontsize = 8)


# Tag locations
array = [851,930,1009,1087]
markersize = [i*4 for i in [4,3,2,1]]

text = ['A.','B.','C.','D.']

# Set up schematic axes
for idx,ax in enumerate([ax4,ax5,ax6,ax7]):
    ax.annotate(text[idx],(0,0.9), xycoords = 'axes fraction',
                fontsize = 9, fontweight = 'bold')
    tu.setup(ax,0,len(df))
    for idx,val in enumerate(array):
        ax.plot([val,val],[0,0.25],'k', linewidth = 0.5)
        ax.plot(val,0.25, marker = '.', markersize = markersize[idx],  color = tu.palette[3])
        
width   = p_fill_r-p_fill_l
scale   = 2



ax8.plot(df[p_fill_l:p_fill_r], color = tu.palette[3])
ax8.fill_between(xs,df[p_fill_l:p_fill_r],line(xs,m,b),color = tu.palette[2])
ax8.set_xlim(p_fill_l-scale*width,p_fill_r+scale*width)
ax8.annotate('', xy=(min_idx, df[min_idx]), xycoords='data', xytext=(min_idx, line(min_idx,m,b)+0.01), 
             arrowprops=dict(arrowstyle="<->", color='k',linewidth=1.5, relpos=(0,0)))
ax8.annotate('Peak Depth', xy=(max_idx+80, (df[min_idx]+df[max_idx])/2), xycoords='data')
ax8.annotate('E.',(0,1.05), xycoords = 'axes fraction',
            fontsize = 9, fontweight = 'bold')
#ax8.set_ylim(df[min_idx],df[max_idx])
ax8.axis('off')
    

plt.savefig('filepath/Lab 2 - Keyser/MPhil Thesis/1-5xTrace-2.png', dpi=300)

#%%
# Create figure 
fig=plt.figure(figsize=(6.26,4), dpi=300)
# Create container
gs = gridspec.GridSpec(4,2, left = 0.01, right = 0.99)
# Add axes to containers
ax0 = fig.add_subplot(gs[0,0])
ax1 = fig.add_subplot(gs[0,1])
ax2 = fig.add_subplot(gs[1,0])
ax3 = fig.add_subplot(gs[1,1])
ax4 = fig.add_subplot(gs[2,0])
ax5 = fig.add_subplot(gs[2,1])
ax6 = fig.add_subplot(gs[3,0])
ax7 = fig.add_subplot(gs[3,1])

# Label axes
texts = ['A.','B.','C.','D.']
for idx,ax in enumerate([ax0,ax2,ax4,ax6]):
    ax.annotate(texts[idx],(0,1.05), xycoords = 'axes fraction',
                fontsize = 9, fontweight = 'bold')
    ax.axis('off')


fold_path = 'filepath/Lab 2 - Keyser/ANA_01_Filip Ends Analysis/-0.1_-0.05_(400,50)_20210803_3/'
# Plot events
tu.current_time(tu.eventLoader(fold_path,7), ax1,50,0.15,6,3)
tu.current_time(tu.eventLoader(fold_path,27),ax3,50,0.15,6,3, fracxoff=0.5)
tu.current_time(tu.eventLoader(fold_path,11),ax5,50,0.15,6,3)
tu.current_time(tu.eventLoader(fold_path,32),ax7,50,0.15,6,3,fracxoff=0.5)

plt.savefig('filepath/Lab 2 - Keyser/MPhil Thesis/1-Difficulties.png', dpi=300)