#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.gridspec as gridspec
import numpy as np
import inspect
import os

# Path to current file
path = inspect.getfile(inspect.currentframe())
# Path to directory that file is in
directory = os.path.realpath(os.path.dirname(path))
# Change directory 
os.chdir(directory)

import thesisutils as tu

#%% Dot Plot

def setup_y(ax,offset,frag_len):
    ax.spines['top'].set_color('none')
    ax.spines['bottom'].set_color('none')
    ax.xaxis.set_major_locator(ticker.NullLocator())
    ax.spines['left'].set_color('none')
    ax.yaxis.set_ticks_position('right')
    ax.set_ylim(0, frag_len+offset)
    ax.set_xlim(0, 1)
    ax.patch.set_alpha(0.0)
    ax.yaxis.set_major_locator(ticker.NullLocator())

# Create figure 
fig = plt.figure(figsize=(5,5), dpi=300)
gs  = fig.add_gridspec(2, 2,  width_ratios=(1, 4), height_ratios=(1, 4),
                      left=0.1, right=0.9, bottom=0.1, top=0.9,
                      wspace=0.05, hspace=0.05)
# Create container
ax0 = fig.add_subplot(gs[1,1])
ax1 = fig.add_subplot(gs[0,1])
ax2 = fig.add_subplot(gs[1,0])

tu.setup(ax1,0,17)
setup_y(ax2,0,17)

x_peaks = np.array([0,0,1,1,0,1,0,0,0,0,1,0,0,0,0,1,1])

y_peaks = np.array([1,1,1,1,0,0,0,0,1,0,0,0,0,1,1,0,0])

y_peaks = y_peaks[::-1]

xs = np.arange(0,len(x_peaks))
ys = np.arange(0,len(y_peaks))

# Hardcoded offset due to dots and text differences in alignemnt
xoff = 0.15
yoff = 0.22

for x in xs:
    for y in ys:
        if x_peaks[x] == 1 and y_peaks[y] == 1:
            ax0.annotate('1',(x,y))
        else:
            ax0.annotate('0',(x,y))
         
            
        # Plot peaks 
        if x_peaks[x] == 1:
            ax1.plot(x+xoff,0.2, marker = '.', color = tu.palette[3])
            
        if y_peaks[y] == 1:
            ax2.plot(0.8,y+yoff, marker = '.', color = tu.palette[3])
        
            
# Fix axes to be correct length
ax0.set_xlim(min(xs),max(xs)+1)
ax0.set_ylim(min(ys),max(ys)+1)

# Bandwidth
bw = 3
ax0.fill_between([0,len(xs)],[len(xs)+bw,bw],[len(xs),0], color = tu.palette[2])

ax0.axis('off')
ax1.annotate("Reference Peaks",(0.5,0.5),xycoords = 'axes fraction',
             fontsize = 8, horizontalalignment = 'center')
ax2.annotate("Input Peaks",(0.5,0.5),xycoords = 'axes fraction',
             fontsize = 8, rotation = 90, verticalalignment = 'center')


plt.savefig('filepath/Lab 2 - Keyser/MPhil Thesis/7-DPMatrix.png', dpi=300)

#%%

# this assumes chance of deleting second peak or inserting b/n first and second is small

frag_len = 10000

untransformed = np.array([1000,2000,3000,6000,8000,8500])

# Add an extra point
trans_added = np.concatenate((np.array([500]),untransformed))

trans_missi = untransformed[1:]

n_lines = 6

# Create figure 
fig=plt.figure(figsize=(6.26,n_lines*0.7), dpi=300)
# Create container
gs = gridspec.GridSpec(n_lines,1, hspace = 0.5, left = 0, right = 1)
# Add axes to containers
ax_dict = {'ax'+str(i):fig.add_subplot(gs[i,0]) for i in range(n_lines)}

for i in ax_dict:
    tu.setup(ax_dict[i],0,frag_len)

palette = [8,8,8,8,8,8]

texts   = ['A. First Peak Read Accurately',
           'B. Extra Peak Inserted',
           'C. First Peak Deleted']

for idx,val in enumerate([untransformed,untransformed,untransformed,
                          trans_added,untransformed,trans_missi]):
    for i in val:
        ax_dict['ax'+str(idx)].plot(i,0.25, marker = '.', color = tu.palette[palette[idx]])

for idx,val in enumerate([0,2,4]):
    ax_dict['ax'+str(val)].annotate(texts[idx],(0,0.5), xycoords = 'axes fraction',
                                    fontsize = 9, fontweight = 'bold')
    
    
# Draw Arrows
ax_dict['ax1'].annotate('', xy=(0.2, 0.3), xycoords='axes fraction', xytext=(0.1, 1.3), 
                        arrowprops=dict(arrowstyle="<-", color=tu.palette[5],linewidth=1.5,
                                        connectionstyle="angle,angleA=90,angleB=-10"))  
ax_dict['ax1'].annotate('', xy=(0.2, 0.3), xycoords='axes fraction', xytext=(0.2, 1.3), 
                        arrowprops=dict(arrowstyle="<-", color=tu.palette[3],linewidth=1.5,
                                        connectionstyle="angle,angleA=90,angleB=-20"))  
ax_dict['ax1'].annotate('', xy=(0.2, 0.3), xycoords='axes fraction', xytext=(0.3, 1.3), 
                        arrowprops=dict(arrowstyle="<-", color=tu.palette[5],linewidth=1.5,
                                        connectionstyle="angle,angleA=90,angleB=10"))
ax_dict['ax1'].annotate('', xy=(0.8, 0.3), xycoords='axes fraction', xytext=(0.6, 1.3), 
                        arrowprops=dict(arrowstyle="<-", color=tu.palette[5],linewidth=1.5, 
                                        connectionstyle="angle,angleA=90,angleB=-10"))      
ax_dict['ax1'].annotate('', xy=(0.8, 0.3), xycoords='axes fraction', xytext=(0.8, 1.3), 
                        arrowprops=dict(arrowstyle="<-", color=tu.palette[3],linewidth=1.5, 
                                        connectionstyle="angle,angleA=90,angleB=10"))  
ax_dict['ax1'].annotate('', xy=(0.8, 0.3), xycoords='axes fraction', xytext=(0.85, 1.3), 
                        arrowprops=dict(arrowstyle="<-", color=tu.palette[5],linewidth=1.5, 
                                        connectionstyle="angle,angleA=90,angleB=10"))   


ax_dict['ax3'].annotate('', xy=(0.1, 0.3), xycoords='axes fraction', xytext=(0.1, 1.3), 
                        arrowprops=dict(arrowstyle="<-", color=tu.palette[3],linewidth=1.5,
                                        connectionstyle="angle,angleA=90,angleB=10"))
ax_dict['ax3'].annotate('', xy=(0.1, 0.3), xycoords='axes fraction', xytext=(0.2, 1.3), 
                        arrowprops=dict(arrowstyle="<-", color=tu.palette[5],linewidth=1.5,
                                        connectionstyle="angle,angleA=90,angleB=15"))  
ax_dict['ax3'].annotate('', xy=(0.1, 0.3), xycoords='axes fraction', xytext=(0.3, 1.3), 
                        arrowprops=dict(arrowstyle="<-", color=tu.palette[5],linewidth=1.5,
                        connectionstyle="angle,angleA=90,angleB=10"))     
ax_dict['ax3'].annotate('', xy=(0.8, 0.3), xycoords='axes fraction', xytext=(0.6, 1.3), 
                        arrowprops=dict(arrowstyle="<-", color=tu.palette[5],linewidth=1.5, 
                                        connectionstyle="angle,angleA=90,angleB=-10"))      
ax_dict['ax3'].annotate('', xy=(0.8, 0.3), xycoords='axes fraction', xytext=(0.8, 1.3), 
                        arrowprops=dict(arrowstyle="<-", color=tu.palette[3],linewidth=1.5, 
                                        connectionstyle="angle,angleA=90,angleB=10"))  
ax_dict['ax3'].annotate('', xy=(0.8, 0.3), xycoords='axes fraction', xytext=(0.85, 1.3), 
                        arrowprops=dict(arrowstyle="<-", color=tu.palette[5],linewidth=1.5, 
                                        connectionstyle="angle,angleA=90,angleB=10"))    
                       

ax_dict['ax5'].annotate('', xy=(0.3, 0.3), xycoords='axes fraction', xytext=(0.1, 1.3), 
                        arrowprops=dict(arrowstyle="<-", color=tu.palette[5],linewidth=1.5,
                                        connectionstyle="angle,angleA=90,angleB=-10")) 
ax_dict['ax5'].annotate('', xy=(0.3, 0.3), xycoords='axes fraction', xytext=(0.2, 1.3), 
                        arrowprops=dict(arrowstyle="<-", color=tu.palette[5],linewidth=1.5,
                                        connectionstyle="angle,angleA=90,angleB=-15"))   
ax_dict['ax5'].annotate('', xy=(0.3, 0.3), xycoords='axes fraction', xytext=(0.3, 1.3), 
                        arrowprops=dict(arrowstyle="<-", color=tu.palette[3],linewidth=1.5,
                        connectionstyle="angle,angleA=90,angleB=-10"))  
ax_dict['ax5'].annotate('', xy=(0.8, 0.3), xycoords='axes fraction', xytext=(0.6, 1.3), 
                        arrowprops=dict(arrowstyle="<-", color=tu.palette[5],linewidth=1.5, 
                                        connectionstyle="angle,angleA=90,angleB=-10"))      
ax_dict['ax5'].annotate('', xy=(0.8, 0.3), xycoords='axes fraction', xytext=(0.8, 1.3), 
                        arrowprops=dict(arrowstyle="<-", color=tu.palette[3],linewidth=1.5, 
                                        connectionstyle="angle,angleA=90,angleB=10"))  
ax_dict['ax5'].annotate('', xy=(0.8, 0.3), xycoords='axes fraction', xytext=(0.85, 1.3), 
                        arrowprops=dict(arrowstyle="<-", color=tu.palette[5],linewidth=1.5, 
                                        connectionstyle="angle,angleA=90,angleB=10"))       
                      
#plt.savefig('filepath/Lab 2 - Keyser/MPhil Thesis/7-FSFirst.png', dpi=300)
#%% Fiduciary Scan
# can only detect one addition or subtraction
def scaling(ref_array, ref_pts, inp_array):

    fid_delta = inp_array[-2] - inp_array[1]
    ref_delta = ref_array[ref_pts[1]] - ref_array[ref_pts[0]]
    
    scaled_array = inp_array * ref_delta/fid_delta
    #print(f'{ref_delta/fid_delta}')
    scaled_array += ref_array[ref_pts[0]]-scaled_array[1]
    #print(scaled_array[-2])
    
    
    return scaled_array
    
frag_len = 10000

untransformed = np.array([1000,2000,3000,6000,8000,8500])

# Add an extra point
trans_added = np.concatenate((np.array([500]),untransformed))
trans_added_scaled0 = scaling(untransformed,(0,-2),trans_added)
trans_added_scaled1 = scaling(untransformed,(1,-2),trans_added)
trans_added_scaled2 = scaling(untransformed,(2,-2),trans_added)
trans_added_scaled3 = scaling(untransformed,(1,-1),trans_added)
trans_added_scaled4 = scaling(untransformed,(1,-2),trans_added)
trans_added_scaled5 = scaling(untransformed,(1,-3),trans_added)

# Miss the first peak
trans_missi = untransformed[1:]
trans_missi_scaled0 = scaling(untransformed,(0,-2),trans_missi)
trans_missi_scaled1 = scaling(untransformed,(1,-2),trans_missi)
trans_missi_scaled2 = scaling(untransformed,(2,-2),trans_missi)
trans_missi_scaled3 = scaling(untransformed,(1,-1),trans_missi)
trans_missi_scaled4 = scaling(untransformed,(1,-2),trans_missi)
trans_missi_scaled5 = scaling(untransformed,(1,-3),trans_missi)

# Plotting
n_lines = 8

# Create figure 
fig=plt.figure(figsize=(6.26,n_lines/2*0.7), dpi=300)
# Create container
gs = gridspec.GridSpec(n_lines,2, hspace = 0.5, left = 0, right = 1)
# Add axes to containers
#ax_dict = {'ax'+str(i):fig.add_subplot(gs[i,0]) for i in range(n_lines)}
ax_dict = {'ax'+str(i):fig.add_subplot(gs[i%n_lines,i//n_lines]) for i in range(n_lines*2)}


for i in ax_dict:
    tu.setup(ax_dict[i],0,frag_len)
    
palette = [8,
           2,3,2,2,2,2,2,
           8,
           4,4,4,5,4,4,4]

texts   = ['A1.',
           'B1.',
           'C1.',
           'D1.',
           'E1.',
           'F1.',
           'G1.',
           'H1.',
           'A2.',
           'B2.',
           'C2.',
           'D2.',
           'E2.',
           'F2.',
           'G2.',
           'H2.',]

for idx,val in enumerate([untransformed,
                          trans_added,
                          trans_added_scaled0,
                          trans_added_scaled1,
                          trans_added_scaled2,
                          trans_added_scaled3,
                          trans_added_scaled4,
                          trans_added_scaled5,
                          untransformed,
                          trans_missi,
                          trans_missi_scaled0,
                          trans_missi_scaled1,
                          trans_missi_scaled2,
                          trans_missi_scaled3,
                          trans_missi_scaled4,
                          trans_missi_scaled5]):
    for i in val:
        ax_dict['ax'+str(idx)].plot(i,0.25, marker = '.', color = tu.palette[palette[idx]])
        ax_dict['ax'+str(idx)].annotate(texts[idx],(0,0.5), xycoords = 'axes fraction',
                                        fontsize = 9, fontweight = 'bold')


#plt.savefig('filepath/Lab 2 - Keyser/MPhil Thesis/7-FSAlign.png', dpi=300)
#%% Fiduciary Scan

# Create figure 
fig = plt.figure(figsize=(5,5), dpi=300)
gs  = fig.add_gridspec(2, 2,  width_ratios=(1, 4), height_ratios=(1, 4),
                      left=0.1, right=0.9, bottom=0.1, top=0.9,
                      wspace=0.05, hspace=0.05)

# Create container
ax0 = fig.add_subplot(gs[1,1])
ax1 = fig.add_subplot(gs[0,1])
ax2 = fig.add_subplot(gs[1,0])

tu.setup(ax1,10,tu.frag_len)
setup_y(ax2,10,tu.frag_len)

ax0.set_xlim(0,frag_len)
ax0.set_ylim(0,frag_len)

ax0.axis('off')
#ax0.set_xticks(np.arange(0,frag_len,2000),['']*5)
#ax0.set_yticks(np.arange(0,frag_len,2000),['']*5)


tl_origin = -trans_added+frag_len

offset = 400 # for drawing bandwidth

for i in untransformed:
    for j in tl_origin:
        ax0.plot(i,j,'.',color = tu.palette[3])
        ax0.plot([untransformed[0],untransformed[-1]],
                 [tl_origin[1]+offset,tl_origin[-1]+offset],color = tu.palette[2])
        ax0.plot([untransformed[0],untransformed[-1]],
                 [tl_origin[1]-offset,tl_origin[-1]-offset],color = tu.palette[2])
        ax0.fill_between([untransformed[0],untransformed[-1]],
                         [tl_origin[1]+offset,tl_origin[-1]+offset],
                         [tl_origin[1]-offset,tl_origin[-1]-offset], color = tu.palette[2])
        
for i in untransformed:
    ax1.plot(i,0.2, marker = '.', color = tu.palette[3])
    
for j in -trans_added+frag_len:
    ax2.plot(0.8,j, marker = '.', color = tu.palette[3])

# Draw band size
ax0.annotate('', xy=(0.45, 0.61), xycoords='axes fraction', xytext=(0.45, 0.49), 
                        arrowprops=dict(arrowstyle="<->", color=tu.palette[5],linewidth=1.5)) 


ax1.annotate("Reference Peaks",(0.5,0.5),xycoords = 'axes fraction',
             fontsize = 8, horizontalalignment = 'center')
ax2.annotate("Input Peaks",(0.5,0.5),xycoords = 'axes fraction',
             fontsize = 8, rotation = 90, verticalalignment = 'center')

plt.savefig('filepath/Lab 2 - Keyser/MPhil Thesis/7-FSMatrix.png', dpi=300)


#%% Gaussian Penalty

def favor_fragment(frag_no):

    current = np.zeros(tu.frag_len)
    peak_len = 200 # optimize this
    peak_x = np.linspace(-peak_len/2,peak_len/2,peak_len)
    peak_y = 2*np.exp(-peak_x**2/(10*peak_len))
    #print(f'{len(peak_x)},{len(peak_y)}')
    for i in tu.MTaqI_locations[frag_no]:
        #print(i)
        for j in np.arange(-peak_len/2,peak_len/2):
            if i+j<tu.frag_len:
                j = int(j)
                #print(i,j)
                current[i+j] = max(current[i+j],peak_y[int(j+peak_len/2)])
            else:
                pass
        #print(i)
    current -= 1
        
    return current  

# just used for visualization
def simulate_peaks(frag_no):
    # for some bizarre reason without making a copy this 
    # modifies the original list of arrays (bad!)
    peaks = tu.MTaqI_locations[frag_no].copy()
    
    jitter = 100
    
    for idx,val in enumerate(peaks):
        peaks[idx] = val + jitter*np.random.rand()-jitter/2
        
    return peaks

# Create figure 
fig=plt.figure(figsize=(4,4.5), dpi=300) # 4,4 worked nicely without labels
# Create container
gs = gridspec.GridSpec(3,1, hspace = 0.2)
gs0 = gridspec.GridSpecFromSubplotSpec(2, 1, subplot_spec=gs[0], hspace = 0.4,height_ratios = (1,2))
gs1 = gridspec.GridSpecFromSubplotSpec(2, 1, subplot_spec=gs[1], hspace = 0.4,height_ratios = (1,2))
gs2 = gridspec.GridSpecFromSubplotSpec(2, 1, subplot_spec=gs[2], hspace = 0.4,height_ratios = (1,2))

ax0 = fig.add_subplot(gs0[0,0])
ax1 = fig.add_subplot(gs0[1,0])
ax2 = fig.add_subplot(gs1[0,0])
ax3 = fig.add_subplot(gs1[1,0])
ax4 = fig.add_subplot(gs2[0,0])
ax5 = fig.add_subplot(gs2[1,0])

fav = favor_fragment(0)
good_peaks = simulate_peaks(0)
bad_peaks  = simulate_peaks(1)

ax1.plot(fav, color = tu.palette[2])
ax3.plot(fav, color = tu.palette[2])
ax5.plot(fav, color = tu.palette[2])

for ax in [ax0,ax2,ax4]:
    tu.setup(ax,0,tu.frag_len)
    
for ax in [ax1,ax3,ax5]:
    ax.set_yticks(np.arange(-1,2))
    ax.set_xlim(0,tu.frag_len)
    ax.set_xticks(np.arange(0,12000,2000),[' ']*6)


sum0 = 0
for i in good_peaks:
    i = int(i)
    ax1.plot(i,fav[i],'.k')
    sum0 += fav[i]

for i in good_peaks:
    ax0.plot(i,0.25, marker = 'o', markersize = 3, color = 'k')

translation = 100    

sum1 = 0
for i in good_peaks:
    i = int(i)+translation
    ax3.plot(i,fav[i],'.k')
    sum1 += fav[i]
    
for i in good_peaks:
    ax2.plot(i+translation,0.25, marker = 'o', markersize = 3, color = 'k')
    
sum2 = 0
for i in bad_peaks:
    i = int(i)
    ax5.plot(i,fav[i],'.k')
    sum2 += fav[i]
    
for i in bad_peaks:
    ax4.plot(i,0.25, marker = 'o', markersize = 3, color = 'k')
    
ax0.annotate('A.',(0,0.7), xycoords = 'axes fraction',
             fontsize = 9, fontweight = 'bold')

ax2.annotate('B.',(0,0.7), xycoords = 'axes fraction',
             fontsize = 9, fontweight = 'bold')

ax4.annotate('C.',(0,0.7), xycoords = 'axes fraction',
             fontsize = 9, fontweight = 'bold')

for ax in [ax1,ax3,ax5]:
    ax.set_ylabel('Score')
    
ax5.set_xlabel('Position (bp)')
    
print(f'{sum0},{sum1},{sum2}')



plt.savefig('filepath/Lab 2 - Keyser/MPhil Thesis/7-GSDPlot.png', dpi=300)
