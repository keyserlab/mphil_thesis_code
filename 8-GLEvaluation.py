#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import pickle

import inspect
import os

# Path to current file
path = inspect.getfile(inspect.currentframe())
# Path to directory that file is in
directory = os.path.realpath(os.path.dirname(path))
# Change directory 
os.chdir(directory)

import thesisutils as tu

# computes accuracy of a result list
def rl_accuracy(rl):
    true_counts  = 0
    false_counts = 0
        
    for i in rl[1:]:
        if (i[0] == i[1][1]) or (i[0]+len(tu.MTaqI_locations) == i[1][1]):
            true_counts +=1
        else:
            #print(i)
            false_counts +=1
            
    return true_counts

def rl_wrongs(rl):
    wrong_list = []
    wrong_frag = []
    for i in rl[1:]:
        if (i[0] == i[1][1]) or (i[0]+len(tu.MTaqI_locations) == i[1][1]):
            pass
        else:
            wrong_list.append(i)
            if i[1][1] > len(tu.MTaqI_locations):
                wrong_frag.append(i[1][1]-len(tu.MTaqI_locations))
            else:
                wrong_frag.append(i[1][1])
        
    return wrong_list,wrong_frag

def fs_scaling(rl):
    fidpt_cor_list = []
    fidpt_wro_list = []
    for i in rl[1:]:
        if (i[0] == i[1][1]) or (i[0]+len(tu.MTaqI_locations) == i[1][1]):
            fidpt_cor_list.append([i[1][2],i[1][3]])
        else:
            fidpt_wro_list.append([i[1][2],i[1][3]])
            
    return fidpt_cor_list, fidpt_wro_list


def counter(fidpt_list):
    counter = 0
    for i in fidpt_list:
        if i == [1,-2]:
            counter +=1
            
    print(f'{counter} correct scalings out of {len(fidpt_list)} events')

    return counter
            
    

#%%

# =============================================================================
# Dot Matrix
# =============================================================================
frag_mins   = [0]
frag_maxs   = [len(tu.MTaqI_locations)] # file with name frag_max-1 must exist
ref_maxs    = [2*len(tu.MTaqI_locations)]
revs        = [False,True]
dm_bands    = [2*i+1 for i in range(9)]
dm_scales   = [.95,.96,.97,.98,.99,1,1.01,1.02,1.03,1.04,1.05]
rems        = [np.array([69, 119]),np.array([-777])]

# dot matrix (scaling) dictionary (False/True) (removed/all)
dm_dict_Fr = {}
dm_dict_Tr = {}
dm_dict_F  = {}
dm_dict_T  = {}
dms_dict_Fr = {}
dms_dict_Tr = {}

files_loaded = 0

for frag_min in frag_mins:
    for frag_max in frag_maxs:
        for ref_max in ref_maxs:
            for rev in revs:
                for band in dm_bands:
                    for rem in rems:
                        
                        scale = 1

                        folderpath = 'filepath/Lab 2 - Keyser/MPhil Thesis/DM_Pickles/'
                        filename   = f'DMfrag_len=10000,frags={frag_min}to{frag_max},ref_max={ref_max},rev={rev},band={band},scale={scale},rem={rem}'
                        

                        with open(folderpath+filename+'.pickle', 'rb') as fp:
                            rl = pickle.load(fp)
                            files_loaded+=1
                        

                        if (rem == np.array([69, 119])).all() & (rev == False):
                            dm_dict_Fr[f'{frag_min}to{frag_max},{ref_max},{rev},{band},{rem}'] = rl
                        elif (rem == np.array([69, 119])).all() & (rev == True):
                            dm_dict_Tr[f'{frag_min}to{frag_max},{ref_max},{rev},{band},{rem}'] = rl
                        elif (rem == np.array([-777])).all() & (rev == False):
                            dm_dict_F[f'{frag_min}to{frag_max},{ref_max},{rev},{band},{rem}'] = rl
                        else:
                            dm_dict_T[f'{frag_min}to{frag_max},{ref_max},{rev},{band},{rem}'] = rl
for rev in revs:                                
    for scale in dm_scales:
        frag_min = 0
        frag_max = len(tu.MTaqI_locations)
        ref_max  = 2*len(tu.MTaqI_locations)
        band     = 11
        rem      = np.array([69, 119])
        
        
        folderpath = 'filepath/Lab 2 - Keyser/MPhil Thesis/DM_Pickles/'
        filename   = f'DMfrag_len=10000,frags={frag_min}to{frag_max},ref_max={ref_max},rev={rev},band={band},scale={scale},rem={rem}'
        
    
        with open(folderpath+filename+'.pickle', 'rb') as fp:
            rl = pickle.load(fp)
            files_loaded+=1
        
    
        if (rem == np.array([69, 119])).all() & (rev == False):
            dms_dict_Fr[f'{frag_min}to{frag_max},{ref_max},{rev},{band},{scale},{rem}'] = rl
        elif (rem == np.array([69, 119])).all() & (rev == True):
            dms_dict_Tr[f'{frag_min}to{frag_max},{ref_max},{rev},{band},{scale},{rem}'] = rl
    
                            


dm_Fr_list = []                            
for key in dm_dict_Fr:
    dm_Fr_list.append(rl_accuracy(dm_dict_Fr[key])/len(tu.MTaqI_locations))
    
dm_Tr_list = []                            
for key in dm_dict_Tr:
    dm_Tr_list.append(rl_accuracy(dm_dict_Tr[key])/len(tu.MTaqI_locations))
    
dm_F_list = []                            
for key in dm_dict_F:
    dm_F_list.append(rl_accuracy(dm_dict_F[key])/len(tu.MTaqI_locations))
    
dm_T_list = []                            
for key in dm_dict_T:
    dm_T_list.append(rl_accuracy(dm_dict_T[key])/len(tu.MTaqI_locations))
    
dms_Fr_list = []                            
for key in dms_dict_Fr:
    dms_Fr_list.append(rl_accuracy(dms_dict_Fr[key])/len(tu.MTaqI_locations))
    
dms_Tr_list = []                            
for key in dms_dict_Tr:
    dms_Tr_list.append(rl_accuracy(dms_dict_Tr[key])/len(tu.MTaqI_locations))


wrong_list,wrong_frag = rl_wrongs(dm_dict_F['0to465,930,False,11,[-777]'])



#%%

# =============================================================================
# Fiduciary Scan
# =============================================================================
frag_mins   = [0]
frag_maxs   = [len(tu.MTaqI_locations)] # file with name frag_max-1 must exist
ref_maxs    = [2*len(tu.MTaqI_locations)]
revs        = [False,True]
fs_bands    = [2*i+1 for i in range(49)]
rems        = [np.array([69, 119]),np.array([-777])]

# fiduciary scan dictionary (False/True) (removed/all)
fs_dict_Fr = {}
fs_dict_Tr = {}
fs_dict_F  = {}
fs_dict_T  = {}

for frag_min in frag_mins:
    for frag_max in frag_maxs:
        for ref_max in ref_maxs:
            for rev in revs:
                for band in fs_bands:
                    for rem in rems:
                        folderpath = 'filepath/Lab 2 - Keyser/MPhil Thesis/FS_Pickles/'
                        filename   = f'FSfrag_len=10000,frags={frag_min}to{frag_max},ref_max={ref_max},rev={rev},band={band},rem={rem}'
                        with open(folderpath+filename+'.pickle', 'rb') as fp:
                            rl = pickle.load(fp)
                        
                        if (rem == np.array([69, 119])).all() & (rev == False):
                            fs_dict_Fr[f'{frag_min}to{frag_max},{ref_max},{rev},{band},{rem}'] = rl
                        elif (rem == np.array([69, 119])).all() & (rev == True):
                            fs_dict_Tr[f'{frag_min}to{frag_max},{ref_max},{rev},{band},{rem}'] = rl
                        elif (rem == np.array([-777])).all() & (rev == False):
                            fs_dict_F[f'{frag_min}to{frag_max},{ref_max},{rev},{band},{rem}'] = rl
                        else:
                            fs_dict_T[f'{frag_min}to{frag_max},{ref_max},{rev},{band},{rem}'] = rl
           
fs_Fr_list = []                            
for key in fs_dict_Fr:
    fs_Fr_list.append(rl_accuracy(fs_dict_Fr[key])/len(tu.MTaqI_locations))
    
fs_Tr_list = []                            
for key in fs_dict_Tr:
    fs_Tr_list.append(rl_accuracy(fs_dict_Tr[key])/len(tu.MTaqI_locations))

fs_F_list = []                            
for key in fs_dict_F:
    fs_F_list.append(rl_accuracy(fs_dict_F[key])/len(tu.MTaqI_locations))
    
fs_T_list = []                            
for key in fs_dict_T:
    fs_T_list.append(rl_accuracy(fs_dict_T[key])/len(tu.MTaqI_locations))
    

# Evaluate whether scaling was correct or incorrect    
fidpt_cor_list, fidpt_wro_list = fs_scaling(fs_dict_Fr['0to465,930,False,59,[ 69 119]'])

counter(fidpt_cor_list)
counter(fidpt_wro_list)


#%%

# =============================================================================
# Gaussian Penalty
# =============================================================================
frag_mins   = [0]
frag_maxs   = [len(tu.MTaqI_locations)] # file with name frag_max-1 must exist
ref_maxs    = [2*len(tu.MTaqI_locations)]
revs        = [False,True]
gp_scales   = [0.1*i+9.5 for i in range(11)]
rems        = [np.array([69, 119]),np.array([-777])]
nadds       = [i for i in range(10)]
ndels       = [i for i in range(10)]
    
# gaussian penalty (indel) dictionary (False/True) (removed/all)
gp_dict_Fr = {}
gp_dict_Tr = {}
gp_dict_F = {}
gp_dict_T = {}
gpi_dict_Fr = {}
gpi_dict_Tr = {}

files_loaded = 0

for frag_min in frag_mins:
    for frag_max in frag_maxs:
        for ref_max in ref_maxs:
            for rev in revs:
                for scale in gp_scales:
                    for rem in rems:
                        folderpath = 'filepath/Lab 2 - Keyser/MPhil Thesis/GP_Pickles/'
                        filename   = f'GPfrag_len=10000,frags={frag_min}to{frag_max},ref_max={ref_max},rev={rev},scale={scale},rem={rem}'
                        with open(folderpath+filename+'.pickle', 'rb') as fp:
                            rl = pickle.load(fp)
                            
                            
                        if (rem == np.array([69, 119])).all() & (rev == False):
                            gp_dict_Fr[f'{frag_min}to{frag_max},{ref_max},{rev},{scale},{rem}'] = rl
                        elif (rem == np.array([69, 119])).all() & (rev == True):
                            gp_dict_Tr[f'{frag_min}to{frag_max},{ref_max},{rev},{scale},{rem}'] = rl
                        elif (rem == np.array([-777])).all() & (rev == False):
                            gp_dict_F[f'{frag_min}to{frag_max},{ref_max},{rev},{scale},{rem}'] = rl
                        else:
                            gp_dict_T[f'{frag_min}to{frag_max},{ref_max},{rev},{scale},{rem}'] = rl
                        
                        
for rev in revs:
    for nadd in nadds:
        for ndel in ndels:
            frag_min = 0
            frag_max = len(tu.MTaqI_locations)
            ref_max  = 2*len(tu.MTaqI_locations)
            scale    = 10.0
            rem      = np.array([69, 119])
            
            folderpath = 'filepath/Lab 2 - Keyser/MPhil Thesis/GP_Pickles/'
            filename   = f'GPfrag_len=10000,frags={frag_min}to{frag_max},ref_max={ref_max},rev={rev},scale={scale},rem={rem},nadd={nadd},ndel={ndel}'
        
        
            with open(folderpath+filename+'.pickle', 'rb') as fp:
                rl = pickle.load(fp)
                files_loaded+=1
            
        
            if (rem == np.array([69, 119])).all() & (rev == False):
                gpi_dict_Fr[f'{frag_min}to{frag_max},{ref_max},{rev},{scale},{rem},{nadd},{ndel}'] = rl
            elif (rem == np.array([69, 119])).all() & (rev == True):
                gpi_dict_Tr[f'{frag_min}to{frag_max},{ref_max},{rev},{scale},{rem},{nadd},{ndel}'] = rl

                            
gp_Fr_list = []                            
for key in gp_dict_Fr:
    gp_Fr_list.append(rl_accuracy(gp_dict_Fr[key])/len(tu.MTaqI_locations))
    
gp_Tr_list = []                            
for key in gp_dict_Tr:
    gp_Tr_list.append(rl_accuracy(gp_dict_Tr[key])/len(tu.MTaqI_locations))
    
gp_F_list = []                            
for key in gp_dict_F:
    gp_F_list.append(rl_accuracy(gp_dict_F[key])/len(tu.MTaqI_locations))
    
gp_T_list = []                            
for key in gp_dict_T:
    gp_T_list.append(rl_accuracy(gp_dict_T[key])/len(tu.MTaqI_locations))
    
    
gpi_Fr_list = []                            
for key in gpi_dict_Fr:
    gpi_Fr_list.append(rl_accuracy(gpi_dict_Fr[key])/len(tu.MTaqI_locations))
    
gpi_Tr_list = []                            
for key in gpi_dict_Tr:
    gpi_Tr_list.append(rl_accuracy(gpi_dict_Tr[key])/len(tu.MTaqI_locations))
    
#%% Performance

# All values transcribed from IPython terminal 

dmb_bd = [1,3,5,7,9,11,13,15,17] # band sizes
dmb_mu = [9.85,12.3,14.7,17.2,19.6,22.1,24.5,26.9,29.6] # mean (s)
dmb_st = [121,78.8,83.8,124,76.4,143,102,75.2,108]   # standard deviations (ms)

# band9
dmr_rm = [930,465,232,116]     # ref max
dmr_mu = [19.5,9.95,4.96,2.46] # mean (s)
dmr_st = [76.4,59.8,26.4,10.7] # tandard deviation (ms)


fsr_rm = [930,465,232,116]     # ref maxes
fsr_mu = [5.37,2.7,1.32,.668]  # means (ms)
fsr_st = [12.3,33.9,2.66,1.13] # standard deviations (us)

fsb_bd = [11,51,91]            # band sizes
fsb_mu = [5.43,5.37,5.36]      # mean (ms)
fsb_st = [80.6,12.3,5.96]      # standard deviations (us)

gpr_rm = [930,465,232,116]     # ref maxes
gpr_mu = [158,79.3,40.3,19.5]  # means (ms)
gpr_st = [1380,136,244,125]    # standard deviations (us)


fig = plt.figure(figsize=(6.26,3), dpi = 300)
gs  = gridspec.GridSpec(1,3, bottom = 0.4, top = 0.9, wspace=0.35, right = 0.99)
gsleg = gridspec.GridSpec(1,1, bottom  = 0,top = 0.3)

ax0 = fig.add_subplot(gs[0,0])
ax1 = fig.add_subplot(gs[0,1])
ax2 = fig.add_subplot(gs[0,2])

axleg = fig.add_subplot(gsleg[0,0])

ax0.errorbar(x = dmr_rm,y= dmr_mu, yerr = [i/1e3 for i in dmr_st], 
             fmt = '.', color = tu.palette[3], label = 'dm')
ax0.errorbar(fsr_rm,[i/1e3 for i in fsr_mu],yerr = [i/1e6 for i in fsr_st],
             fmt = '.', color = tu.palette[1], label = 'fs')
ax0.errorbar(gpr_rm,[i/1e3 for i in gpr_mu],yerr = [i/1e6 for i in gpr_st],
             fmt = '.', color = tu.palette[7], label = 'gp')
ax0.set_xlabel('Ref Max')
ax0.set_ylabel('Time (s)')
ax0.set_yscale('log')
#ax0.set_xscale('log')

ax1.errorbar(x = dmb_bd, y = dmb_mu, yerr = [i/1e3 for i in dmb_st], 
             fmt='.', color = tu.palette[3])
ax1.set_xlabel('Band Size')
ax1.set_ylabel('Time (s)')
ax1.set_ylim(0,30)

ax2.errorbar(x = fsb_bd, y = fsb_mu, yerr = [i/1e3 for i in fsb_st], 
             fmt='.', color = tu.palette[1])
ax2.set_xlabel('Band Size')
ax2.set_ylabel('Time (ms)')
ax2.set_ylim(0,6)
#ax2.ticklabel_format(style = 'scientific', scilimits= (0,0))


h, l = ax0.get_legend_handles_labels()
axleg.legend(h,['Dot Matrix','Fiduciary Scan','Gaussian Penalty'], loc = 'lower center')
axleg.axis('off')


ax0.annotate('A.',(0,1.05), xycoords = 'axes fraction',
             fontsize = 9, fontweight = 'bold')
ax1.annotate('B.',(0,1.05), xycoords = 'axes fraction',
             fontsize = 9, fontweight = 'bold')
ax2.annotate('C.',(0,1.05), xycoords = 'axes fraction',
             fontsize = 9, fontweight = 'bold')

#plt.savefig('filepath/Lab 2 - Keyser/MPhil Thesis/8-Performance.png',dpi = 300)  


#%%

fig = plt.figure(figsize=(6.26,8), dpi = 300)
gs  = gridspec.GridSpec(3,3, bottom = 0.2, top = 0.95, wspace=0.35, hspace = 0.3)
gsleg = gridspec.GridSpec(1,2, bottom  = 0.05,top = 0.15)

# Was easier to move around subplot here
ax0 = fig.add_subplot(gs[0,0])
ax1 = fig.add_subplot(gs[1,1])
ax2 = fig.add_subplot(gs[2,0])
ax3 = fig.add_subplot(gs[1,0])
ax4 = fig.add_subplot(gs[0,1:])
ax5 = fig.add_subplot(gs[2,1])
ax6 = fig.add_subplot(gs[2,2])

axleg = fig.add_subplot(gsleg[0,0])
#axcbar = fig.add_subplot(gsleg[0,1])

forrem = tu.palette[0]
revrem = tu.palette[1]
fornon = tu.palette[2]
revnon = tu.palette[3]

# Dot matrix accuracy vs. band size
ax0.plot(dm_bands, dm_Fr_list, label = 'For Rem', color = forrem)
ax0.plot(dm_bands, dm_Tr_list, label = 'Rev Rem', color = revrem)
ax0.plot(dm_bands, dm_F_list, label = 'For', color = fornon)
ax0.plot(dm_bands, dm_T_list, label = 'Rev', color = revnon)
ax0.set_xlabel('Band Size')

ax0.annotate('A.',(0,1.05), xycoords = 'axes fraction',
             fontsize = 9, fontweight = 'bold')

# Fiduciary Scan accuracy vs. band size
ax1.plot(fs_bands,fs_Fr_list, label = 'For Rem', color = forrem)
ax1.plot(fs_bands,fs_Tr_list, label = 'Rev Rem', color = revrem)
ax1.plot(fs_bands,fs_F_list, label = 'For', color = fornon)
ax1.plot(fs_bands,fs_T_list, label = 'Rev',  color = revnon)
ax1.set_xlabel('Band Size')
ax1.annotate('D.',(0,1.05), xycoords = 'axes fraction',
             fontsize = 9, fontweight = 'bold')

# Gaussian Penalty accuracy vs. scaling
ax2.plot(gp_scales,gp_Fr_list, label = 'For Rem', color = forrem)
ax2.plot(gp_scales,gp_Tr_list, label = 'Rev Rem', color = revrem)
ax2.plot(gp_scales,gp_F_list, label = 'For', color = fornon)
ax2.plot(gp_scales,gp_T_list, label = 'Rev', color = revnon)
ax2.set_xlabel('Scaling')
ax2.annotate('E.',(0,1.05), xycoords = 'axes fraction',
             fontsize = 9, fontweight = 'bold')

ax2.annotate('', xy=(0.2, 0.7), xycoords='axes fraction', xytext=(0.5,0.985), 
                        arrowprops=dict(arrowstyle="<-", color= 'k', linewidth=1.5,
                                        connectionstyle="angle,angleA=20,angleB=90")) 
ax2.annotate('98.5%', xy=(0.2, 0.6), xycoords = 'axes fraction',
             fontsize = 8, horizontalalignment = 'center')
 

# Dot Matrix accuracy vs. scaling
ax3.plot(dm_scales,dms_Fr_list,label = 'R. False', color = forrem)
ax3.plot(dm_scales,dms_Tr_list,label = 'R. True', color = revrem)
ax3.set_xlabel('Scaling')
ax3.annotate('C.',(0,1.05), xycoords = 'axes fraction',
             fontsize = 9, fontweight = 'bold')

# Most frequent misidentifications
ax4.hist(wrong_frag,bins = np.arange(0,len(tu.MTaqI_locations)+1), color = 'k')
ax4.annotate('B.',(0,1.05), xycoords = 'axes fraction',
             fontsize = 9, fontweight = 'bold')
ax4.annotate('Frag. 69',(0.2,0.9), xycoords = 'axes fraction',
             fontsize = 8)
ax4.annotate('Frag. 119',(0.3,0.115), xycoords = 'axes fraction',
             fontsize = 8)
ax4.set_xlabel('Fragment Number')
ax4.set_ylabel('Misidentifications')

# Gaussian Penalty accuracy vs. indels
gpi_Fr_arr = np.array(gpi_Fr_list)
gpi_Fr_sq  = np.reshape(gpi_Fr_arr, (10,10))
gpi_Tr_arr = np.array(gpi_Tr_list)
gpi_Tr_sq  = np.reshape(gpi_Tr_arr, (10,10))


mappable = ax5.imshow(gpi_Fr_sq, vmin = 0, vmax = 1,cmap='Reds')
ax5.annotate('F.',(0,1.05), xycoords = 'axes fraction',
             fontsize = 9, fontweight = 'bold')
ax6.imshow(gpi_Tr_sq, vmin = 0, vmax = 1,cmap='Reds')
ax6.annotate('G.',(0,1.05), xycoords = 'axes fraction',
             fontsize = 9, fontweight = 'bold')

# Manually add axes
axcbar = fig.add_axes([0.5, 0.09, 0.33, 0.02])
axcbar.set_title('Accuracy', fontsize = 8)
#fig.colorbar(im, cax=cax)

plt.colorbar(mappable, orientation = 'horizontal',cax = axcbar)
#axcbar.axis('off')


h, l = ax0.get_legend_handles_labels()
axleg.legend(h,['Forward, Removed','Reverse, Removed','Forward','Reverse'], loc = 'lower center')
axleg.axis('off')


for ax in [ax0,ax1,ax2,ax3]:
    ax.set_ylim(0,1)
    ax.set_ylabel('Accuracy')
    
fig.align_ylabels([ax4,ax1])
    
for ax in [ax5,ax6]:
    ax.set_xlabel('Number of deletions')
    ax.set_ylabel('Number of additions')
    ax.set_xticks(np.arange(0,10,2),['0','2','4','6','8'])
    ax.set_yticks(np.arange(0,10,2),['0','2','4','6','8'])
    
plt.savefig('filepath/Lab 2 - Keyser/MPhil Thesis/8-Accuracy-2.png',dpi = 300)    
    
    


                    