#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import pandas as pd

import inspect
import os

# Path to current file
path = inspect.getfile(inspect.currentframe())
# Path to directory that file is in
directory = os.path.realpath(os.path.dirname(path))
# Change directory 
os.chdir(directory)

import thesisutils as tu

path = 'filepath/Lab 2 - Keyser/ANA_03_Artemis/7.1/DNA/-0.1_-0.05_(1000,50)/Unfolded_w_identifable_peaks/'

# Load and run some dataprocessing
intdf1234  = tu.wrapperA4('filepath/Lab 2 - Keyser/ANA_03_Artemis/7.1/DNA/-0.1_-0.05_(1000,50)/Unfolded_w_identifable_peaks/',
                          4,3,2,1)

big_data = intdf1234
big_data['Width'] = big_data['right_x']-big_data['left_x']

big_data[['Integral','deltaI','Event duration (s)','Event charge deficit (fC)',
          'dI/Io','Ratio dI', 'Ratio pECD']] = big_data[['Integral','deltaI','Event duration (s)','Event charge deficit (fC)',
                    'dI/Io','Ratio dI', 'Ratio pECD']].apply(pd.to_numeric)

# =============================================================================
# big_data.rename(columns = {'Integral':'peak Charge Deficit (pECD)', 
#                            'deltaI':'peak Depth (dI)',
#                            'dI/Io':'Normalize peak depth (dI/Io)'}, inplace = True)
# 
# =============================================================================

print('Peak Labeling Completed!')

event0 = tu.eventLoader(path,107)
event1 = tu.eventLoader(path,27)


fig = plt.figure(figsize=(6.26,7), dpi = 300)
gs = gridspec.GridSpec(3,2, wspace=0.4,hspace = 0.4, left = 0.1, right = 0.8, height_ratios = (0.8,1,1))
gsleg = gridspec.GridSpec(1,1, left = 0.8,right = 1)

ax0 = fig.add_subplot(gs[0,0])
ax1 = fig.add_subplot(gs[0,1])
ax2 = fig.add_subplot(gs[1,0])
ax3 = fig.add_subplot(gs[1,1])
ax4 = fig.add_subplot(gs[2,0])
ax5 = fig.add_subplot(gs[2,1])
axleg = fig.add_subplot(gsleg[0,0])

for ax in [ax0,ax1]:
    ax.set_ylim(-0.4,0.05)

tu.current_time(event0,ax0,50,0.1,6,4)
tu.current_time(event1,ax1,50,0.1,6,4)

ax0.annotate("A.",(0,1.05),xycoords = 'axes fraction',
            fontsize = 9, fontweight = 'bold')

# =============================================================================
# x0 = big_data[['Integral', 'deltaI','hline', 'Pore Baseline',
#               'Event duration (s)', 'Mean event current (nA)', 'Peak current value (nA)',
#               'Event charge deficit (fC)', 'Io', 'dI/Io', 'dI/Io/ECD', 'dI/Io/s']]
# =============================================================================

x0 = big_data[['Integral', 'deltaI','Direction',
              'Event duration (s)', 'Mean event current (nA)', 'Peak current value (nA)',
              'Event charge deficit (fC)','dI/Io','Width']]

x1 = big_data[['Integral', 'deltaI','Direction',
              'dI/Io','Width']]

x2 = big_data[['Ratio pECD','Ratio dI']]

x3 = big_data[['Integral', 'deltaI']]

tu.LDA_plot(x0,big_data, ax2,'B.')
tu.LDA_plot(x1,big_data, ax3,'C.')
tu.LDA_plot(x2,big_data, ax4,'D.')
tu.LDA_plot(x3,big_data, ax5,'E.')

# Get legend from ax1 and then plot it in axleg
h, l = ax2.get_legend_handles_labels()
axleg.legend(h,['Peak 1','Peak 2','Peak 3','Peak 4'], loc = 'center')
axleg.axis('off')


plt.savefig('filepath/Lab 2 - Keyser/MPhil Thesis/2-LDA_Artemis.png',dpi=300)

#%% Barcode 
import seaborn as sns

input_barcode = [1,4,2,1]

target_barcodes = []
for i in [1,2,3,4]:
    for j in [1,2,3,4]:
        for k in [1,2,3,4]:
            for l in [1,2,3,4]:
                target_barcodes.append([i,j,k,l])

# rows are what peak actually is
# columns are what peak is predicted
cm = [[32,  5,  0,  0],
      [ 9, 30,  4,  0],
      [ 0,  2, 32,  4],
      [ 0,  0,  9, 33]]

# Peak identies in order
cm_order = [1,2,3,4]

# Make a nested diction. 
#cm_dict[x][y] tells you how many times a peak x is seen as a peak y
cm_dict = {i:{j:cm[i-1][j-1] for j in cm_order} for i in cm_order}

rel_conc = [1 for i in target_barcodes] # relative concentration

def heatmapper(input_barcode, ax):

    prob = [] # array to store probabilities
    
    for idx,target in enumerate(target_barcodes):
        Ptarget = rel_conc[idx]/sum(rel_conc)
        #print(f'Evaluating target: {target} Rel conc. = {Ptarget :.1%}')
        
        PintGtar = 0    # initialize forward
        PintGtarREV = 0 # intiiaalize reverse
        for jdx, bar in enumerate(target):
            cm_entry = cm[cm_order.index(bar)][cm_order.index(input_barcode[jdx])]
            cm_sum   = (sum(sum(x) for x in cm))
            #print(bar,'is seen as a', input_barcode[jdx],cm_dict[bar][input_barcode[jdx]],sum(cm_dict[bar].values()))
            PintGtar += cm_entry/sum(cm_dict[bar].values())
            
            
            print(cm_dict[bar][input_barcode[jdx]]-cm[cm_order.index(bar)][cm_order.index(input_barcode[jdx])])
            
        #for jdx, bar in enumerate(target[::-1]):
            #print(bar,'is seen as a', input_barcode[jdx],cm_dict[bar][input_barcode[jdx]],sum(cm_dict[bar].values()))
            #PintGtarREV += cm_dict[bar][input_barcode[jdx]]/sum(cm_dict[bar].values())/len(input_barcode)

        #print(f'P(input|target) P(input|targetREV) = {PintGtar :.1%} {PintGtarREV :.1%}')
        
        prob.append(Ptarget*max(PintGtar,PintGtarREV))
        print(f'P(target)*P(input|target) = {Ptarget*max(PintGtar,PintGtarREV) :.3%}')
        
    
    prob = np.array(prob)
    
    prob_square = np.reshape(prob, (16,16))
    
    prob_square /= prob_square.max()
    
    thd = 0.3
    
    thd_arr = prob_square.max()-prob_square
    
    imshow = ax.imshow(prob_square, cmap = 'bwr_r')
    cax = fig.add_axes([ax.get_position().x1+0.03,ax.get_position().y0,0.03,ax.get_position().y1-ax.get_position().y0])
    fig.colorbar(imshow,cax = cax)
    cax.plot([0,1],[1-thd,1-thd],c='#39ff14', linewidth = 3)
    cax.set_ylabel('Relative Probability')
    #sns.heatmap(prob_square, ax = ax)
    #ax.set_xticks(np.arange(0,16,2),['xx11','xx13','xx21','xx23','xx31','xx33','xx41','xx43'])
    ax.set_xticks(np.arange(0,16,1),['xx11','xx12','xx13','xx14','xx21','xx22','xx23','xx24',
                                     'xx31','xx32','xx33','xx34','xx41','xx42','xx43','xx44'])
    ax.tick_params(axis = 'x',labelrotation = 90)
    #ax.set_yticks(np.arange(0,16,2),['11xx','13xx','21xx','23xx','31xx','33xx','41xx','43xx'])
    ax.set_yticks(np.arange(0,16,1),['11xx','12xx','13xx','14xx','21xx','22xx','23xx','24xx',
                                     '31xx','32xx','33xx','34xx','41xx','42xx','43xx','44xx'])
    ax.set_xlabel('Last two digits of barcode')
    ax.set_ylabel('First two digits of barcode')
    
    
    
    for row in range(thd_arr.shape[0]):
        for col in range(thd_arr.shape[1]):
            if thd_arr[row][col] < thd:
                ax.plot(col,row,c='#39ff14',marker = 'o')
    



# Create figure 
fig=plt.figure(figsize=(6.26,8), dpi=300)
# Create container
gs = gridspec.GridSpec(2,1, left = 0.1, bottom = 0.1, top = 0.95,hspace = 0.3, right = 0.85)
# Add axes to containers
ax0 = fig.add_subplot(gs[0,0])
ax1 = fig.add_subplot(gs[1,0])
#cax = fig.add_subplot(gs[0,1])

# This shows probability that input is seen as each possible barcode

heatmapper([1,4,2,1],ax0)
heatmapper([3,3,1,2],ax1)

ax0.annotate('A. (1421)',(0,1.05), xycoords = 'axes fraction',
             fontsize = 9, fontweight = 'bold')
ax1.annotate('B. (3312)',(0,1.05), xycoords = 'axes fraction',
             fontsize = 9, fontweight = 'bold')

plt.savefig('filepath/Lab 2 - Keyser/MPhil Thesis/2-Spellcheck.png',dpi = 300)


