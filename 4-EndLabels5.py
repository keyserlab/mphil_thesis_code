#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import pandas as pd
import seaborn as sns

import inspect
import os

# Path to current file
path = inspect.getfile(inspect.currentframe())
# Path to directory that file is in
directory = os.path.realpath(os.path.dirname(path))
# Change directory 
os.chdir(directory)

import thesisutils as tu

# Load and run some dataprocessing
intdf3  = tu.wrapperE5('filepath/Lab 2 - Keyser/ANA_01_Filip Ends Analysis/-0.1_-0.05_(400,50)_20210803_3/intcleanNormfirst846.pkl',
                       5,2)
intdf31 = tu.wrapperE5('filepath/Lab 2 - Keyser/ANA_01_Filip Ends Analysis/-0.1_-0.05_(400,50)_20210803_3.1/auseful file/intcleanNormfirst2550.pkl',
                       4,2)
intdf32 = tu.wrapperE5('filepath/Lab 2 - Keyser/ANA_01_Filip Ends Analysis/-0.1_-0.05_(400,40)_20210803_3.2/ausefulfile/intcleanNormfirst2555.pkl',
                       3,1)

big_data = pd.concat([intdf3,intdf31,intdf32],axis=0, ignore_index=True)
big_data['Width'] = big_data['right_x']-big_data['left_x']

# https://stackoverflow.com/questions/45989249/pandas-pivot-table-valueerror-index-contains-duplicate-entries-cannot-reshape
# reformat data for plotting scatter
# must include event_type to a) have hues for scatter and 
# b) to have all events included (some have same event number)
big_dataInt=big_data.groupby(['event_num','event_type','Peak_Q']).Integral.first().unstack()
big_dataInt=big_dataInt.reset_index()
big_datadI=big_data.groupby(['event_num','event_type','Peak_Q']).deltaI.first().unstack()
big_datadI=big_datadI.reset_index()

print('Peak Labeling Completed!')

#palette = ['#b2df8a','#33a02c','#a6cee3','#1f78b4',
#           '#fb9a99','#e31a1c','#fdbf6f','#ff7f00']

palette = ['#33a02c','#1f78b4','#e31a1c','#ff7f00','#000000']
    

fig = plt.figure(figsize=(6.26,8))
gs = gridspec.GridSpec(3,2, wspace=0.4,hspace = 0.4, 
                       left = 0.1, right = 0.8, bottom = 0.1, top = 0.9)
gsleg = gridspec.GridSpec(2,1, left = 0.82, right = 0.99, height_ratios=(2,1))

ax0 = fig.add_subplot(gs[0,0])
ax1 = fig.add_subplot(gs[0,1])
ax2 = fig.add_subplot(gs[1,0])
ax3 = fig.add_subplot(gs[1,1])
ax4 = fig.add_subplot(gs[2,0])
ax5 = fig.add_subplot(gs[2,1])
axleg1 = fig.add_subplot(gsleg[0,0])
axleg2 = fig.add_subplot(gsleg[1,0])

x0 = big_data[['Integral', 'deltaI','Direction',
              'Event duration (s)', 'Mean event current (nA)', 'Peak current value (nA)',
              'Event charge deficit (fC)','dI/Io','Width']]

x1 = big_data[['Integral', 'deltaI','Direction',
              'dI/Io','Width']]

x2 = big_data[['Ratio pECD','Ratio dI']]

x3 = big_data[['Integral', 'deltaI']]

tu.LDA_plot(x0,big_data, ax0,'A.')
tu.LDA_plot(x1,big_data, ax1,'B.')
tu.LDA_plot(x2,big_data, ax2,'C.')
tu.LDA_plot(x3,big_data, ax3,'D.')

# Get legend from ax1 and then plot it in axleg
h, l = ax1.get_legend_handles_labels()
axleg1.legend(h,['Peak 1','Peak 2','Peak 3','Peak 4','Peak 5'], loc = 'center left')
axleg1.axis('off')

scatter_palette = [tu.palette[1],tu.palette[3],tu.palette[5]]

sns.scatterplot(x='Lil',y='Big',data=big_dataInt,hue = 'event_type', ax = ax4,
                palette = scatter_palette)
ax4.axis('equal')
IntMax = big_dataInt['Big'].max()
ax4.set_xlim(0,IntMax)
#ax4.set_title('Peak Charge Deficit')
ax4.annotate('E. Peak Charge Deficit',(0,1.05),xycoords = 'axes fraction',
            fontsize = 9, fontweight = 'bold')
ax4.set_ylabel('Large Peak Charge Deficit (fC)', fontsize = 8)
ax4.set_xlabel('Small Peak Charge Deficit (fC)', fontsize = 8)
ax4.legend().set_visible(False)

sns.scatterplot(x='Lil',y='Big',data=big_datadI,hue = 'event_type', ax = ax5,
                palette = scatter_palette)
ax5.axis('equal')
dIMax = big_datadI['Big'].max()
ax5.set_xlim(0,dIMax)
#ax5.set_title('Peak Depth')
ax5.annotate('F. Peak Depth',(0,1.05),xycoords = 'axes fraction',
            fontsize = 9, fontweight = 'bold')
ax5.set_ylabel('Large Peak Depth (nA)', fontsize = 8)
ax5.set_xlabel('Small Peak Depth (nA)', fontsize = 8)
ax5.legend().set_visible(False)

h, l = ax4.get_legend_handles_labels()
axleg2.legend(h,['3-1','4-2','5-2'], loc = 'center left')
axleg2.axis('off')

# Annoying fix because some y axes have single digits others double digits
# so they become misaligned
fig.align_ylabels([ax0,ax2,ax4])
fig.align_ylabels([ax1,ax3,ax5])

plt.savefig('filepath/Lab 2 - Keyser/MPhil Thesis/4-LDAE5v2.png',dpi=300)

#%% Sample Events

path13 = 'filepath/Lab 2 - Keyser/ANA_01_Filip Ends Analysis/-0.1_-0.05_(400,40)_20210803_3.2/ausefulfile/'
path24 = 'filepath/Lab 2 - Keyser/ANA_01_Filip Ends Analysis/-0.1_-0.05_(400,50)_20210803_3.1/auseful file/' 
path25 = 'filepath/Lab 2 - Keyser/ANA_01_Filip Ends Analysis/-0.1_-0.05_(400,50)_20210803_3/'

fig = plt.figure(figsize=(6.26,4))
gs = gridspec.GridSpec(3,2, wspace=0.4,hspace = 0.3, 
                       left = 0.1, right = 0.8, bottom = 0.1, top = 0.9)


ax0 = fig.add_subplot(gs[0,0])
ax1 = fig.add_subplot(gs[0,1])
ax2 = fig.add_subplot(gs[1,0])
ax3 = fig.add_subplot(gs[1,1])
ax4 = fig.add_subplot(gs[2,0])
ax5 = fig.add_subplot(gs[2,1])

for ax in [ax0,ax1,ax2,ax3,ax4,ax5]:
    ax.set_ylim(-0.41,0.05)
    
texts = ['A.','B.','C.']
for idx,ax in enumerate([ax0,ax2,ax4]):
    ax.annotate(texts[idx],(0,1.05),xycoords = 'axes fraction',
                fontsize = 9,fontweight = 'bold')


tu.current_time(tu.eventLoader(path13,23),ax0,100,0.05,6,3)
tu.current_time(tu.eventLoader(path13,33),ax1,100,0.05,6,3)
tu.current_time(tu.eventLoader(path24,4),ax2,100,0.05,6,3)
tu.current_time(tu.eventLoader(path24,9),ax3,100,0.05,6,3)
tu.current_time(tu.eventLoader(path25,7),ax4,100,0.05,6,3)
tu.current_time(tu.eventLoader(path25,12),ax5,100,0.05,6,3)

#plt.savefig('filepath/Lab 2 - Keyser/MPhil Thesis/4-E5Events.png',dpi=300)





