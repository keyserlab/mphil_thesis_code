#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.ticker as ticker
import pandas as pd

import inspect
import os

# Path to current file
path = inspect.getfile(inspect.currentframe())
# Path to directory that file is in
directory = os.path.realpath(os.path.dirname(path))
# Change directory 
os.chdir(directory)

import thesisutils as tu

path = 'filepath/Lab 2 - Keyser/ANA_02_Jinbo 81 Carrier library/2022.10.24_code 012/unfolded_p1/'

# Load and run some dataprocessing
intdf012 = tu.wrapperP3('filepath/Lab 2 - Keyser/ANA_02_Jinbo 81 Carrier library/2022.10.24_code 012/unfolded_p1/',
                        2,1,0)

big_data = intdf012
big_data[['Integral','deltaI','Event duration (s)','Event charge deficit (fC)',
          'dI/Io','Ratio dI', 'Ratio pECD']] = big_data[['Integral','deltaI','Event duration (s)','Event charge deficit (fC)',
                    'dI/Io','Ratio dI', 'Ratio pECD']].apply(pd.to_numeric)
big_data['Width'] = big_data['right_x']-big_data['left_x']

print('Peak Labeling Completed!')

event0 = tu.eventLoader(path,1)
event1 = tu.eventLoader(path,2)

fig = plt.figure(figsize=(6.26,5))
gs = gridspec.GridSpec(2,2,wspace=0.4,hspace = 0.4, height_ratios=(0.8,1),
                       left = 0.1, right = 0.8, bottom = 0.2, top = 0.9)
gsleg = gridspec.GridSpec(1,1, left = 0.8,right = 1)

ax0 = fig.add_subplot(gs[0,0])
ax1 = fig.add_subplot(gs[0,1])
ax2 = fig.add_subplot(gs[1,0])
ax3 = fig.add_subplot(gs[1,1])
axleg = fig.add_subplot(gsleg[0,0])

for ax in [ax0,ax1]:
    ax.set_ylim(-0.81,0.05)

tu.current_time(event0,ax0,50,0.1,6,4,fracxoff = 0.5)
tu.current_time(event1,ax1,50,0.1,6,4,fracxoff = 0.5)

ax0.annotate("A.",(0,1.05),xycoords = 'axes fraction',
            fontsize = 9, fontweight = 'bold')

tags = ['R','0','1','2','R']
pos0 = [148,215,271,350,415]
pos1 = [118,183,240,301,356]

for i in zip(tags,pos0):
    ax0.annotate(i[0],(i[1],-0.1),xycoords = 'data',
                 horizontalalignment = 'center',fontsize = 8)
    
for i in zip(tags,pos1):
    ax1.annotate(i[0],(i[1],-0.1),xycoords = 'data',
                 horizontalalignment = 'center',fontsize = 8)


x0 = big_data[['Ratio pECD','Ratio dI']]
x1 = big_data[['Integral', 'deltaI']]

tu.LDA_plot(x0,big_data, ax2,'B.')
tu.LDA_plot(x1,big_data, ax3,'C.')

# Get legend from ax1 and then plot it in axleg
h, l = ax2.get_legend_handles_labels()
axleg.legend(h,['Peak 0','Peak 1','Peak 2'], loc = 'center')
axleg.axis('off')

plt.savefig('filepath/Lab 2 - Keyser/MPhil Thesis/3-LDAP27.png',dpi=300)

#%%


ref_peaks = pd.read_pickle('filepath/Lab 2 - Keyser/MPhil Thesis/P27_refpeaks_standardized.pkl')

ref_peaks.dropna(inplace=True)
ref_peaks['Width'] = ref_peaks['right_x']-ref_peaks['left_x']

for i in ref_peaks.index.get_level_values(0):
        a = ref_peaks.loc[i,'minI_x']
        spacing = a.max()-a.min()
        ref_peaks.loc[(i,),'Spacing'] = spacing

print(f"nevents = {len(ref_peaks)/2}")

# https://stackoverflow.com/questions/36153410/how-to-create-a-swarm-plot-with-matplotlib
def simple_beeswarm(y, nbins=None):
    """
    Returns x coordinates for the points in ``y``, so that plotting ``x`` and
    ``y`` results in a bee swarm plot.
    """
    y = np.asarray(y)
    if nbins is None:
        nbins = len(y) // 6

    # Get upper bounds of bins
    x = np.zeros(len(y))
    ylo = np.min(y)
    yhi = np.max(y)
    dy = (yhi - ylo) / nbins
    ybins = np.linspace(ylo + dy, yhi - dy, nbins - 1)

    # Divide indices into bins
    i = np.arange(len(y))
    ibs = [0] * nbins
    ybs = [0] * nbins
    nmax = 0
    for j, ybin in enumerate(ybins):
        f = y <= ybin
        ibs[j], ybs[j] = i[f], y[f]
        nmax = max(nmax, len(ibs[j]))
        f = ~f
        i, y = i[f], y[f]
    ibs[-1], ybs[-1] = i, y
    nmax = max(nmax, len(ibs[-1]))

    # Assign x indices
    dx = 1 / (nmax // 2)
    for i, y in zip(ibs, ybs):
        if len(i) > 1:
            j = len(i) % 2
            i = i[np.argsort(y)]
            a = i[j::2]
            b = i[j+1::2]
            x[a] = (0.5 + j / 3 + np.arange(len(b))) * dx
            x[b] = (0.5 + j / 3 + np.arange(len(b))) * -dx

    return x

def plot_errorbars(x,column,ax):
    y = ref_peaks.mean().loc[[column]].to_list()
    y_err = np.sqrt(ref_peaks.var().loc[[column]].to_list()) # 1 sd
    ax.errorbar(x,y,y_err, marker = '_', markersize = 20, capsize = 20, color = 'k')

offset = 0

# Just get the first reference peak from each event
spacing_df = ref_peaks.loc[(slice(None), 0), :]

x_int = simple_beeswarm(ref_peaks['Integral'])
x_dep = simple_beeswarm(ref_peaks['deltaI'])+offset
x_wid = simple_beeswarm(ref_peaks['Width'])
x_spa = simple_beeswarm(spacing_df['Spacing'])


fig = plt.figure(figsize=(6.26,6))
gs = gridspec.GridSpec(2,1, height_ratios = (0.7,2),
                       left = 0.1, right = 0.99)
gs0 = gridspec.GridSpecFromSubplotSpec(1, 4, wspace = 0.7, hspace = 0.3,  
                                       subplot_spec=gs[1])
gs1 = gridspec.GridSpecFromSubplotSpec(1, 2, subplot_spec=gs[0], wspace = 0)

ax0 = fig.add_subplot(gs0[0,0])
ax1 = fig.add_subplot(gs0[0,1])
ax2 = fig.add_subplot(gs0[0,2])
ax3 = fig.add_subplot(gs0[0,3])

ax0.plot(x_int, ref_peaks['Integral'], '.', markersize = 5)
ax0.set_ylabel('Peak Charge Defecit (fC)', fontsize = 8)
ax0.set_ylim(0,10)
plot_errorbars(0,'Integral',ax0)
ax0.annotate('C.',(0,1.05),xycoords = 'axes fraction',
             fontsize = 9,fontweight = 'bold')

ax1.plot(x_dep, ref_peaks['deltaI'], '.', markersize = 5)
ax1.set_ylabel('Peak Depth (nA)', fontsize = 8)
ax1.set_ylim(0,0.5)
plot_errorbars(0,'deltaI',ax1)
ax1.annotate('D.',(0,1.05),xycoords = 'axes fraction',
             fontsize = 9,fontweight = 'bold')

ax2.plot(x_wid, ref_peaks['Width'],'.', markersize = 5)
ax2.set_ylabel('Width', fontsize = 8)
ax2.set_ylim(0,80)
plot_errorbars(0,'Width',ax2)
ax2.annotate('E.',(0,1.05),xycoords = 'axes fraction',
             fontsize = 9,fontweight = 'bold')

ax3.plot(x_spa, spacing_df['Spacing'],'.', markersize = 5)
ax3.set_ylabel('Spacing', fontsize = 8)
ax3.set_ylim(0,450)
plot_errorbars(0,'Spacing',ax3)
ax3.annotate('F.',(0,1.05),xycoords = 'axes fraction',
             fontsize = 9, fontweight = 'bold')

# Disable ticks
ax0.xaxis.set_major_locator(ticker.NullLocator())
ax1.xaxis.set_major_locator(ticker.NullLocator())
ax2.xaxis.set_major_locator(ticker.NullLocator())
ax3.xaxis.set_major_locator(ticker.NullLocator())


# harcoded to 2 and 16
# https://coolors.co/palettes/trending/bright
color2 = '#e9190f'
color16= '#f5b700'
def individualEventColor(start,stop,xarray,column, event,color,ax):
    ax.plot(xarray[start:stop],ref_peaks[column].loc[event],'.',c=color)
    
# For spacing will be weird because they are pairs
ax0.plot(x_int[2:4],ref_peaks['Integral'].loc[2],'.', c= color2, markersize = 5)
ax1.plot(x_dep[2:4],ref_peaks['deltaI'].loc[2],'.', c= color2, markersize = 5)
ax2.plot(x_wid[2:4],ref_peaks['Width'].loc[2],'.', c= color2, markersize = 5)
ax3.plot(x_spa[1],spacing_df['Spacing'].loc[2],'.', c= color2, markersize = 5)
ax0.plot(x_int[22:24],ref_peaks['Integral'].loc[16],'.', c= color16, markersize = 5)
ax1.plot(x_dep[22:24],ref_peaks['deltaI'].loc[16],'.', c= color16, markersize = 5)
ax2.plot(x_wid[22:24],ref_peaks['Width'].loc[16],'.', c= color16, markersize = 5)
ax3.plot(x_spa[11],spacing_df['Spacing'].loc[16],'.', c= color16, markersize = 5)

path = 'filepath/Lab 2 - Keyser/ANA_02_Jinbo 81 Carrier library/2022.10.09_27 mix/unfolded_p1/'

ax4 = fig.add_subplot(gs1[0,0])
ax5 = fig.add_subplot(gs1[0,1])

df = tu.eventLoader(path,2) # load event
left_xs = ref_peaks.loc[2,'left_x']
right_xs = ref_peaks.loc[2,'right_x']
tu.current_time(df,ax4,50,0.1,8,4)
for i in range(len(left_xs)): #plot reference peaks
    ax4.plot(df[left_xs[i]:right_xs[i]],c = color2)
ax4.annotate('A.',(0,1.05),xycoords = 'axes fraction',
             fontsize = 9, fontweight = 'bold')

df = tu.eventLoader(path,16) # load event
left_xs = ref_peaks.loc[16,'left_x']
right_xs = ref_peaks.loc[16,'right_x']
tu.current_time(df,ax5,50,0.1,8,4)
for i in range(len(left_xs)): #plot reference peaks
    ax5.plot(df[left_xs[i]:right_xs[i]],c=color16)
ax5.annotate('B.',(0,1.05),xycoords = 'axes fraction',
             fontsize = 9,fontweight = 'bold')

error_x = 431
ax4.annotate('', xy=(error_x, -0.25), xycoords='data', xytext=(error_x, -0.5), 
            arrowprops=dict(arrowstyle="->", color='k',linewidth=1.5,
                            #connectionstyle=f"bar,angle={angle},fraction = 0.2")) 
                            connectionstyle='arc3'))

plt.savefig('filepath/Lab 2 - Keyser/MPhil Thesis/3-RefPeaks.png', dpi=300)

