# -*- coding: utf-8 -*-

from numba import jit
import numpy as np
import time
import thesisutils2 as tu2


# can only detect one addition or subtraction
@jit
def scaling(ref_peaks, ref_pts, inp_array):

    fid_delta = inp_array[-2] - inp_array[1]
    ref_delta = ref_peaks[ref_pts[1]] - ref_peaks[ref_pts[0]]
   
    scaled_array = inp_array * ref_delta/fid_delta
    #print(f'{ref_delta/fid_delta}')
    scaled_array += ref_peaks[ref_pts[0]]-scaled_array[1]
    #print(scaled_array[-2])
   
    return scaled_array

@jit(nopython=True)
def FS_mainloop(inputPeaks, ref_max, band,rem):

    max_score = (-1000,-100,-10,-1) # score, ref_no, Fstart, Fstop
                                
    for ref_no in range(ref_max):
        
        # Skip if ref_no should be skipped
        if ((ref_no in rem) or (ref_no in rem+465)):
            pass
        
        # Otherwise consider it
        else:

            score = (0,-10,-1) # score2, Fstart, Fstop
            # which ref peak the second measured peak aligns to
            for first in np.array([0,1,2]):
                # which ref peak the second-to-last measure peak aligns to
                for last in np.array([-1,-2,-3]):
                    ref_peaks = ref_array[ref_no,:lengths[ref_no]]
                    scaled_input = scaling(ref_peaks,(first,last),inputPeaks)
                    
                    # slope will be 1 since that's what we scaled to
                    
                    score2 = 0 
                    for i in ref_peaks:
                        for j in scaled_input:
                            if -band<(i-j)<band:
                                score2 += 1
            
                    if score2>score[0]:
                        score = (score2,first,last)
            
            if score[0] > max_score[0]:
                max_score = (score[0],ref_no,score[1],score[2])
            
    return max_score


# =============================================================================
# USER INPUT
# =============================================================================
input_folder   = 'filepath'
output_folder  = 'filepath'
peak_height = 1.8   # for use with peak finding algorithm
frag_mins   = [0]
frag_maxs   = [len(tu2.MTaqI_locations)] # file with name frag_max-1 must exist
#frag_maxs    = [450]
ref_maxs    = [2*len(tu2.MTaqI_locations)]
#ref_maxs    = [450]
revs        = [False,True]
bands       = [2*i+1 for i in range(49)]
# None value a little hacky but must be less than -465!
# None value is -777
rems        = [np.array([-777]), np.array([69,119])]

# =============================================================================

ref_array, lengths = tu2.make_2D_array(tu2.MTaqI_locations)

#%%
for frag_min in frag_mins:
    for frag_max in frag_maxs:
        for ref_max in ref_maxs:
            for rev in revs:
                for band in bands:
                    for rem in rems:
                    
                        start = time.time()
    
                        inputstring = f'FSfrag_len={tu2.frag_len},frags={frag_min}to{frag_max},ref_max={ref_max},rev={rev},band={band},rem={rem}'
                        print(f'Beginning: {inputstring}')
                        
                        # initialize result list
                        rl_FS2 = [inputstring]
                                              
                        for frag_no in np.arange(frag_min,frag_max):
                            #if frag_no%50 == 0:
                            #    print(f'{frag_no} fragments categorized in {time.time()-start:.0f} seconds')
                                
                            inputPeaks = tu2.loadPeaks(frag_no,input_folder,peak_height)
                            
                            if rev == True:
                                inputPeaks = tu2.reverser(inputPeaks,tu2.frag_len*1.5)
                            
                            max_score = FS_mainloop(inputPeaks, ref_max, band, rem)
                                    
                            rl_FS2.append((frag_no,max_score))
                        
                        # Save results list
                        tu2.saveResultList(rl_FS2,inputstring, output_folder)
                        # Print accuracy
                        tu2.rl_accuracy(rl_FS2,inputstring)
                        
                        print(f'One condition: {time.time()-start:.0f} seconds ')
                    
