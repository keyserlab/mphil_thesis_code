#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#import Bio
from Bio import SeqIO, Restriction
# Import restriction enyzmes to be used
from Bio.Restriction import (AgeI,ApoI,BamHI,BbsI,BclI,BmtI,BsaI,BsiWI,BsrGI,BstEII,BstZ17I,DraIII,
                             EagI,EcoRI,EcoRV,HindIII,KpnI,MfeI,MluI,NcoI,NheI,NotI,NruI,NsiI,PstI,
                             PvuI,PvuII,SacI,SalI,SbfI,ScaI,SpeI,SphI,SspI,StyI)
#from Bio.Restriction import *
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.gridspec as gridspec
import numpy as np
import inspect
import os

# Path to current file
path = inspect.getfile(inspect.currentframe())
# Path to directory that file is in
directory = os.path.realpath(os.path.dirname(path))
# Change directory 
os.chdir(directory)

import thesisutils as tu

#%%
class Methyltransferase:
    def __init__(self, name, count):
        self.name  = name
        self.count = count


# Number of MT labeling sites
BseCI = Methyltransferase('BseCI',tu.MG1655.count('ATCGAT'))
BseCI_counts = tu.MG1655.count('ATCGAT')
# sum of 1 + 2 + ... + 53 = 1431
print(f'There are {BseCI.count} BseCI labeling sites')
# This means I can make 52 tags based off number of tags: avg. seq length of 90kb
# Probably need three different length levels to get an average length of 30k

MTaqI = Methyltransferase('MTaqI',tu.MG1655.count('TCGA'))
MTaqI_counts = tu.MG1655.count('TCGA')
# sum of 1 + 2 + ... + 176 = 15576
print(f'There are {MTaqI.count} MTaqI labeling sites')
# This means I can make 175 tags based off number of tags: avg. seq length of 26 kb


def plotgrams(seqs, MT,exp_bw):
    frag_ends   = np.arange(0,len(tu.MG1655),tu.frag_len) 
    lam         = MT.count/(len(frag_ends)) # lambda for Poisson distribution
    
    spacing     = []
    position    = []
    running_sum = 0
    for i in range(len(seqs)):
        running_sum += len(seqs[i])
        position.append(running_sum)
        spacing.append(len(seqs[i]))
    print(f'SANITY CHECK: Sequence is {running_sum} bp long')
    print(f'SUGGESTION: Max spacing is {max(spacing)}')
    
    fig = plt.figure(figsize=(6.26,6.26))
    gs0 = gridspec.GridSpec(2,2, wspace=0.25,hspace = 0.5, left = 0.08, right = 0.99, bottom = 0.2)
    gsleg = gridspec.GridSpec(1,1, bottom = 0, top = 0.1)
    

    ax0 = fig.add_subplot(gs0[0,0])
    ax1 = fig.add_subplot(gs0[0,1])
    ax2 = fig.add_subplot(gs0[1,0])
    ax3 = fig.add_subplot(gs0[1,1])
    axleg = fig.add_subplot(gsleg[0,0])
    
    #ax0.plot(position, spacing)
    ax0.bar(position, spacing, width = tu.frag_len)
    ax0.set_xlabel('Position (bp)')
    ax0.set_ylabel('Distance to next label (bp)')
    ax0.ticklabel_format(style = 'scientific', scilimits= (0,0))
    ax0.annotate('A.',(0,1.13),xycoords = 'axes fraction',
                fontsize = 9, fontweight = 'bold')

    
    # Histogram of spacing between fragments
    # Exponential distribution
    bin_width = exp_bw
    bins = np.arange(1,(max(spacing)//bin_width+1)*bin_width,bin_width)
    counts, bins = np.histogram(spacing, bins=bins, density = True)
    ax1.hist(bins[:-1], bins, weights=counts, label = 'Histogram')
    ax1.set_xlabel('Distance to next label (bp)')
    ax1.set_ylabel('Frequency')
    bins_y =  []
    for i in bins:
        bins_y.append(counts[0]*np.exp(-lam*i/10000))
    ax1.plot(bins+bin_width/2,bins_y,'-xr',label = 'Exponential Distribution')
    ax1.legend().set_visible(False)
    ax1.ticklabel_format(style = 'scientific', scilimits= (0,0))
    ax1.annotate('B.',(0,1.13),xycoords = 'axes fraction',
                fontsize = 9, fontweight = 'bold')
    
    counts, bins = np.histogram(position, bins=frag_ends)
    ax2.hist(bins[:-1], bins, weights=counts)
    ax2.set_xlabel('Fragment start (bp)')
    ax2.set_ylabel(f'# {MT.name} Labels per Fragment')
    ax2.annotate('C.',(0,1.13),xycoords = 'axes fraction',
                fontsize = 9, fontweight = 'bold')

    
    bin_width = 1
    bins = np.arange(0,counts.max()+bin_width,bin_width) # bins that include most labels per fragment
    counts, bins = np.histogram(counts, bins=bins, density = True)
    ax3.hist(bins[:-1], bins, weights = counts, label = 'Histogram')
    # Poisson dist.
    bins_y =  []
    for i in bins:
        print(type(i))
        bins_y.append(lam**i*np.exp(-lam)/np.math.factorial(i))
    ax3.plot(bins+bin_width/2,bins_y,'-xr', label = 'Poisson Distribution')
    ax3.set_xlabel(f'# {MT.name} Labels per Fragment')
    ax3.set_ylabel('Frequency')
    ax3.legend().set_visible(False)
    ax3.ticklabel_format(style = 'scientific', scilimits= (0,2))
    ax3.annotate('D.',(0,1.13),xycoords = 'axes fraction',
                fontsize = 9, fontweight = 'bold')
    
    fig.align_ylabels([ax0,ax2])
    fig.align_ylabels([ax1,ax3])
    
    h, l = ax3.get_legend_handles_labels()
    axleg.legend(h,['MG1655 Genome','Fitted Distribution'], loc = 'lower center')
    axleg.axis('off')
    
    #plt.savefig(f'filepath/Lab 2 - Keyser/MPhil Thesis/6-MT_{MT.name}_distr.png', dpi=300)
    

plotgrams(Restriction.BseCI.catalyze(tu.MG1655, linear = True),BseCI,1000)
plotgrams(Restriction.TaqI.catalyze(tu.MG1655, linear = True),MTaqI,100)


#%%

def plotgrams_2(seqs, MT,exp_bw, axa, axb, axc):
    frag_ends   = np.arange(0,len(tu.MG1655),tu.frag_len) 
    lam         = MT.count/(len(frag_ends)) # lambda for Poisson distribution
    
    spacing     = []
    position    = []
    running_sum = 0
    for i in range(len(seqs)):
        running_sum += len(seqs[i])
        position.append(running_sum)
        spacing.append(len(seqs[i]))
    print(f'SANITY CHECK: Sequence is {running_sum} bp long')
    print(f'SUGGESTION: Max spacing is {max(spacing)}')
    
    # Histogram of spacing between fragments
    # Exponential distribution
    bin_width = exp_bw
    bins = np.arange(1,(max(spacing)//bin_width+1)*bin_width,bin_width)
    counts, bins = np.histogram(spacing, bins=bins, density = True)
    axa.hist(bins[:-1], bins, weights=counts, label = 'Histogram')
    axa.set_xlabel('Label spacing (bp)')
    axa.set_ylabel('Frequency')
    bins_y =  []
    for i in bins:
        bins_y.append(counts[0]*np.exp(-lam*i/10000))
    axa.plot(bins+bin_width/2,bins_y,'-xr',label = 'Exponential Distribution')
    axa.legend().set_visible(False)
    axa.ticklabel_format(style = 'scientific', scilimits= (0,0))

    
    counts, bins = np.histogram(position, bins=frag_ends)
    axb.hist(bins[:-1], bins, weights=counts)
    axb.set_xlabel('Fragment start (bp)')
    axb.set_ylabel(f'{MT.name} Labels per Fragment')
    
    bin_width = 1
    bins = np.arange(0,counts.max()+bin_width,bin_width) # bins that include most labels per fragment
    counts, bins = np.histogram(counts, bins=bins, density = True)
    axc.hist(bins[:-1], bins, weights = counts, label = 'Histogram')
    # Poisson dist.
    bins_y =  []
    for i in bins: 
        bins_y.append(lam**i*np.exp(-lam)/np.math.factorial(i))
    axc.plot(bins+bin_width/2,bins_y,'-xr', label = 'Poisson Distribution')
    axc.set_xlabel(f'{MT.name} Labels per Fragment')
    axc.set_ylabel('Frequency')
    axc.legend().set_visible(False)
    axc.ticklabel_format(style = 'scientific', scilimits= (0,2))

    
    
fig = plt.figure(figsize=(6.26,8))
#gs0 = gridspec.GridSpec(3,2, wspace=0.25,hspace = 0.5, left = 0.08, right = 0.99, bottom = 0.2)
gs0 = gridspec.GridSpec(3,2, wspace=0.3,hspace = 0.5, 
                        left = 0.1, right = 0.8, bottom = 0.1, top = 0.95)
#gsleg = gridspec.GridSpec(1,1, bottom = 0, top = 0.1)
gsleg = gridspec.GridSpec(1,1, left = 0.85, right = 0.98)


ax0 = fig.add_subplot(gs0[0,0])
ax1 = fig.add_subplot(gs0[0,1])
ax2 = fig.add_subplot(gs0[1,0])
ax3 = fig.add_subplot(gs0[1,1])
ax4 = fig.add_subplot(gs0[2,0])
ax5 = fig.add_subplot(gs0[2,1])
axleg = fig.add_subplot(gsleg[0,0])

plotgrams_2(Restriction.BseCI.catalyze(tu.MG1655, linear = True),BseCI,1000, ax0,ax2,ax4)
plotgrams_2(Restriction.TaqI.catalyze(tu.MG1655,  linear = True),MTaqI,100,  ax1,ax3,ax5)
    
ax0.annotate('A. BseCI',(0,1.13),xycoords = 'axes fraction',
            fontsize = 9, fontweight = 'bold')
ax2.annotate('B. BseCI',(0,1.13),xycoords = 'axes fraction',
            fontsize = 9, fontweight = 'bold')
ax4.annotate('C. BseCI',(0,1.13),xycoords = 'axes fraction',
            fontsize = 9, fontweight = 'bold')
ax1.annotate('D. MTaqI',(0,1.13),xycoords = 'axes fraction',
            fontsize = 9, fontweight = 'bold')
ax3.annotate('E. MTaqI',(0,1.13),xycoords = 'axes fraction',
            fontsize = 9, fontweight = 'bold')
ax5.annotate('F. MTaqI',(0,1.13),xycoords = 'axes fraction',
            fontsize = 9, fontweight = 'bold')

ax4.xaxis.set_ticks(np.arange(0, 12, 2))

fig.align_ylabels([ax0,ax2,ax4])
fig.align_ylabels([ax1,ax3,ax5])

h, l = ax0.get_legend_handles_labels()
axleg.legend(h,['MG1655\nGenome','Fitted\nDistribution'], loc = 'center')
axleg.axis('off')
    
plt.savefig('filepath/Lab 2 - Keyser/MPhil Thesis/6-MT_Comb_distr.png', dpi=300)

#%% Simulate fragment (generatefragments was used for genomic library
#                      this is just for plotting)

def simulate_fragment(frag_no):
    #scaling = (np.random.rand()-0.5) # a scaling parameter
    scaling = -0.4
    current = np.ones(tu.frag_len)*1*scaling-1
    flex    = scaling/3*np.sin(np.arange(tu.frag_len)*np.pi/tu.frag_len+np.random.rand())
    current += flex
    peak_len = 200
    peak_x = np.linspace(-peak_len/2,peak_len/2,peak_len)
    peak_y = -1*np.exp(-peak_x**2/(10*peak_len))
    for i in tu.MTaqI_locations[frag_no]:
        # add jitter
        i+= int(100*(np.random.rand()-0.5)) 
        for j in np.arange(-peak_len/2,peak_len/2):
            if i+j<tu.frag_len:
                j = int(j)
                current[i+j] = current[i+j]+peak_y[int(j+peak_len/2)]
            else:
                pass
    current = current[::10] # take every tenth measurement to mimick refresh rate
    # add negative slope
    for i in range(len(current)):
        current[i] += -0.2*i/1000
    # add pore baseline
    current = np.append(np.zeros(int(100*np.random.rand())+50),current)
    current = np.append(current,np.zeros(int(100*np.random.rand())+50))
    # add noise
    for i in range(len(current)):
        current[i]+= 0.08*np.random.rand()-0.04
        
    return current

n_lines = 3
fig = plt.figure(figsize=(6.26,4))
gs = gridspec.GridSpec(n_lines,2)

frag_no = 0

ax_dict = {'ax'+str(i):fig.add_subplot(gs[i%n_lines,i//n_lines]) for i in range(n_lines*2)}

for ax in ax_dict.values():
    ax.set_ylim(-6,0.5)

for i in range(n_lines*2):
    tu.current_time(simulate_fragment(frag_no),ax_dict['ax'+str(i)],50,0.1,8,4, fracxoff = 0.55)
    
plt.savefig('filepath/Lab 2 - Keyser/MPhil Thesis/6-Simulatedfrags-2.png', dpi=300)

#%% 5 errors

#print(tu.MTaqI_locations[0])

offset = 2000
# From MT_second_moment.py


    
counts = []
for i in tu.MTaqI_locations:
    counts.append(len(i))
    
counts2 = [len(i) for i in tu.MTaqI_locations]

counts_tuples = list(enumerate(counts))
    
n_lines = 6

fig=plt.figure(figsize=(6.26,n_lines*0.4), dpi=300)
gs = gridspec.GridSpec(n_lines,3, hspace = 0.5, left = 0, right = 0.99)

# Goes down rows, then switches columns
ax_dict = {'ax'+str(i):fig.add_subplot(gs[i%n_lines,i//n_lines]) for i in range(n_lines*3)}

for i in ax_dict:
    tu.setup(ax_dict[i],0,tu.frag_len)
    
f10_ax = [0,1,2,3,4,6,7,8,9,10] #axes to plot first 10 events
for idx,peaks in enumerate(tu.MTaqI_locations[0:len(f10_ax)]):
    print(idx)
    for peak in peaks:
        ax_dict['ax'+str(f10_ax[idx])].plot(peak,0.25, marker = '.',markersize = 3,color = tu.palette[5])
        
# Commononly misidentified events
for peak in tu.MTaqI_locations[69]:
    ax_dict['ax'+str(5)].plot(peak,0.25, marker = '.',markersize = 3,color = tu.palette[5])
for peak in tu.MTaqI_locations[119]:
    ax_dict['ax'+str(11)].plot(peak,0.25, marker = '.',markersize = 3,color = tu.palette[5])
    

# Transformed input arrays
arrayori = tu.MTaqI_locations[0].copy()
arrayrev = [tu.frag_len-i for i in arrayori[::-1]]
arrayjit = [i+(np.random.rand()*100-50) for i in arrayori]
arraytra = [i+offset for i in arrayori]
arraysca = [i*1.2 for i in arrayori]
arraydel = arrayori.copy()
for i in [15,20]: # 15 blends in, 20 (really 19) is visible
    del arraydel[i]
for i in [5000, 8500]:
    arraydel.append(i)
arraydel.sort()

listofarrays = [arrayori,arraytra,arraydel,arrayjit,arrayrev,arraysca]

for idx,val in enumerate(listofarrays):
    for i in val:
        ax_dict['ax'+str(idx+12)].plot(i,0.25, marker = '.',markersize = 3,color = tu.palette[5])
    if idx not in [2,4]:
        ax_dict['ax'+str(idx+12)].plot(val[-3],0.25, marker = '.',markersize = 3,color = tu.palette[1])
        ax_dict['ax'+str(idx+12)].plot(val[11],0.25, marker = '.',markersize = 3,color = tu.palette[3])
    elif idx == 2:
        ax_dict['ax'+str(idx+12)].plot(val[-4],0.25, marker = '.',markersize = 3,color = tu.palette[1])
        ax_dict['ax'+str(idx+12)].plot(val[11],0.25, marker = '.',markersize = 3,color = tu.palette[3])
    elif idx == 4:
        ax_dict['ax'+str(idx+12)].plot(val[2],0.25, marker = '.',markersize = 3,color = tu.palette[1])
        ax_dict['ax'+str(idx+12)].plot(val[-12],0.25, marker = '.',markersize = 3,color = tu.palette[3])
        
ax_dict['ax0'].annotate('A.',(0,0.6), xycoords = 'axes fraction',
                        fontsize = 9, fontweight = 'bold')
ax_dict['ax5'].annotate('B.',(0,0.6), xycoords = 'axes fraction',
                        fontsize = 9, fontweight = 'bold')


texts = ['C. Untransformed','D. Translation','E. Indel',
         'F. Jitter','G. Reversal','H. Scaling']

for idx,val in enumerate(listofarrays):
    ax_dict['ax'+str(idx+12)].annotate(texts[idx],(0,0.6), xycoords = 'axes fraction',
                                    fontsize = 9, fontweight = 'bold')

# Arrows to point at indels
ax_dict['ax14'].annotate('', xy=(arrayori[15],1), xycoords='data', xytext=(arrayori[15],0.25), 
                        arrowprops=dict(arrowstyle="<-", color='k',linewidth=1.5,
                                        connectionstyle="arc3")) 
ax_dict['ax14'].annotate('', xy=(arrayori[21],1), xycoords='data', xytext=(arrayori[21],0.25), 
                        arrowprops=dict(arrowstyle="<-", color='k',linewidth=1.5,
                                        connectionstyle="arc3")) 
ax_dict['ax14'].annotate('', xy=(5000,1), xycoords='data', xytext=(5000,0.25), 
                        arrowprops=dict(arrowstyle="<-", color='k',linewidth=1.5,
                                        connectionstyle="arc3")) 
ax_dict['ax14'].annotate('', xy=(8500,1), xycoords='data', xytext=(8500,0.25), 
                        arrowprops=dict(arrowstyle="<-", color='k',linewidth=1.5,
                                        connectionstyle="arc3")) 


#plt.savefig('filepath/Lab 2 - Keyser/MPhil Thesis/6-Fragments.png', dpi=300)