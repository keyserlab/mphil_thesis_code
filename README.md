# MPhil_Thesis


##  Explanation of Files
| File Name | Purpose |
| --------- | ------- |
|1_Introduction.py | Graphs used in Introduction |
|2_Artemis 4.py    | Graphs related to ARTEMIS-4 dataset |
|3_Probe27.py      | Graphs related to Probes-012 dataset |
|4-EndLabels5.py   | Graphs related to End Labelled-5 dataset |
|5-CosineSimilarity.py     | Graphs used in section 2.4 Cosine Similarity |
|6-GenomicLibrary.py       | Graphs used in section 3.1 A Genomic Library |
|6.5 Restriction Enzymy.py | Fig. 13: Simulated Restriction Enzyme Cutting |
|7-Methods.py          | Conceptual figures for all 3 genomic library methods |
|8-GLEvaluation.py     | Performance and accuracy of all 3 genomic library methods |
|9-Error Correction.py | Fig.24: Barcode Space |
|Nanopycker_thesis.py  | Fork of Nanopycker with cosine similarity method implemented |
|dotmatrix.py       | Dot matrix method for recognizing genomic library |
|fiduciaryscan.py   | Fiduciary scan method for recognizing genomic library |
|gaussianpenalty.py | Gaussian penalty method for recognizing genomic library |
|thesisutils.py     | Common functions for graphing |
|thesisutils2.py    | Common functions for genomic library methods |

