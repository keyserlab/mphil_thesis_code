# -*- coding: utf-8 -*-

import numpy as np
import nptdms
import matplotlib.pyplot as plt
import thesisutils2 as tu2

def simulate_fragment(frag_no):
    #scaling = (np.random.rand()-0.5) # a scaling parameter
    scaling = -0.4
    current = np.ones(tu2.frag_len)*1*scaling-1
    flex    = scaling/3*np.sin(np.arange(tu2.frag_len)*np.pi/tu2.frag_len+np.random.rand())
    current += flex
    peak_len = 200
    peak_x = np.linspace(-peak_len/2,peak_len/2,peak_len)
    peak_y = -1*np.exp(-peak_x**2/(10*peak_len))
    for i in tu2.MTaqI_locations[frag_no]:
        # add jitter
        i+= int(100*(np.random.rand()-0.5)) 
        for j in np.arange(-peak_len/2,peak_len/2):
            if i+j<tu2.frag_len:
                j = int(j)
                current[i+j] = current[i+j]+peak_y[int(j+peak_len/2)]
            else:
                pass
    current = current[::10] # take every tenth measurement to mimick refresh rate
    # add negative slope
    for i in range(len(current)):
        current[i] += -0.2*i/1000
    # add pore baseline (this also adds translation)
    current = np.append(np.zeros(int(100*np.random.rand())+50),current)
    current = np.append(current,np.zeros(int(100*np.random.rand())+50))
    # add noise
    for i in range(len(current)):
        current[i]+= 0.08*np.random.rand()-0.04
        
    return current


# =============================================================================
# USER INPUT
# =============================================================================
# Make sure below exists
output_folder   = 'filepath'
save = True  # save generated fragments
plot = False # plot generated fragments    
# =============================================================================

for frag_no in range(len(tu2.MTaqI_locations)):
    current = simulate_fragment(frag_no)
    
    if save:
        with nptdms.TdmsWriter(output_folder
                               +'Separateevent'+' '*(6-len(str(frag_no)))
                               +str(frag_no)+'.tdms') as tdms_writer:
            data_array = current
            channel = nptdms.ChannelObject('simulated group', 'simulated channel', data_array)
            tdms_writer.write_segment([channel])

    if plot:
        plt.figure()
        plt.plot(np.arange(len(current)),current)
        plt.title(f'Simulated 10kb Fragment No. {frag_no} of MTaqI Labeled Genome')
