# -*- coding: utf-8 -*-

from numba import jit
import numpy as np
import time
import thesisutils2 as tu2

# https://stackoverflow.com/questions/70613681/numba-compatible-numpy-meshgrid
@jit(nopython=True)
def meshgrid(x, y):
    xx = np.empty(shape=(y.size, x.size), dtype=x.dtype)
    yy = np.empty(shape=(y.size, x.size), dtype=y.dtype)
    for i in range(y.size):     # iterate over row
        for j in range(x.size): # iterate over column
                xx[i,j] = x[j]  # change to x[k] if indexing xy
                yy[i,j] = y[i]  # change to y[j] if indexing xy
    return xx,yy

# Given a list of peaks compute similarity to fragments
@jit(nopython=True)
def DM_mainloop(peaks, ref_max, band, rem):
    band_start = int(-(band-1)/2)
    band_stop  = int( (band+1)/2)
    
    # create test array
    t = np.zeros((peaks[-1]+1)) #just long enough so last entry is the last peak
    t[peaks] +=1

    max_score = (-100,-10, -1) # score, ref_no, boffset
    for ref_no in range(ref_max):
        
        # Skip if ref_no should be skipped
        if ((ref_no in rem) or (ref_no in rem+465)):
            pass
        
        # Otherwise consider it
        else:
            # create reference array
            r = np.zeros(1000)
            for k in ref_array[ref_no,:lengths[ref_no]]: # comma needed to get slice of 1 row!
                r[int(k/10)] += 1
            r[r>1] = 1   # can get numbers higher than one if int(k/10) is repeated
            
            tt,rr = meshgrid(r,t) # use jit meshgrid function
            
            # add so intersections constructively interfere
            ss = tt+rr
            # find just the intersections
            s2 = ss == 2
            # s2 = s2*1 not needed?
        
            score   = 0 # similarity score
            boffset = 0 # best offset
                                            
            # for each band
            for i in np.arange(-max(s2.shape),max(s2.shape)):
                score2 = 0
                # for each off-diagonal in band
                for offd in np.arange(band_start,band_stop):
                    score2 += np.sum(np.diag(s2,k=i+offd))
                if score2>score:
                    score   = score2
                    boffset = i
    
            # compare if similarity for fragment is best                
            if score>max_score[0]:
                max_score = (score, ref_no, boffset)
            
    diagnostics = (tt,rr,s2,t,r)
            
    return max_score, diagnostics


# =============================================================================
# USER INPUT
# =============================================================================
input_folder   = 'filepath'
output_folder  = 'filepath'
peak_height = 1.8   # for use with peak finding algorithm
frag_mins   = [0]
frag_maxs   = [len(tu2.MTaqI_locations)] # file with name frag_max-1 must exist
#frag_maxs   = [30]
ref_maxs    = [2*len(tu2.MTaqI_locations)]
#ref_maxs    = [930]
revs        = [False,True]
bands       = [11]
scales      = [.95,.96,.97,.98,.99,1.01,1.02,1.03,1.04,1.05]
rems        = [np.array([69, 119])]
# =============================================================================

ref_array, lengths = tu2.make_2D_array(tu2.MTaqI_locations)
        
#%%
        
for frag_min in frag_mins:
    for frag_max in frag_maxs:
        for ref_max in ref_maxs:
            for rev in revs:
                for band in bands:
                    for scale in scales:
                        for rem in rems:
                        
                            start = time.time()
                                                    
                            inputstring = f'DMfrag_len={tu2.frag_len},frags={frag_min}to{frag_max},ref_max={ref_max},rev={rev},band={band},scale={scale},rem={rem}'
                            print(inputstring)
                            
                            # initialize result list
                            rl_DM = [inputstring]
                            
                            for frag_no in np.arange(frag_min,frag_max):
                                
                                if frag_no%50 == 0:
                                    print(f'{frag_no} fragments categorized in {time.time()-start:.0f} seconds')
                                    
                                inputPeaks = tu2.loadPeaks(frag_no,input_folder,peak_height)
                                
                                if rev == True:
                                    # divide by 10 otherwise fragment gets really long and scales the whole operation
                                    inputPeaks = tu2.reverser(inputPeaks,inputPeaks[-1]+10)
                                    
                                scalePeaks = (inputPeaks*scale).round().astype('int64') 
                                    
                                max_score, _ = DM_mainloop(scalePeaks, ref_max, band,rem)
                                        
                                rl_DM.append((frag_no,max_score))
                            
                            # Save results list
                            tu2.saveResultList(rl_DM,inputstring, output_folder)
                            # Print accuracy
                            tu2.rl_accuracy(rl_DM,inputstring)
                            
                            print(f'One set: {time.time()-start:.0f} seconds ')
                        
