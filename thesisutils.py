#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from Bio import SeqIO, Restriction
# Import restriction enyzmes to be used
from Bio.Restriction import (AgeI,ApoI,BamHI,BbsI,BclI,BmtI,BsaI,BsiWI,BsrGI,BstEII,BstZ17I,DraIII,
                             EagI,EcoRI,EcoRV,HindIII,KpnI,MfeI,MluI,NcoI,NheI,NotI,NruI,NsiI,PstI,
                             PvuI,PvuII,SacI,SalI,SbfI,ScaI,SpeI,SphI,SspI,StyI)
import numpy as np
from nptdms import TdmsFile
import matplotlib as mpl
import matplotlib.ticker as ticker
import pandas as pd
import seaborn as sns
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.model_selection import cross_val_score
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis 

# common functions

# Load event to a dataframe
def eventLoader(filepath,event):
    tdms_file = TdmsFile.read(filepath
                              +'Separateevent'+' '*(6-len(str(event)))
                              +str(event)+'.tdms')

    df = tdms_file.as_dataframe(time_index=False, absolute_time=False,
                                scaled_data=True).iloc[:,0]
    
    return df


def LDA_plot(x,big_data,ax,label):
    y = big_data['Peak_ID']

    # for some weird reason y in not an interger?
    y=y.astype('int')

    model = LinearDiscriminantAnalysis(n_components=2)
    model.fit(x, y)

    #https://machinelearningmastery.com/repeated-k-fold-cross-validation-with-python/
    #https://scikit-learn.org/stable/modules/cross_validation.html#stratification
    #Define method to evaluate model
    cv = RepeatedStratifiedKFold(n_splits=10, n_repeats=3, random_state=1)

    #evaluate model
    scores = cross_val_score(model, x, y, scoring='accuracy', cv=cv, n_jobs=-1)
    print(np.mean(scores))

    LDA_peaks = model.fit(x, y).transform(x)
    print(LDA_peaks.shape)
    
    palette = ['#33a02c','#1f78b4','#e31a1c','#ff7f00','#000000']
    
    sns.scatterplot(x = LDA_peaks[:,0],
                    y = LDA_peaks[:,1],
                    hue = big_data['Peak_ID'],palette=palette[:5], size = 1, ax = ax)
    
    #ax.set_title(f'{np.mean(scores):.1%}', fontsize = 9)
    ax.set_xlabel('LDA_1')
    ax.set_ylabel('LDA_2')
    ax.annotate(f"{label} Accuracy = {np.mean(scores):.1%}",(0,1.05),xycoords = 'axes fraction',
                fontsize = 9, fontweight = 'bold')
    ax.legend().set_visible(False)
    ax.axis('equal')
    

# Dot plot axis setup    
def setup(ax,offset,frag_len):
    ax.spines['right'].set_color('none')
    ax.spines['left'].set_color('none')
    ax.yaxis.set_major_locator(ticker.NullLocator())
    ax.spines['top'].set_color('none')
    ax.xaxis.set_ticks_position('bottom')
    #ax.tick_params(which='major', width=1.00)
    #ax.tick_params(which='major', length=5)
    #ax.tick_params(which='minor', width=0.75)
    #ax.tick_params(which='minor', length=2.5)
    #ax.tick_params(bottom=False)
    ax.set_xlim(0, frag_len+offset)
    ax.set_ylim(0, 1)
    ax.patch.set_alpha(0.0)
    #ax.xaxis.set_major_locator(ticker.LinearLocator(3))
    ax.xaxis.set_major_locator(ticker.NullLocator())
    
    
# Plot an event with scale arrows
def current_time(df, ax, roundx,roundy,fracx,fracy, **kwargs):
    """

    Parameters
    ----------
    df : Event to plot
    ax : Axes to plot on
    roundx : Round x to nearest
    roundy : Rount y to nearest
    fracx : Fraction of x axis to extend arrow
    fracy : Fraction of y axis to extend arrow

    Returns
    -------
    None.

    """
    # Choose color and plot
    color = '#1f78b4'
    if 'color' in kwargs:
        color = kwargs['color']
    ax.plot(df, color = color)
    ax.axis('off')
    
    xr = ax.get_xlim()[1]-ax.get_xlim()[0] # domain
    yr = ax.get_ylim()[1]-ax.get_ylim()[0] # range
    
    arrow_origin_x = ax.get_xlim()[0]                                      # us
    arrow_origin_y = int(round(yr/fracy/2/roundx)*roundx)+ax.get_ylim()[0] # nA
    arrow_up       = max(round(yr/fracy/roundy)*roundy, roundy)            # nA
    arrow_right    = max(int(round(xr/fracx/roundx)*roundx),roundx)        # us
    
    # Add fraction offset to modify origin of arrows
    if 'fracxoff' in kwargs:
        arrow_origin_x += kwargs['fracxoff']*xr
        
    if 'fracyoff' in kwargs:
        arrow_origin_y += kwargs['fracyoff']*yr
    
    print(arrow_right,arrow_up,arrow_origin_x,arrow_origin_y)

    offx = 0   # needed to get arrows to overlap
    offy = 0   # needed to get arrows to overlap
    
    # Create arrows and labels
    ax.annotate('', xy=(arrow_origin_x, arrow_origin_y-offy), xycoords='data', xytext=(arrow_origin_x, arrow_origin_y-offy+arrow_up), 
                            arrowprops=dict(arrowstyle="<-", color='k',linewidth=1.5,relpos=(0,0)))
    ax.annotate(f'{arrow_up:.2f} nA', xy=(arrow_origin_x+arrow_right/4, arrow_origin_y+arrow_up/2), xycoords='data',
                fontsize = 8)
    ax.annotate('', xy=(arrow_origin_x-offx, arrow_origin_y), xycoords='data', xytext=(arrow_origin_x+arrow_right-offx, arrow_origin_y), 
                            arrowprops=dict(arrowstyle="<-", color='k',linewidth=1.5, relpos=(0,0)))
    ax.annotate(f'{arrow_right/1000} ms', xy=(arrow_origin_x+arrow_right, arrow_origin_y-offy), xycoords='data',
                fontsize = 8)
    
    
# Set font sizes of tick marks and labels
mpl.rcParams['xtick.labelsize'] = 8
mpl.rcParams['ytick.labelsize'] = 8
mpl.rcParams['axes.labelsize']  = 8
mpl.rcParams['legend.fontsize'] = 8


# color palette for plots
palette = ['#b2df8a','#33a02c','#a6cee3','#1f78b4',
           '#fb9a99','#e31a1c','#fdbf6f','#ff7f00','#000000','#fff000']

#%% Genomic Library project

# This holds a sequence (as a Seq object) with additional annotation
# including an identifier, name and description. 
MG1655_record = SeqIO.read('filepath/Lab 2 - Keyser/Methyltransferase/Escherichia coli str. K-12 substr. MG1655 strain K-12 chromosome, complete genome.fasta',
                    "fasta")

# Make a Seq.Seq type
MG1655 = MG1655_record.seq

frag_len = 10000
frag_ends   = np.arange(0,len(MG1655)+frag_len,frag_len)

# Sequences generated by uniform cutting
uniform_seqs = []
for i in range(len(frag_ends)-1): #minus one to avoid edge case
    uniform_seqs.append(MG1655[frag_ends[i]:frag_ends[i+1]])
    
MTaqI_locations = []    
for i in uniform_seqs:
    MTaqI_locations.append(Restriction.TaqI.search(i))

#%% Artemis-4

# Wrap data processing into one function
def wrapperA4(filepath, *args):

    intdf = pd.read_pickle(filepath+'apeak1234.pkl')
    
    # Drop events that had no integrals because it will mess up ratios part    
    intdf.dropna(inplace = True)        

    for i in intdf.index.get_level_values(0).unique():
        # Rank based on left_x
        a = intdf.loc[i,'left_x'].rank().astype(int)
        # MUST DO Set index so it can be assigned to intdf properly
        a.index = pd.MultiIndex.from_product([(i,), a.index])
        intdf.loc[(i,),'Peak_Pos_Ord'] = a 
        
    # Make current index into columns
    intdf.reset_index(inplace=True)
    # Sort by ascending order of position 
    intdf = intdf.sort_values(by=['Event Index', 'Peak_Pos_Ord'], ascending=True)
    intdf['Peak_Pos_Ord_Col'] = intdf['Peak_Pos_Ord']
    # Make index from columns
    intdf.set_index(['Event Index', 'Peak_Pos_Ord'], inplace=True)

    
    for i in intdf.index.get_level_values(0).unique():
        # Rank based on integral 
        a = intdf.loc[i,'Integral'].rank()
        # MUST DO Set index so it can be assigned to intdf properly
        a.index = pd.MultiIndex.from_product([(i,), a.index])
        intdf.loc[(i,),'Peak_ECD_Ord'] = a 
        
        b = intdf.loc[i,'Peak_ECD_Ord']
        #b2 = intdf.loc[i,'Peak_Pos_Ord']
        
        # must be in order of peaks
        delta = []
        for idx,val in enumerate(b):
            try:
                delta.append(b[idx+1]-b[idx])
                #print('Appending',b[idx+1]-b[idx])
            except:
                KeyError
                
        sigma_delta = 0
        for j in delta: # use different index than higher level for loop
            sigma_delta += j
        #print(i, sigma_delta)
                
        # Now change Peak_ID into a user labeled Peak_ID
        rank_to_label = {4:args[0],3:args[1],2:args[2],1:args[3]}
        rank_to_label_2 = {1:args[0],2:args[1],3:args[2],4:args[3]}
        
        if sigma_delta > 0:
            direction = 1
            intdf.loc[(i,),'Peak_ID'] = intdf['Peak_Pos_Ord_Col'].map(rank_to_label)
        else:
            direction = 0
            intdf.loc[(i,),'Peak_ID'] = intdf['Peak_Pos_Ord_Col'].map(rank_to_label_2)
        # Copy index from earlier
        intdf.loc[(i,),'Direction'] = pd.Series(direction,index = a.index)
    
        # Calculate ratio pECD and ratio dI
        stan_pECD = intdf.loc[(i,)].loc[intdf.loc[(i,)]['Peak_ID']==args[0]]['Integral'].iat[0]
        stan_dI   = intdf.loc[(i,)].loc[intdf.loc[(i,)]['Peak_ID']==args[0]]['deltaI'].iat[0]
        
        # No idea why you need to add in .values here
        dummy = intdf.loc[(i,),'Integral']/stan_pECD
        dummy2 = intdf.loc[(i,),'deltaI']/stan_dI
        
        intdf.loc[(i,),'Ratio pECD'] = pd.Series(dummy.values,
                                                 index = a.index)
        
        intdf.loc[(i,),'Ratio dI'] = pd.Series(dummy2.values,
                                                 index = a.index)
        
    # Add data from event_statistics.dat
    # Import event statistics
    event_stats = pd.read_csv(filepath+'Event statistics.dat', sep='\t', engine='python')
    # Drop last column (artifact from import)
    event_stats.drop(event_stats.columns[[-1]], axis=1, inplace=True)
    # Add 1 to every index (no tdms 0 file) HARDCODE
    event_stats.index +=1
    # Create a column with event number in case of reindexing shenanigans
    event_stats['event_num'] = event_stats.index
    
    intdf = pd.merge(left = intdf, right = event_stats, on = 'event_num',
                     how = 'left')
        
    intdf['Ratio dI/ECD'] = intdf['Ratio dI']/intdf['Event charge deficit (fC)']
    intdf['Ratio dI/s'] = intdf['Ratio dI']/intdf['Event duration (s)']
    
    intdf['Ratio pECD/ECD'] = intdf['Ratio pECD']/intdf['Event charge deficit (fC)']
    intdf['Ratio pECD/s'] = intdf['Ratio pECD']/intdf['Event duration (s)']
    
    # FIXME better peak depth normalization
    intdf['dI/Io']     = -intdf['deltaI']/intdf['Mean event current (nA)']
    intdf['dI/Io/ECD'] = intdf['dI/Io']/intdf['Event charge deficit (fC)']
    intdf['dI/Io/s']   = intdf['dI/Io']/intdf['Event duration (s)']
    
    return intdf

#%% Probes-27

# Wrap data processing into one function
def wrapperP3(filepath, big_peak_label, med_peak_label,lil_peak_label):

    intdf = pd.read_pickle(filepath+'apeak012v2.pkl')
    
    # Drop events that had no integrals because it will mess up ratios part    
    intdf.dropna(inplace = True)        

    for i in intdf.index.get_level_values(0).unique():
        # Rank based on integral 
        a = intdf.loc[i,'Integral'].rank()
        # MUST DO Set index so it can be assigned to intdf properly
        a.index = pd.MultiIndex.from_product([(i,), a.index])
        intdf.loc[(i,),'Peak_ECD_Ord'] = a 
        
        # Rank based on left_x
        a = intdf.loc[i,'left_x'].rank()
        # MUST DO Set index so it can be assigned to intdf properly
        a.index = pd.MultiIndex.from_product([(i,), a.index])
        intdf.loc[(i,),'Peak_Pos_Ord'] = a 
        
        b = intdf.loc[i,'Peak_ECD_Ord']
        b2 = intdf.loc[i,'Peak_Pos_Ord']
        
        # Keep peak ID as relative up to this points
        if len(b.loc[b==b2]) == len(b.loc[b.notna()]):
            direction = 1
        elif len(b.loc[(-b+4)==b2]) == len(b.loc[b.notna()]):
            direction = 0
        else: # Make sure there are not any 021s
            print(b,'OH NO')
        # Copy index from earlier
        intdf.loc[(i,),'Direction'] = pd.Series(direction,index = a.index)
        
        # Now change Peak_ID into a user labeled Peak_ID
        rank_to_label = {1:lil_peak_label,2:med_peak_label,3:big_peak_label}
        intdf['Peak_ID'] = intdf['Peak_ECD_Ord'].map(rank_to_label)
        
        # Calculate ratio pECD and ratio dI
        stan_pECD = intdf.loc[(i,)].loc[intdf.loc[(i,)]['Peak_ID']==med_peak_label]['Integral'].iat[0]
        stan_dI   = intdf.loc[(i,)].loc[intdf.loc[(i,)]['Peak_ID']==med_peak_label]['deltaI'].iat[0]
        
        # No idea why you need to add in .values here
        dummy = intdf.loc[(i,),'Integral']/stan_pECD
        dummy2 = intdf.loc[(i,),'deltaI']/stan_dI
        
        intdf.loc[(i,),'Ratio pECD'] = pd.Series(dummy.values,
                                                 index = a.index)
        
        intdf.loc[(i,),'Ratio dI'] = pd.Series(dummy2.values,
                                                 index = a.index)
        
    # Add data from event_statistics.dat
    # Import event statistics
    event_stats = pd.read_csv(filepath+'Event statistics.dat', sep='\t', engine='python')
    # Drop last column (artifact from import)
    event_stats.drop(event_stats.columns[[-1]], axis=1, inplace=True)
    # Add 1 to every index (no tdms 0 file) HARDCODE
    event_stats.index +=1
    # Create a column with event number in case of reindexing shenanigans
    event_stats['event_num'] = event_stats.index
    
    intdf = pd.merge(left = intdf, right = event_stats, on = 'event_num',
                     how = 'left')
        
    intdf['Ratio dI/ECD'] = intdf['Ratio dI']/intdf['Event charge deficit (fC)']
    intdf['Ratio dI/s'] = intdf['Ratio dI']/intdf['Event duration (s)']
    
    intdf['Ratio pECD/ECD'] = intdf['Ratio pECD']/intdf['Event charge deficit (fC)']
    intdf['Ratio pECD/s'] = intdf['Ratio pECD']/intdf['Event duration (s)']
    
    # FIXME better peak depth normalization
    intdf['dI/Io']     = -intdf['deltaI']/intdf['Mean event current (nA)']
    intdf['dI/Io/ECD'] = intdf['dI/Io']/intdf['Event charge deficit (fC)']
    intdf['dI/Io/s']   = intdf['dI/Io']/intdf['Event duration (s)']
    
    
            
    return intdf

#%% END LABELS-5

# Wrap data processing into one function
def wrapperE5(filepath, big_peak_label, lil_peak_label):
    intdf = pd.read_pickle(filepath)
    intdf = dualPeakLabeler(intdf,big_peak_label,lil_peak_label)
    intdf = forwardBackward(intdf,big_peak_label,lil_peak_label)
    return intdf
    

# Identifies large and small peak based off ECD
def dualPeakLabeler(intdf, big_peak_label,lil_peak_label):
    intdf['Peak_ID']=''
    for i in intdf['event_num'].unique():
        big_peak = 0
        lil_peak = 0
        j_loop_count = 0
        for j in intdf.loc[intdf['event_num']==i]['Integral']:
            if j_loop_count == 0:
                big_peak = j
            elif j_loop_count == 1:
                if j>big_peak:
                    lil_peak = big_peak
                    big_peak = j
                else: 
                    lil_peak = j
            else: 
                print('looks like there were more than two peaks integrated!')
            j_loop_count += 1
        
        # AVOID CHAINING INDEXING like
        #intdf.loc[intdf['Integral']==lil_peak]['Peak_ID'] = 2 
        intdf.loc[intdf['Integral']==big_peak,'Peak_ID'] = big_peak_label
        intdf.loc[intdf['Integral']==big_peak,'Peak_Q'] = 'Big'
        intdf.loc[intdf['Integral']==lil_peak,'Peak_ID'] = lil_peak_label
        intdf.loc[intdf['Integral']==lil_peak,'Peak_Q'] = 'Lil'
    intdf['event_type'] = f'{big_peak_label},{lil_peak_label}'
    return intdf

# Label whether event is forward (big then little) or backward (little then big)
def forwardBackward(intdf, big_peak_label,lil_peak_label):
    intdf['Direction'] = ''
    # if you don't initialize these then lil_peaks will have nan for ratio
    intdf['Ratio dI']  = 1
    intdf['Ratio pECD']  = 1
    for i in intdf['event_num'].unique():
        # left edge of big peak
        x2 = intdf.loc[((intdf['event_num']==i)&(intdf['Peak_ID']==big_peak_label))]['left_x'].iat[0]
        # left edge of lil peak
        x1 = intdf.loc[((intdf['event_num']==i)&(intdf['Peak_ID']==lil_peak_label))]['left_x'].iat[0]
        
        dI2 = intdf.loc[((intdf['event_num']==i)&(intdf['Peak_ID']==big_peak_label))]['deltaI'].iat[0]
        dI1 = intdf.loc[((intdf['event_num']==i)&(intdf['Peak_ID']==lil_peak_label))]['deltaI'].iat[0]
        
        ECD2 = intdf.loc[((intdf['event_num']==i)&(intdf['Peak_ID']==big_peak_label))]['Integral'].iat[0]
        ECD1 = intdf.loc[((intdf['event_num']==i)&(intdf['Peak_ID']==lil_peak_label))]['Integral'].iat[0]
        
        if x2<x1:
            intdf.loc[intdf['event_num']==i,'Direction'] = 1
        else:
            intdf.loc[intdf['event_num']==i,'Direction'] = 0
            
        intdf.loc[((intdf['event_num']==i)&(intdf['Peak_ID']==big_peak_label)),
                  'Ratio dI'] = dI2/dI1
        intdf.loc[((intdf['event_num']==i)&(intdf['Peak_ID']==big_peak_label)),
                  'Ratio pECD'] = ECD2/ECD1
        
    intdf['Ratio dI/ECD'] = intdf['Ratio dI']/intdf['Event charge deficit (fC)']
    intdf['Ratio dI/s'] = intdf['Ratio dI']/intdf['Event duration (s)']
    
    intdf['Ratio pECD/ECD'] = intdf['Ratio pECD']/intdf['Event charge deficit (fC)']
    intdf['Ratio pECD/s'] = intdf['Ratio pECD']/intdf['Event duration (s)']
            
    return intdf