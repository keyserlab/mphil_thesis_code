#!/usr/bin/env python3
# -*- coding: utf-8 -*-
 
print('SegFault: First Line')

import glob 
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import pandas as pd
import tkinter as tk
import tkinter.font
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from nptdms import TdmsFile 
from scipy import signal
import os
from os.path import exists

try:
    import h5py
except ModuleNotFoundError:
    print('INFO: h5py not installed, can only analyze tdms files')

#os.chdir('filepath')

# Create class for toolbar that doesn't display x and y coordinates
class CustomToolbar(NavigationToolbar2Tk):
    def set_message(self, s):
        pass

# Application class inhereits from frame
class Application(tk.Frame):
    
    cusdf = pd.DataFrame(columns = ['event_num','hline'])
    
    # Store left and right bounds when finding peak integrals
    lBoundsList = []
    rBoundsList = []

    # Always executed when class is being initiated
    def __init__(self, master = None): # master=None
        print('SegFault: __init__')
        tk.Frame.__init__(self,master = None)
        
        # Change font to a monospace font
        # Comment out if there are font problems
        tkinter.font.nametofont('TkDefaultFont').configure(family = 'Courier',
                                                           size='12',
                                                           weight = 'normal')
        
        wcattype.set('event_type') # set a default wcattype

        # Default values for peak finding
        prommin.set(0.05)
        prommax.set(1)
        wlen.set(80)
        width.set(0)
        rel_height.set(1)
        
        # Default values for baseline parameters
        pore_baseline_pts.set(50)
        # FIXME bug when this is less than 2x event size
        #dna_baseline_pts.set(150)
        dna_baseline_pts.set(15)
        xyscale.set(10000)

        # Run createWidgets function
        self.createWidgets()
        print('SegFault: createWidgets')
        
    def open_peak_param_window(self, event = None):
        # Create secondary window
        peak_param_window = tk.Toplevel()
        peak_param_window.title('Peak Finding Parameters')
        
        # Create a button to close this window
        button_close = tk.Button(peak_param_window,text='Store and Close window',
                                 command=peak_param_window.destroy)
        button_close.grid(row = 0,column = 0, columnspan=2)
        
        self.enter_npeaks     = tk.Entry(peak_param_window, textvariable = npeaks)
        self.enter_npeaks.grid(row=6,column=0, sticky = 'EW')
        tk.Label(peak_param_window, 
                 text = 'n Peak Threshold').grid(row=6,column=1)
        
        self.enter_prommin    = tk.Entry(peak_param_window, textvariable = prommin)
        self.enter_prommin.grid(row=1,column=0, sticky = 'EW')
        tk.Label(peak_param_window,
                 text = 'prommin').grid(row=1, column=1, sticky = 'EW')
        
        self.enter_prommax    = tk.Entry(peak_param_window, textvariable = prommax)
        self.enter_prommax.grid(row=2,column=0, sticky = 'EW')
        tk.Label(peak_param_window,
                 text = 'prommax').grid(row=2, column=1, sticky = 'EW')
        
        self.enter_width    = tk.Entry(peak_param_window, textvariable = width)
        self.enter_width.grid(row=3,column=0, sticky = 'EW')        
        tk.Label(peak_param_window,
                 text = 'width').grid(row=3, column=1, sticky = 'EW')
                
        self.enter_wlen    = tk.Entry(peak_param_window, textvariable = wlen)
        self.enter_wlen.grid(row=4,column=0, sticky = 'EW')        
        tk.Label(peak_param_window,
                 text = 'wlen').grid(row=4, column=1, sticky = 'EW')
                
        self.enter_rel_height    = tk.Entry(peak_param_window, textvariable = rel_height)
        self.enter_rel_height.grid(row=5,column=0, sticky = 'EW')
        tk.Label(peak_param_window,
                 text = 'rel_height').grid(row=5, column=1, sticky = 'EW')
        
        
    def open_temp_param_window(self, event = None):
        # Create secondary window
        temp_param_window = tk.Toplevel()
        temp_param_window.title('Baselines & Significance')
        
        # Create a button to close this window
        button_close = tk.Button(temp_param_window,text='Store and Close window',
                                 command=temp_param_window.destroy)
        button_close.grid(row = 0,column = 0, columnspan=2)
        
        self.enter_porebase    = tk.Entry(temp_param_window, textvariable = pore_baseline_pts)
        self.enter_porebase.grid(row=1,column=0, sticky = 'EW')
        tk.Label(temp_param_window,
                 text = 'Pore Baseline first x points').grid(row=1, column=1, sticky = 'EW')
        
        self.enter_dnabase     = tk.Entry(temp_param_window, textvariable = dna_baseline_pts)
        self.enter_dnabase.grid(row=2,column=0, sticky = 'EW')
        tk.Label(temp_param_window, 
                 text = 'DNA Baseline exclude x points').grid(row=2, column=1, sticky = 'EW')
        
        self.enter_xyscale = tk.Entry(temp_param_window, textvariable = xyscale)
        self.enter_xyscale.grid(row=3,column=0,sticky = 'EW')
        tk.Label(temp_param_window,
                 text = 'xy scale').grid(row=3, column=1,sticky = 'EW')
        
        self.enter_pss    = tk.Entry(temp_param_window, textvariable = pos_slope_sig)
        self.enter_pss.grid(row=1,column=2, sticky = 'EW')
        tk.Label(temp_param_window,
                 text = 'Pos Slope Significance').grid(row=1, column=3, sticky = 'EW')
        
        self.enter_nss     = tk.Entry(temp_param_window, textvariable = neg_slope_sig)
        self.enter_nss.grid(row=2,column=2, sticky = 'EW')
        tk.Label(temp_param_window, 
                 text = 'Neg Slope Significance').grid(row=2, column=3, sticky = 'EW')
        
        self.enter_p1s    = tk.Entry(temp_param_window, textvariable = p1s_sig)
        self.enter_p1s.grid(row=3,column=2, sticky = 'EW')
        tk.Label(temp_param_window,
                 text = 'Peak 1 Significance').grid(row=3, column=3, sticky = 'E')
        
        self.enter_p2s     = tk.Entry(temp_param_window, textvariable = p2s_sig)
        self.enter_p2s.grid(row=4,column=2, sticky = 'EW')
        tk.Label(temp_param_window, 
                 text = 'Peak 2 Significance').grid(row=4, column=3, sticky = 'E')
        
        self.enter_p3s    = tk.Entry(temp_param_window, textvariable = p3s_sig)
        self.enter_p3s.grid(row=5,column=2, sticky = 'EW')
        tk.Label(temp_param_window,
                 text = 'Peak 3 Significance').grid(row=5, column=3, sticky = 'E')
        
        self.enter_p4s     = tk.Entry(temp_param_window, textvariable = p4s_sig)
        self.enter_p4s.grid(row=6,column=2, sticky = 'EW')
        tk.Label(temp_param_window, 
                 text = 'Peak 4 Significance').grid(row=6, column=3, sticky = 'E')
        
        
    def open_set_cat_window(self, event = None):
        # Create secondary window
        set_cat_window = tk.Toplevel()
        set_cat_window.title('Set Categories')
        
        # Create a button to close this window
        button_close = tk.Button(set_cat_window,text='Store and Close window',
                                 command=set_cat_window.destroy)
        button_close.grid(row = 0,column = 0, columnspan=2)
        
        self.enter_wcattype = tk.Entry(set_cat_window, textvariable = wcattype)
        self.enter_wcattype.grid(row = 1, column = 0)
        tk.Label(set_cat_window,
                 text = 'Write Category Type').grid(row = 1, column = 2)
        
        self.list_text = ['Q','W','E','R','T','Y','U','I','O','P']
        self.list_cats = [cat0,cat1,cat2,cat3,cat4,cat5,cat6,cat7,cat8,cat9]
        self.list_label_vars = {}
        
        # Make labels and entries
        for index,i in enumerate(self.list_text):
            self.list_label_vars['self.label'+str(index)] = tk.Label(set_cat_window,text = i+' set to')
            self.list_label_vars['self.label'+str(index)].grid(row=index+2,column=0)
            
            self.list_label_vars['self.entry'+str(index)] = tk.Entry(set_cat_window,
                                                                 textvariable = self.list_cats[index])
            self.list_label_vars['self.entry'+str(index)].grid(row=index+2,column = 2)
            
        
    def open_debug_window(self, event = None):
        debug_window = tk.Toplevel()
        debug_window.title('Debug')
        
        # Create a button to close this window.
        button_close = tk.Button(debug_window,text='Destroy window',
                                 command=debug_window.destroy)
        button_close.grid(row = 0,column = 0, columnspan=2)
        
        # not Tk variables, won't autoupdate
        tk.Label(debug_window,
                 text = 'currplot : ').grid(row = 1, column = 0, sticky = 'W')
        tk.Label(debug_window,
                 text = str(self.currplot)).grid(row=1, column = 1, sticky = 'W')
        

        self.debug_string_name = ['filepath', 'catpath','intpath','cuspath',
                                  'pos_slope_sig','neg_slope_sig',
                                  'p1s_sig','p2s_sig','p3s_sig','p4s_sig',
                                  'catdf_savename','intdf_savename','cusdf_savename', 
                                  'wcattype','rcattype',
                                  'cat0','cat1','cat2','cat3','cat4',
                                  'cat5','cat6','cat7','cat8','cat9']
        
        self.debug_string_vals = [filepath, catpath, intpath,cuspath,
                                  pos_slope_sig,neg_slope_sig,
                                  p1s_sig,p2s_sig,p3s_sig,p4s_sig,
                                  catdf_savename,intdf_savename,cusdf_savename,
                                  wcattype, rcattype,
                                  cat0,cat1,cat2,cat3,cat4,
                                  cat5,cat6,cat7,cat8,cat9]
        
        # Make Labels
        for index,i in enumerate(self.debug_string_vals):
            tk.Label(debug_window, 
                     text = self.debug_string_name[index]+' : ').grid(row = index+2,
                                                                      column=0, 
                                                                      sticky ='W')
            tk.Label(debug_window, 
                     text = self.debug_string_vals[index].get()).grid(row = index+2,
                                                                       column=1, 
                                                                       sticky ='W')
  
    def open_headless_window(self,event = None):
        headless_window = tk.Toplevel()
        headless_window.title('Automatic Assignation')
        
        # Create a button to close this window
        button_close = tk.Button(headless_window,text='Destroy window',
                                 command=headless_window.destroy)
        button_close.grid(row = 0,column = 0, columnspan=2, sticky = 'EW')
        
        # Create a button to run function
        button_run   = tk.Button(headless_window,text='Run',
                                 # should probably have auto_type as an argument
                                 command= lambda: self.headless_categorize())
        button_run.grid(row = 1,column = 0, columnspan=2, sticky = 'EW')
        
        self.enter_acattype = tk.Entry(headless_window, textvariable = acattype)
        self.enter_acattype.grid(row = 2, column = 0)
        tk.Label(headless_window,
                 text = 'Auto Category Type').grid(row = 2, column = 2)
        
        # Types of auto-analysis to pick from
        auto_options = tk.OptionMenu(headless_window, auto_type,
                                     'Slope','npeaks','Auto Integrate')
        auto_options.grid(row=3,column=0, sticky = 'EW')
        tk.Label(headless_window,
                 text = 'Type of auto-analysis').grid(row = 3, column = 2)
        
        # FIXME Duplicate from temp peak param but that is good
        self.enter_pss    = tk.Entry(headless_window, textvariable = pos_slope_sig)
        self.enter_pss.grid(row=4,column=2, sticky = 'EW')
        tk.Label(headless_window,
                 text = 'Pos Slope Significance').grid(row=4, column=3, sticky = 'EW')
        
        self.enter_nss     = tk.Entry(headless_window, textvariable = neg_slope_sig)
        self.enter_nss.grid(row=5,column=2, sticky = 'EW')
        tk.Label(headless_window, 
                 text = 'Neg Slope Significance').grid(row=5, column=3, sticky = 'EW')
        
        self.enter_min_npeaks  = tk.Entry(headless_window, textvariable = min_npeaks)
        self.enter_min_npeaks.grid(row=6,column=2, sticky = 'EW')
        tk.Label(headless_window,
                 text = 'Exclude _ or less peaks').grid(row=6, column=3, sticky = 'EW')
        
        self.enter_max_npeaks  = tk.Entry(headless_window, textvariable = max_npeaks)
        self.enter_max_npeaks.grid(row=7,column=2, sticky = 'EW')
        tk.Label(headless_window,
                 text = 'Exclude _ or more peaks').grid(row=7, column=3, sticky = 'EW')
        
        self.enter_include_npeaks_sig = tk.Entry(headless_window, textvariable = include_npeaks_sig)
        self.enter_include_npeaks_sig.grid(row = 8, column = 2, sticky = 'EW')
        tk.Label(headless_window,
                 text = 'Include npeaks Significance').grid(row = 8, column = 3)
        
        self.enter_exclude_npeaks_sig = tk.Entry(headless_window, textvariable = exclude_npeaks_sig)
        self.enter_exclude_npeaks_sig.grid(row = 9, column = 2, sticky = 'EW')
        tk.Label(headless_window,
                 text = 'Exclude npeaks Significance').grid(row = 9, column = 3)
            

    def createWidgets(self):
        
        # Create frames
        # The grid call MUST be separate from creating the object
        
        # Frame to store setup inputs
        self.frame_init   = tk.Frame(root)
        self.frame_init.grid(row  = 0, column = 0)
        
        # Frame to store printers
        self.frame_button = tk.Frame(root)
        self.frame_button.grid(row=1,column=0)
        
        # Frame to store categorization inputs/bindings
        self.frame_cats   = tk.Frame(root)
        self.frame_cats.grid(row=2,column=0)
        
        # Frame to display graph
        self.frame_graph  = tk.Frame(root)
        self.frame_graph.grid(row=0,column=3, rowspan=3, padx=100)
        
        # for changing row and columnspan of canvas
        canvassize = 5
        
# =============================================================================
#         FRAME INIT
# =============================================================================
        
        # Entry to display only a specific category
        self.enter_rcattype = tk.Entry(self.frame_init, textvariable = rcattype)
        self.enter_rcattype.grid(row = 0, column = 0,sticky = 'EW')
        tk.Label(self.frame_init,
                  text = 'Read Category Type').grid(row = 0, column = 1,sticky = 'W')
        
        # Entry to display specific type of event within that category
        self.enter_evlist    = tk.Entry(self.frame_init,textvariable = evlist)
        self.enter_evlist.grid(row=1,column=0, sticky = 'EW')
        tk.Label(self.frame_init, 
                  text = 'Category to display').grid(row=1,column=1,sticky='W')
        
        # Entry for loading a dataframe     
        self.enter_dataframe = tk.Entry(self.frame_init, textvariable = catpath)
        self.enter_dataframe.grid(row=2, column = 0, sticky = 'EW')
        tk.Label(self.frame_init, 
                 text = '.pkl file to restore cats').grid(row=2, column=1, sticky = 'EW')
        
        self.enter_intdfpath = tk.Entry(self.frame_init, textvariable = intpath)
        self.enter_intdfpath.grid(row=3, column = 0, sticky = 'EW')
        tk.Label(self.frame_init, 
                 text = '.pkl file to restore ints').grid(row=3, column=1, sticky = 'EW')

        self.enter_cusdfpath = tk.Entry(self.frame_init, textvariable = cuspath)
        self.enter_cusdfpath.grid(row=4, column = 0, sticky = 'EW')
        tk.Label(self.frame_init, 
                 text = '.pkl file to restore cust').grid(row = 4, column=1, sticky = 'EW')
        
        # Create filepath input
        # The textvariable option is critical!!!!!
        self.enter_filepath = tk.Entry(self.frame_init, textvariable=filepath)
        self.enter_filepath.grid(row=5,column=0,columnspan = 2, sticky = 'NEWS')
        tk.Label(self.frame_init,
                 text = '^Folder w/ .tdms^').grid(row=6,column=0, columnspan=1, sticky = 'NEWS')
        
        self.open_file_button = tk.Button(self.frame_init, text = 'Navigate files',
                                          command = lambda: self.open_file())
        self.open_file_button.grid(row = 6, column = 1)
        

        
        # Modfify file browser behavior
        self.check_navhdf5  = tk.Checkbutton(self.frame_init,text = 'Navigate  hdf5?',
                                             variable = navHDF5,
                                             onvalue = True, offvalue = False)
        self.check_navhdf5.grid(row = 7, column = 0, sticky = 'W')
        
        
        # Modify loading behavior to exclude specified types
        self.check_notLoad  = tk.Checkbutton(self.frame_init,text= 'Not Load?',
                                             variable = notLoad,
                                             onvalue = True, offvalue = False)
        self.check_notLoad.grid(row=8, column = 0, sticky = 'W')
        
        # Toggle between plotting individual points or lines
        self.check_ptorline = tk.Checkbutton(self.frame_init,text = 'Plot Points?',
                                             variable = ptorline,
                                             onvalue = True, offvalue = False)
        self.check_ptorline.grid(row=9, column = 0, sticky = 'W')
        
        # Enable DNA Baseline
        self.check_dnafit      = tk.Checkbutton(self.frame_init, text = 'DNA Baseline?',
                                                variable = dnafit)
        self.check_dnafit.grid(row = 10, column = 0, sticky = 'W')
        
        # Enable peak-finding
        self.check_findpeak = tk.Checkbutton(self.frame_init,text = 'Find Peaks (experimental)?',
                                             variable = findpeak,
                                             onvalue = True, offvalue = False)
        self.check_findpeak.grid(row=11, column = 0, sticky = 'W')
    
        
        # Secondary Windows
        self.peak_param_win    = tk.Button(self.frame_init, text = 'Peak Finding', fg = '#363BF7',
                                        command = lambda: self.open_peak_param_window())
        self.peak_param_win.grid(row=8,column=1, rowspan=1, columnspan = 1,sticky = 'NEWS')
        
        self.temp_param_win    = tk.Button(self.frame_init, text = 'Baseline & Sig', fg = '#363BF7',
                                        command = lambda: self.open_temp_param_window())
        self.temp_param_win.grid(row=9,column=1, rowspan=1, columnspan = 1,sticky = 'NEWS')
        
        self.debug_win    = tk.Button(self.frame_init, text = 'Debug', fg = '#363BF7',
                                        command = lambda: self.open_debug_window())
        self.debug_win.grid(row=10,column=1, rowspan=1, columnspan = 1,sticky = 'NEWS')
        
        self.headless_win    = tk.Button(self.frame_init, text = 'Auto', fg = '#363BF7',
                                        command = lambda: self.open_headless_window())
        self.headless_win.grid(row=11,column=1, rowspan=1, columnspan = 1,sticky = 'NEWS')
        
        self.init_button    = tk.Button(self.frame_init, text = 'Start!', fg = '#363BF7',
                                        command = lambda: self.initStart())
        self.init_button.grid(row=7,column=1, rowspan=1, columnspan = 1,sticky = 'NEWS')
        
# =============================================================================
#         FRAME BUTTON
# =============================================================================
        
        # Display previous actions
        tk.Label(self.frame_button, text = 'Cos pred', width = 10,
                 height = 2).grid(row = 0,column = 0, sticky = 'EW')
        
        tk.Label(self.frame_button, text = 'Ev  pred',width = 10, 
                 height = 2).grid(row = 1, column = 0, sticky = 'EW')
        
        tk.Label(self.frame_button, text = 'df  read',width = 10, 
                 height = 2).grid(row = 2, column = 0, sticky = 'EW')
        
        tk.Label(self.frame_button, text = '2nd Last',width = 10, 
                 height = 2).grid(row = 3, column = 0, sticky = 'EW')
        
        tk.Label(self.frame_button, text = 'Previous',width = 10, 
                 height = 2).grid(row = 4, column = 0, sticky = 'EW')
        
        self.Cos_pred_printer   = tk.Label(self.frame_button,
                                           textvariable = cos_pred,
                                           bg = '#c6afdf',width = 30, height = 2)
        self.Cos_pred_printer.grid(row=0,column = 1, sticky = 'NEWS')
        
        self.Ev_pred_printer = tk.Label(self.frame_button, 
                                        textvariable = ev_pred,
                                        bg = '#80AAFF', width = 30, height = 2)
        self.Ev_pred_printer.grid(row=1,column = 1, sticky = 'NEWS')

        # Current category assigned
        self.current_cat_printer = tk.Label(self.frame_button, 
                                             textvariable = curr_cat,
                                             bg = '#F1BCC2', width=30, height = 2)
        self.current_cat_printer.grid(row=2,column=1,sticky = 'NEWS')
        
        # Category assigned two times ago
        self.prevv_cat_printer   = tk.Label(self.frame_button,
                                             textvariable = prevv_cat, 
                                             bg='#F1E3BC', width=30, height = 2)
        self.prevv_cat_printer.grid(row=3,column=1, sticky = 'NEWS')
        
        # Previous category assigned
        self.last_cat_printer    = tk.Label(self.frame_button,
                                             textvariable = prev_cat, 
                                             bg = '#EDF1BC', width=30, height = 2)
        self.last_cat_printer.grid(row=4,column=1, sticky = 'NEWS')
        

        # Create unlock button to allow for changing inputs
        self.enable_buttons = tk.Button(self.frame_button, text = 'Unlock',
                                         command=lambda: self.enableButtons())
        self.enable_buttons.grid(row=5,column=0, columnspan=2, sticky='EW')
        
        button_close = tk.Button(self.frame_button,text='Destroy window',
                                 command=root.destroy)
        button_close.grid(row = 6,column = 0, columnspan=2, sticky='EW')
        
        
# =============================================================================
#       Frame Categories
# =============================================================================
        self.set_cat_win    = tk.Button(self.frame_cats, text = 'Set Categories', fg = '#363BF7',
                                        command = lambda: self.open_set_cat_window())
        self.set_cat_win.grid(row=10,column=0, rowspan=1, columnspan = 3,sticky = 'NEWS')
        
        # Make save buttons
        self.enter_catdf_savename  = tk.Entry(self.frame_cats, textvariable = catdf_savename)
        self.enter_catdf_savename.grid(row=11, column=0, sticky = 'EW')
        
        self.store_catdf_dataframe = tk.Button(self.frame_cats, text = 'Store cat dataframe', fg = '#363BF7',
                                               command=lambda: self.universalStoreDataframe(self.catdf, catdf_savename))
        self.store_catdf_dataframe.grid(row=11,column= 1, columnspan = 2, sticky = 'NEWS')
        
        self.enter_intdf_savename  = tk.Entry(self.frame_cats, textvariable = intdf_savename)
        self.enter_intdf_savename.grid(row=12, column=0, sticky = 'EW')
        
        self.store_intdf_dataframe = tk.Button(self.frame_cats, text = 'Store int dataframe', fg = '#363BF7',
                                               command=lambda: self.intStoreDataframe(self.intdf, intdf_savename))
                                        
        self.store_intdf_dataframe.grid(row=12,column= 1, columnspan = 2, sticky = 'NEWS')
        
        self.enter_cusdf_savename  = tk.Entry(self.frame_cats, textvariable = cusdf_savename)
        self.enter_cusdf_savename.grid(row=13, column = 0, sticky = 'EW')
        
        self.store_cusdf_dataframe = tk.Button(self.frame_cats, text = 'Store cus dataframe', fg = '#363BF7',
                                               command=lambda: self.universalStoreDataframe(self.cusdf, cusdf_savename))
        self.store_cusdf_dataframe.grid(row=13, column =1, columnspan = 2, sticky = 'NEWS')
            

# =============================================================================
#         Frame Graph
# =============================================================================
        
        # Matplotlib plotting commmands
        # Create figure (was 4,4)
        fig=plt.figure(figsize=(6,4), dpi=72)
        # Create container
        gs = gridspec.GridSpec(1,1)
        # Add axes to containers
        self.ax  = fig.add_subplot(gs[0,0])
        
        # first parameter mpl object, second tk object
        self.canvas=FigureCanvasTkAgg(fig,master=self.frame_graph)
        
        # Set position of canvas
        self.canvas.get_tk_widget().grid(row=0,column=0,rowspan=canvassize,
                                         columnspan=canvassize)
        self.canvas.draw()
        
        # Create toolbar
        toolbar = CustomToolbar(self.canvas,self.frame_graph,pack_toolbar=False)
        toolbar.grid(row=canvassize+1,column=2)
        
        
# =============================================================================
#         Bindings
# =============================================================================
        
        # Navigate events in plotlist
        self.canvas.get_tk_widget().bind('<Right>',lambda event: self.plot(self.canvas,self.ax,1,0))
        self.canvas.get_tk_widget().bind('<Left>', lambda event: self.plot(self.canvas,self.ax,-1,0))
        self.canvas.get_tk_widget().bind('<Up>',   lambda event: self.plot(self.canvas,self.ax,100,0))
        self.canvas.get_tk_widget().bind('<Down>', lambda event: self.plot(self.canvas,self.ax,-100,0))
        
        # Navigate marker
        self.canvas.get_tk_widget().bind('z',lambda event: 
                                         self.plot(self.canvas,self.ax,0,  20,keep_bounds=True))
        self.canvas.get_tk_widget().bind('x',lambda event: 
                                         self.plot(self.canvas,self.ax,0, -20,keep_bounds=True))
        self.canvas.get_tk_widget().bind('c',lambda event: 
                                         self.plot(self.canvas,self.ax,0,   5,keep_bounds=True))
        self.canvas.get_tk_widget().bind('v',lambda event: 
                                         self.plot(self.canvas,self.ax,0,  -5,keep_bounds=True))
        self.canvas.get_tk_widget().bind('b',lambda event: 
                                         self.plot(self.canvas,self.ax,0,   1,keep_bounds=True))
        self.canvas.get_tk_widget().bind('n',lambda event: 
                                         self.plot(self.canvas,self.ax,0,  -1,keep_bounds=True))
        self.canvas.mpl_connect('button_press_event', self.on_mouse_press)

        
        # Integration 
        self.canvas.get_tk_widget().bind('a',lambda event: self.setLeft())
        self.canvas.get_tk_widget().bind('d',lambda event: self.setRight())
        self.canvas.get_tk_widget().bind('f',lambda event: self.integrate())
        self.canvas.get_tk_widget().bind('k',lambda event: self.integrateSelPeak(auto_l = True, auto_r = True))
        self.canvas.get_tk_widget().bind('j',lambda event: self.integrateSelPeak(auto_l = True))
        self.canvas.get_tk_widget().bind('l',lambda event: self.integrateSelPeak(auto_r = True))
        self.canvas.get_tk_widget().bind('g',lambda event: self.detectNextPeak())
        self.canvas.get_tk_widget().bind('h',lambda event: self.eventDuration())
        self.canvas.get_tk_widget().bind('m',lambda event: self.customEvent())
        
        # Category Setting
        self.canvas.get_tk_widget().bind('q',lambda event: self.setCat(cat0.get()))
        self.canvas.get_tk_widget().bind('w',lambda event: self.setCat(cat1.get()))
        self.canvas.get_tk_widget().bind('e',lambda event: self.setCat(cat2.get()))
        self.canvas.get_tk_widget().bind('r',lambda event: self.setCat(cat3.get()))
        self.canvas.get_tk_widget().bind('t',lambda event: self.setCat(cat4.get()))
        self.canvas.get_tk_widget().bind('y',lambda event: self.setCat(cat5.get()))
        self.canvas.get_tk_widget().bind('u',lambda event: self.setCat(cat6.get()))
        self.canvas.get_tk_widget().bind('i',lambda event: self.setCat(cat7.get()))
        self.canvas.get_tk_widget().bind('o',lambda event: self.setCat(cat8.get()))
        self.canvas.get_tk_widget().bind('p',lambda event: self.setCat(cat9.get()))

        
    # Function to plot tdms files    
    def plot(self,canvas,ax, dCounter, dMarker, **kwargs):
        ax.clear() # clear axes
        
        # set event to plot based on plotlist
        self.currplot = self.plotlist[counter.get()+dCounter]
        
        # Should be wcat. If you are using rcat you know what it is!
        # Read current categorization of event from catdf
        try:
            if self.catdf[wcattype.get()][self.currplot] == '':
                curr_cat.set('Event #'+str(self.currplot)+' prev UNASSIGNED')
            else:
                curr_cat.set('Event #'+str(self.currplot)+' prev set to '
                             +self.catdf[wcattype.get()][self.currplot])
        except KeyError:
            print('INFO: Category Type Not Previously Used: Initializing')
            self.catdf[wcattype.get()] = ['']*len(self.filelist)
            curr_cat.set('Event #'+str(self.currplot)+' prev UNASSIGNED')
            
        self.df = self.loadEvent(self.currplot)
        
        numpoints = len(self.df) # store length of dataframe in memory
        pore_baseline = np.mean(self.df[0:pore_baseline_pts.get()])
        dna_baseline  = np.mean(self.df)
        
        # if marker would be outside next plot set it to last point
        if marker.get()+dMarker <= numpoints:
            j = marker.get()+dMarker
        else:
            marker.set(numpoints-1)
            j = marker.get()
        
        # Simple plottting command
        if ptorline.get() == True: # plot points
            ax.plot(self.df, '.', markersize=2)
        else:                # plot lines
            ax.plot(self.df) 
        
        # Plot marker
        ax.plot(j,self.df[j],'xr')
        
        # Palette for previous integral bounds
        #int_palette = ['#1d2337', '#2b3452', '#39456d', '#5667a4', '#7289da', 
        #               '#b9c4ed', '#cbd3f2', '#dce2f6', '#e5eaf9', '#eef1fb']
        
        int_palette = ['#00467f', '#00569b', '#0064b5', '#80c6ff','#bfe2ff',
                       '#00467f', '#00569b', '#0064b5', '#80c6ff','#bfe2ff']
        
        # redefined in integrate()
        #int_num = self.intdf.loc[self.currplot].index[self.intdf.loc[self.currplot].isna().all(axis=1)][0]
        int_lst = self.intdf.loc[self.currplot].index[self.intdf.loc[self.currplot].notna().all(axis=1)]
        prev_bounds = self.intdf.loc[self.currplot].loc[self.intdf.loc[self.currplot][['left_x','right_x']].notna().all(axis=1)][['left_x','right_x']]
        
        # plot previous integral bounds
        for i in int_lst:
            ax.plot(prev_bounds['left_x'][i],self.df[prev_bounds['left_x'][i]],c = int_palette[i],marker = 'o')
            ax.plot(prev_bounds['right_x'][i],self.df[prev_bounds['right_x'][i]],c = int_palette[i], marker = 'o')
            #print('DEBUG:plot'+str(i))
        
        # Plotting commands for bounds
        if 'keep_bounds' in kwargs:
            if 'add_bounds' in kwargs:
                # Add bound to list or initialize list with bound
                if kwargs['add_bounds'] == 'left':
                    if intCount.get()==0:
                        self.lBoundsList = [left.get()]
                    else:
                        self.lBoundsList.append(left.get())
                # Add bound to list or initialize list with bound
                elif kwargs['add_bounds'] == 'right':
                    if intCount.get()==0:
                        self.rBoundsList = [right.get()]
                    else:
                        self.rBoundsList.append(right.get())
            
            for i in [self.lBoundsList,self.rBoundsList]:
                # plot bounds
                try:
                    ax.plot(i[-1], self.df[i[-1]],'oc')
                    ax.plot(i[:-1], self.df[i[:-1]],'om')
                except IndexError:
                    ax.plot(i, self.df[i],'oc')
            
        # reset if not keeping integral bounds
        else:
            self.lBoundsList = []
            self.rBoundsList = []
            intCount.set(0)
            
            
        # Plotting commands for selected peak
        if 'selected_peak' in kwargs:
            # get peak number
            #idx = np.where(self.peaks == self.selected_peak)[0][0]
            ax.plot(self.selected_peak,self.df[self.selected_peak],'r^', markersize = 10)
        # Prevent errors of a previously assigned selected_peak
        else:
            self.selected_peak = None

        # Plot average of first 100 points for nanopore baseline
        ax.plot(np.ones(numpoints)*pore_baseline,'-r')
    
    
        # Plot mean event current (experimental)
        if dnafit.get() == True:
            # Coefficients for the unscaled and unshifted basis polynomials
            coef = np.polynomial.Polynomial.fit(self.df.index[dna_baseline_pts.get():-dna_baseline_pts.get()],
                                                self.df[dna_baseline_pts.get():-dna_baseline_pts.get()],deg = 1).convert().coef
            b = np.polynomial.Polynomial(coef)
            ax.plot(self.df.index[dna_baseline_pts.get():-dna_baseline_pts.get()],b(self.df.index[dna_baseline_pts.get():-dna_baseline_pts.get()]))
        
        
        # Plotting command for horizontal line at marker
        if 'plot_hline' in kwargs:
            ax.plot(np.arange(numpoints),np.ones(numpoints)*self.df[marker.get()],'y')
    
        # peak stuff
        self.peaks = 0
            
        # Peak finding
        if findpeak.get() == True:
            # Find peaks
            # rel height used for width
            self.peaks, self.properties = signal.find_peaks(-self.df, height = -dna_baseline,
                                                            prominence=(prommin.get(),
                                                                        prommax.get()), 
                                                            wlen = wlen.get(),
                                                            width = width.get(), 
                                                            rel_height = rel_height.get())
            
            # event prediction with slope
            if len(self.peaks) >= npeaks.get() and dnafit.get() == True:
                if coef[1]>0:
                    ev_pred.set(' '+pos_slope_sig.get()+' '+str(len(self.peaks)))
                elif coef[0]<0:
                    ev_pred.set(' '+neg_slope_sig.get()+' '+str(len(self.peaks)))
            else:
                # start of excluding events based on peak number
                if excluden.get() == True:
                    print(f'DEBUG: Should exclude {self.plotlist}')
                    self.plot(self.canvas,self.ax,1,0)
                else:
                    #ev_pred.set(' this would be excluded' + str(len(self.peaks)))
                    ev_pred.set(f'{str(len(self.peaks))} peaks detected')
            
            ax.plot(self.peaks, self.df[self.peaks],'x')

            ax.vlines(x=self.peaks, ymin=self.df[self.peaks] + self.properties["prominences"],
                       ymax = self.df[self.peaks], color = "C1")
            ax.hlines(y=-self.properties["width_heights"], xmin=self.properties["left_ips"],
                       xmax=self.properties["right_ips"], color = "C2")
            #ax.hlines(y=-self.properties["width_heights"], xmin=self.properties["left_bases"],
            #           xmax=self.properties["right_bases"], color = "C3")
            
            peak_sig_list = [p1s_sig.get(),p2s_sig.get(),p3s_sig.get(),p4s_sig.get()]
            if len(self.peaks) >= npeaks.get():
                for i in range(len(peak_sig_list)):
                    try:
                        ax.annotate(peak_sig_list[i],xy=(self.peaks[i],self.df[self.peaks[i]]),
                                    xytext = (0,-20),textcoords = 'offset points', ha = 'center')
                    except IndexError:
                        pass
        else:
            ev_pred.set('false findpeaks')
        
        # Set title from tdms import to verify that tkinter isn't messing up 
        ax.set_title(str(self.currplot))
        
        # Draw plot
        canvas.draw()

        # Set new counter and marker values 
        counter.set(counter.get() + dCounter)
        marker.set(marker.get()+dMarker)
        
                
    # Bundle set up functions
    def initStart(self, event=None):
        print('SegFault: Marker 4')
        
        self.storeFilepath()
        self.loader2()

        # Error message 
        if evimport.get()==True and catimport.get()==False:
            print('Oops! You tried to display only a category but did not say of what')
        
        # Disable input fields to prevent mistypes
        self.enter_rcattype['state']  = tk.DISABLED
        self.enter_dataframe['state'] = tk.DISABLED
        self.enter_intdfpath['state'] = tk.DISABLED
        self.enter_cusdfpath['state'] = tk.DISABLED
        self.enter_filepath['state']  = tk.DISABLED
        self.enter_evlist['state']    = tk.DISABLED
            
        # Plot first event
        self.plot(self.canvas,self.ax,0,0)

    
    # Function to store filepath of tdms files for plotting
    # and detect 0 indexing or 1 indexing        
    def storeFilepath(self,event=None):
        
        if filepath.get()[-4:] == 'hdf5':
            HDF5format.set(True)
            print('INFO: HDF5 format identified')
            
            self.HDF5file=h5py.File(filepath.get(),'r+')
            self.filelist = list(self.HDF5file.keys())
            
            if self.filelist[0][-1] == '0':
                indexed0.set(True)
                print('INFO: 0 indexed')
            elif self.filelist[0][-1] == '1':
                indexed1.set(True)
                print('INFO: 0 indexed')
            else:
                print('OOPS: Not recognized as 0 or 1 indexed')
            
        elif filepath.get()[-1:] == '/':
            TDMSformat.set(True)
            print('INFO: TDMS format identified')
        
            # Make a list of all tdms files in folder
            self.filelist = []
            for file in sorted(glob.glob(filepath.get()+'*.tdms')):
                self.filelist.append(file)
            
            # Remove Timed events
            if exists(filepath.get()+'Timed events.tdms'):
                self.filelist = self.filelist[:-1]
            else:
                pass
            
            try:
                if self.filelist[0][-6:-5] == '0':
                    indexed0.set(True)
                    print('INFO: 0 indexed')
                elif self.filelist[0][-6:-5] == '1':
                    indexed1.set(True)
                    print('INFO: 1 indexed')
                else:
                    print('OOPS: Not recognized as 0 or 1 indexed. Ignore for tdms')
            except IndexError:
                print('OOPS: Mistake with filepath?')
    
        # Print filepath and number of files
        print('INFO:filepath = '+filepath.get())
        try:
            print('INFO:number of files = '+str(len(self.filelist)))  
        except AttributeError:
            print('OOPS: Did you forget the last / at the end of the path?')
            
        if len(self.filelist)==0:
            print('Oops! Looks like no files were found. Is your path correct?')
            
        
    def universalLoadDataframe(self, path, marker, name,event = None):
        try:
            tmpdf = pd.read_pickle(path.get())
            print('INFO:'+name+' LOADED:'+path.get())
            print(tmpdf.head(10))
        except FileNotFoundError:
            print('OOPS: Wrong '+name+'df filepath? Maybe you forgot a /')
            
        marker.set(True)
        
        return tmpdf
        
    def loader2(self,event = None):
        
        # ev list is category to display
        # Make a list of which events to plot and then catdf
        if catpath.get() =='': # nothing to load, so make plotlist from all events and make catdf from plotlist
            if TDMSformat.get() == True:
                tmp = []
                for i in self.filelist:
                    tmp.append(int(i[-11:-5])) #number of tdms event
                self.plotlist = np.array(tmp)
            elif HDF5format.get() == True:
                # look at whether tdms is 0 indexed or 1 indexed and make plotlist
                if indexed0.get() == True:
                    self.plotlist = np.arange(len(self.filelist)).tolist()
                elif indexed1.get() == True:
                    self.plotlist = np.arange(1,len(self.filelist)+1).tolist()

            print('INFO:'+str(len(self.plotlist))+' events loaded.')
            
            self.catdf = pd.DataFrame(['']*len(self.filelist),columns = [wcattype.get()], index = self.plotlist)
            catimport.set(False)
            
        else: # import catdf, use evlist (if given) or rcattype (if given) to make plotlist
            evimport.set(True)
            #print('DEBUG:',catpath.get())
            try:
                if catpath.get() !='':
                    self.catdf = self.universalLoadDataframe(catpath, catimport, 'catdf')
                else:
                    print('OOPS: You did not specify a catpath')
                    
                if evlist.get() != '': # events of the specific category name
                    if notLoad.get() == True:
                        self.plotlist = self.catdf.loc[self.catdf[rcattype.get()]!=evlist.get()].index.to_list()
                        print('INFO:'+str(len(self.plotlist))+' events of not name '+evlist.get()+' restored.')
                    else:
                        self.plotlist = self.catdf.loc[self.catdf[rcattype.get()]==evlist.get()].index.to_list()
                        print('INFO:'+str(len(self.plotlist))+' events of name '+evlist.get()+' restored.')
                    wcattype.set(rcattype.get())

                elif rcattype.get() != '': # all events of the read category type
                    if notLoad.get() == True:
                        self.plotlist = self.catdf.loc[self.catdf[rcattype.get()]==''].index.to_list()
                        print('INFO:'+str(len(self.plotlist))+' not categorized events of type '+rcattype.get()+' restored.')
                    else:
                        self.plotlist = self.catdf.loc[self.catdf[rcattype.get()]!=''].index.to_list()
                        print('INFO:'+str(len(self.plotlist))+' categorized events of type '+rcattype.get()+' restored.')
                    wcattype.set(rcattype.get())

                else: #nothing specified, load all
                    self.plotlist = self.catdf.index.to_list()
                    print('INFO:'+str(len(self.plotlist))+' events restored.')
                
            except KeyError:
                print('Oops! Pick a Read Category Type that exists as a column in catdf!')
                    
             
        if intpath.get() !='':
            self.intdf = self.universalLoadDataframe(path = intpath, marker = intimport, name = 'intdf')
        # Initialize dataframe 
        else: 
            if TDMSformat.get() == True:
                array1 = self.plotlist
            elif HDF5format.get() == True:
                if indexed0.get() == True:
                        array1 = np.arange(len(self.filelist))
                elif indexed1.get() == True:
                        array1 = np.arange(1,len(self.filelist)+1)

            array2 = np.arange(10) # max number of integrals per event HARDCODE
            multiindex = pd.MultiIndex.from_product([array1,array2],
                                                    names = ['Event Index','Integral Number'])
            self.intdf = pd.DataFrame(columns = ['Integral','deltaI','event_num',
                                                 'left_x','right_x','minI_x',
                                                 'left_y','right_y','minI_y','m','b',
                                                 'Rel Pos'],
                                      index = multiindex)
            #self.intdf.fillna('', inplace = True)
            
                                            
            intimport.set(False)
            
        if cuspath.get() !='':
            self.cusdf = self.universalLoadDataframe(path = cuspath, marker =  cusimport, name = 'cusdf')
        else:
            self.cusdf = pd.DataFrame(columns = ['Left','Right','Duration (ticks)', 'Duration (ms)'], index = self.plotlist)
            cusimport.set(False)
            
            
    #def universalMakePlotlist(self, event = None):
    #    pass


    # compute relative position of peaks given left and right bounds of molecule
    def peakRelPos(self, df):
        min_l = df.loc[df['left_x'] == df['left_x'].min()]
        max_r = df.loc[df['right_x'] == df['right_x'].max()]
        if min_l.equals(max_r) and len(min_l)>0:
            left_edge = min_l['left_x'].values[0]
            right_edge = max_r['right_x'].values[0]
            df['Percent'] = (df['minI_x']-left_edge)/(right_edge-left_edge)
        return df
        
    
    def intStoreDataframe(self, intdf,savename,event = None):
        # apply function
        intdf = intdf.groupby(level=0, group_keys = False).apply(self.peakRelPos)

        # actually save dataframe
        self.universalStoreDataframe(intdf, savename)
        
            
    # Save function
    def universalStoreDataframe(self, df,savename,event = None):
        if filepath.get() != '':
            df.to_pickle(filepath.get()+savename.get()+'.pkl')
            print('SAVED:'+filepath.get()+savename.get()+'.pkl')
        else:
            df.to_pickle(os.getcwd()+'/'+savename.get()+'.pkl')
            print('SAVED:'+os.getcwd()+'/'+savename.get()+'.pkl')
            
            
    def open_file(self, event = None):
        if navHDF5.get() == True:
            filetypes = (('HDF5 files', '*.hdf5'),)

            filepath.set(tk.filedialog.askopenfilename(title='Open a file',
                                                       initialdir = os.getcwd(),
                                                       filetypes=filetypes))
        
        else:
            filepath.set(tk.filedialog.askdirectory(initialdir = os.getcwd())+'/')
            
            
    def setCat(self, letter, event = None):
        # Store category in dataframe at correct index
        self.catdf[wcattype.get()][self.currplot] = letter
        # Update 2nd to last and last category sets
        prevv_cat.set(prev_cat.get())
        prev_cat.set('Event #'+str(self.currplot)+' set as '+letter)
        # Also print to console
        print('Event #'+str(self.currplot)+' is '+letter)
        
        # Advance plot to next event if not at end of list
        if self.currplot != self.plotlist[-1]:
           self.plot(self.canvas,self.ax,1,0)
        
        
    # Mouse press    
    def on_mouse_press(self, event):
        # Left mouse button selects a new marker
        if event.button == 1:
            try:
                tmp_x = round(event.xdata) # closest index to mouse press
                marg  = 25                 # number of points to look around
                
                shortest_distance = 50 # initialize to value that is large
                shortest_x = tmp_x     # initialize where to set marker
                
                # needed to weight x and y distances correctly since it does it based
                # on the graph, not on the canvas
                
                # Find closest point of all nearby points to click
                for i in np.arange(tmp_x-marg,tmp_x+marg):
                    # distance from mouse press to point
                    event_distance = np.sqrt(((event.xdata-i)/xyscale.get())**2+(event.ydata-self.df[i])**2)
                    # save if distance is shorter
                    if event_distance<shortest_distance:
                        shortest_distance = event_distance
                        shortest_x = i
            
                marker.set(shortest_x)
                # Update plot
                self.plot(self.canvas,self.ax,0,0, keep_bounds = True)
            except TypeError:
                print('Click on the graph itself.')
        # Right mouse button:
        elif event.button == 3:
            if findpeak.get() == False:
                print('OOPS! Please enable find peaks')
            else:
                # find closest peak to mouse press
                self.selected_peak = min(self.peaks, 
                                        key=lambda x:abs(x-event.xdata))
                self.plot(self.canvas,self.ax,0,0, keep_bounds = True,
                          selected_peak = True)
      
    # for debugging            
    def key_press(self, evt):
        print(evt)        
            
        
    # also for debugging        
    def on_key_press(self, event):
        print('you pressed', event.key, event.xdata, event.ydata)
        
        
    # function to take marker to next local maximum    
    def detectNextPeak(self,event=None):
        minPassed = 0
        leftBound = self.df[marker.get()]
        for i in np.arange(marker.get(),len(self.df)):
            # detect when the minimum has been passed
            if minPassed == 0:
                if self.df[i]<leftBound:
                    minPassed = 1
                else:
                    pass
            # once minimum has been passed, stop at a maximum
            else:
                # do this instead of < i-1 to avoid getting stuck in a minimum
                if self.df[i]>self.df[i+1]: 
                    marker.set(i)
                    self.plot(self.canvas,self.ax,0,0, keep_bounds = True)
                    break
                else:
                    pass    
        
    
    def setLeft(self, event = None):
        left.set(marker.get())
        self.plot(self.canvas,self.ax,0,0,keep_bounds=True, add_bounds = 'left')
        print('Left bound set to point '+str(left.get()))
        
        
    def setRight(self, event = None):
        right.set(marker.get())
        self.plot(self.canvas,self.ax,0,0,keep_bounds=True, add_bounds = 'right')
        print('Right bound set to point '+str(right.get()))
        
        
    def integrate(self, event = None):
        # calculate slope
        m = (self.df.iloc[right.get()]-self.df.iloc[left.get()])/(right.get()-left.get())
        # calculate y-intercept
        b = self.df.iloc[left.get()]-m*left.get()
        
        # initialize integral
        integral = 0
        minI = left.get()
        
        # calculate integral
        for i in np.arange(left.get(),right.get()):
            h1 = self.line(i,m,b)-self.df.iloc[i]
            h2 = self.line(i+1,m,b)-self.df.iloc[i+1]
            base  = 1
            # trapezoidal sum
            integral += 1/2*base*(h1+h2)
            
            if self.df.iloc[i]<self.df.iloc[minI]:
                minI = i
                
        dI = self.line(minI,m,b)-self.df.iloc[minI]
        
        # New implemetation of integral dataframe
        # slice to get entire row
        int_num = self.intdf.loc[self.currplot].index[self.intdf.loc[self.currplot].isna().all(axis=1)][0]
        self.intdf.loc[(self.currplot,int_num),:] = [integral, dI, self.plotlist[counter.get()],
                                                     left.get(),right.get(), minI, 
                                                     self.df.iloc[left.get()],
                                                     self.df.iloc[right.get()],
                                                     self.df.iloc[minI], m,b,0]

        
        print(f'{m = :.3f},{b = :.3f},{integral = :.3f} , {minI = }, L = {left.get()}, R={right.get()})')
        
        # Increase counter beacuse a peak was integrated
        intCount.set(intCount.get()+1)
        
        
    def integrateSelPeak(self, event = None, **kwargs):
        # find index of selected peak
        idx = np.where(self.peaks == self.selected_peak)[0][0]
        
        # set left and right bounds if applicable
        if 'auto_l' in kwargs:
            left.set(self.properties['left_ips'][idx])
            print('Using automatic left bound')
        if 'auto_r' in kwargs:
            right.set(self.properties['right_ips'][idx])
            print('Using automatic right bound')
        
        # run integrate function
        self.integrate()
        
        
    # equation for a line
    def line(self, x,m,b):
        return m*x+b
    
        
    def eventDuration(self, event = None):
        durationpt = right.get()-left.get()
        tickrate   = 1e6 # frequency of measurment in Hz
        durationms = durationpt/tickrate*1e3 # duration in milliseconds 
        self.cusdf.loc[self.currplot] = [left.get(),right.get(),
                                         durationpt, durationms]
        print(f'Measured event {self.currplot} to be {durationpt} ticks long.')
    
    
    # # Custom event can be changed based on needs of analysis. This is just an example
    # def customEvent(self, event = None):
    #     self.cusdf.loc[self.currplot] = [left.get(),right.get(),right.get()-left.get()]
        
    # Cosine Similarity Method
    def customEvent(self, event=None):
        # peak depths to consider
        peak_heights = [0,1,2] 
        # create all possible peak depth permutations
        barcode_space = []
        for i in peak_heights:
            for j in peak_heights:
                for k in peak_heights:
                    barcode_space.append([i,j,k])
        # remove 000 from consideration because no similarity to 0 vector
        barcode_space.remove([0,0,0]) 
        
        # Initailize values
        best_score = 0
        best_match = ['No Match']
        scores     = {}    # dictionary to store scores
        threshold  = 0.005 # threshold to say barcode isn't clear

        # get window width where barcode is
        window = self.df[left.get():right.get()]
        # scale window to be from 0 to 2 (could just do to 0 to 1 but will be normalized anyways)
        window = -(window-window.min())/(window.max()-window.min())*2+2
        # normalize area of window
        window  /= sum(window)
        
        tmppeaks, tmpproperties = signal.find_peaks(window, height = 0.004, distance = len(window)/6)
        
        for i in barcode_space:
            if len(tmppeaks) == len([j for j in i if j != 0]):
                print(f'Considering {i}, {len(tmppeaks)} = {len([j for j in i if j != 0])}')
                search = np.ones(len(peak_heights)*round((right.get()-left.get())/len(peak_heights)))
                search[:int(len(search)/len(peak_heights))]     = i[0]
                search[ int(len(search)/len(peak_heights))  :int(len(search)/len(peak_heights)*2)] = i[1]
                search[ int(len(search)/len(peak_heights)*2):]  = i[2]
                
                peak_len = np.floor((right.get()-left.get())/3)
                peak_x = np.linspace(-peak_len/2,peak_len/2,int(peak_len))
                
                # each Guassian's area is proportional to height
                ideal_y = np.concatenate((i[0]*np.exp(-peak_x**2/(3*peak_len)),
                                          i[1]*np.exp(-peak_x**2/(3*peak_len)),
                                          i[2]*np.exp(-peak_x**2/(3*peak_len))))
                
                # pad with zeroes on the right side to make arrays the same length
                if (right.get()-left.get())-len(ideal_y) > 0:
                    ideal_y = np.concatenate((ideal_y,np.zeros((right.get()-left.get())-len(ideal_y))))
                
                # scale so area is 1 (not needed, will do normalization in dot product)
                #ideal_y /= sum(ideal_y)
    
                # Compute dot product
                score = np.dot(ideal_y,window)/np.sqrt(np.dot(ideal_y,ideal_y))
                # Store dot product in dictionary
                scores[str(i)] = score
                if score > best_score:
                    best_score, best_match = score, i
                    
                print(f'{score:.2e},{i}')
                
                #cc = np.correlate(window,ideal_y)
                #print(np.max(cc)/sum(i),i)
            else:
                print(f'Not considering {i}')
                pass
        
        
        # Filter to include close scores
        close_scores = [barcode for barcode in scores if 0 < max(scores.values())-scores[barcode] < threshold]
        # reverse barcodes if molecule is going backwards
        if np.average([left.get(),right.get()])>len(self.df)/2:
            best_match = best_match[::-1]
            # Should fix formatting to be nicer
            close_scores = [i[::-1] for i in close_scores]
            
        # Display
        print(f'{len(tmppeaks)} peaks found')
        print(f'Best score = {best_score:.2e},{best_match}')
        cos_pred.set(f'{best_match}|{close_scores}') # Display in GUI
        if close_scores:
            print(f'Uncertain: Could also be {close_scores}')
        

    # restore button function, useful if you mess up an entry
    def enableButtons(self, event=None):
        self.enter_rcattype['state']   = tk.NORMAL
        self.enter_dataframe['state']  = tk.NORMAL
        self.enter_intdfpath['state']  = tk.NORMAL
        self.enter_cusdfpath['state']  = tk.NORMAL
        self.enter_filepath['state']   = tk.NORMAL
        self.enter_evlist['state']     = tk.NORMAL
        
        
    # Import file and convert to dataframe for plotting or processing
    def loadEvent(self,i):
        if HDF5format.get() == True:
            print('DEBUG: Attempting to load HDF5 event '+self.filelist[i-1])
            self.df = pd.DataFrame(self.HDF5file[self.filelist[i-1]]['current_nA'][...])
            print('DEBUG: Length of self.df is '+str(len(self.df)))
                        
            # get shape to work
            self.df = self.df.squeeze()
            
            return self.df
        elif TDMSformat.get() == True:

            # No plus one because i should always be the number of the file
            tdms_file = TdmsFile.read(filepath.get()
                                      +'Separateevent'+' '*(6-len(str(i)))
                                      +str(i)+'.tdms')
    
            self.df = tdms_file.as_dataframe(time_index=False, absolute_time=False,
                                        scaled_data=True).iloc[:,0]
            
            return self.df
        
        else:
            print('OOPS: Did not recognize event type as either HDF5 or TDMS.')

    
    # Automated categorization (experimental)    
    def headless_categorize(self, event=None):
        if auto_type.get() == 'Slope':
            if pos_slope_sig == '' or neg_slope_sig == '':
                print('Oops! You tried to automatically categorize by slope but'
                      + ' did not specify what the slope means.')
            else:
                for i in self.plotlist:
                    self.df = self.loadEvent(i)
                    
                    coef = np.polynomial.Polynomial.fit(self.df.index[dna_baseline_pts.get():-dna_baseline_pts.get()],
                                                        self.df[dna_baseline_pts.get():-dna_baseline_pts.get()],deg = 1).convert().coef
                     
                    if coef[1]>0:   # positive slope
                        meaning = pos_slope_sig.get()
                    elif coef[0]<0: # negative slope
                        meaning = neg_slope_sig.get()
                        
                    try:
                        self.catdf[acattype.get()][i] = meaning
                    except KeyError:
                        print('INFO: Category Type Not Previously Used: Initializing')
                        self.catdf[acattype.get()] = ['']*len(self.filelist)
                        self.catdf[acattype.get()][i] = meaning
                print('INFO: Slope categorization done!')
        
        elif auto_type.get() == 'npeaks':
            if min_npeaks == '' or max_npeaks == '':
                print('Oops! You tried to automatically categorize by npeaks but'
                      + ' did not specify minimum or maximum.')
            else:
                for i in self.plotlist:
                    self.df = self.loadEvent(i)
                    
                    dna_baseline  = np.mean(self.df)
                    
                    peaks, properties = signal.find_peaks(-self.df, height = -dna_baseline,
                                                          prominence=(prommin.get(),
                                                                      prommax.get()), 
                                                          wlen = wlen.get(),
                                                          width = width.get(), 
                                                          rel_height = rel_height.get())
                    
                    if min_npeaks.get() < len(peaks) < max_npeaks.get():
                        meaning = include_npeaks_sig.get()
                    else:
                        meaning = exclude_npeaks_sig.get()
                        
                    try:
                        self.catdf[acattype.get()][i] = meaning
                    except KeyError:
                        print('INFO: Category Type Not Previously Used: Initializing')
                        self.catdf[acattype.get()] = ['']*len(self.filelist)
                        self.catdf[acattype.get()][i] = meaning
                print('INFO: npeaks categorization done!')
                    
        elif auto_type.get() == 'Auto Integrate':
            print('Add functionality')

        else:
            print('Oops! Analysis type not specified')
        

print('SegFault: Before root creation')
        
root = tk.Tk()

# Tkinter thinks 2880x1800 screen is 1440x900
width = root.winfo_screenwidth()
height = root.winfo_screenheight()

print ('INFO: Window resolution ',width, height)

# Initialize variables
counter   = tk.IntVar()     # Which event to plot
marker    = tk.IntVar()     # Which point to plot in event
left      = tk.IntVar()     # Left bound
right     = tk.IntVar()     # Right bound

intCount  = tk.IntVar()     # Number of times a peak was integrated on a graph

catimport = tk.BooleanVar() # Tracks whether category dataframe was restored
intimport = tk.BooleanVar() # Tracks whether integral dataframe was restored
cusimport = tk.BooleanVar()
evimport  = tk.BooleanVar() # Tracks whether a custom list was imported

ptorline  = tk.BooleanVar() # Plot either points or lines
dnafit    = tk.BooleanVar() # Plot DNA baseline linear fit

findpeak  = tk.BooleanVar() # Plot found peaks
excluden  = tk.BooleanVar() # Exclude if peaks is lower than n
npeaks    = tk.IntVar()     # Number of peaks in an event
prommin   = tk.DoubleVar()
prommax   = tk.DoubleVar()
wlen      = tk.DoubleVar()
width     = tk.DoubleVar()
rel_height= tk.DoubleVar()

pore_baseline_pts = tk.IntVar()
dna_baseline_pts  = tk.IntVar()
xyscale           = tk.IntVar()        # amount that x-axis is larger than y axis

pos_slope_sig     = tk.StringVar() # significance of event with pos slope
neg_slope_sig     = tk.StringVar() # significance of event with neg slope
p1s_sig           = tk.StringVar() # 1st peak significance
p2s_sig           = tk.StringVar() # 2nd peak significance
p3s_sig           = tk.StringVar() # 3rd peak significance
p4s_sig           = tk.StringVar() # 4th peak significance

auto_type = tk.StringVar() # Type of analysis to automate

min_npeaks = tk.IntVar() # exclude less than
max_npeaks = tk.IntVar() # exclude more than
include_npeaks_sig = tk.StringVar()
exclude_npeaks_sig = tk.StringVar()

navHDF5    = tk.BooleanVar() # browse for hdf 5 files
notLoad    = tk.BooleanVar() # modify loading behavior to exclude types
TDMSformat = tk.BooleanVar()
HDF5format = tk.BooleanVar()
indexed0   = tk.BooleanVar()
indexed1   = tk.BooleanVar()
indexedC   = tk.BooleanVar() # indicates that a custom index is used
catpath  = tk.StringVar()    # Path to saved categories
intpath  = tk.StringVar()    # Path to saved integrals
cuspath  = tk.StringVar()    # Path to custom
filepath = tk.StringVar()    # Path to folder with tdms files
evlist   = tk.StringVar()    # Which event type to display
catdf_savename = tk.StringVar()
intdf_savename = tk.StringVar()
cusdf_savename = tk.StringVar()

wcattype = tk.StringVar()  # Category type to write
acattype = tk.StringVar()  # Category type to set in headless mode
rcattype = tk.StringVar()  # Category to read
cat0     = tk.StringVar()
cat1     = tk.StringVar()
cat2     = tk.StringVar()
cat3     = tk.StringVar()
cat4     = tk.StringVar()
cat5     = tk.StringVar()
cat6     = tk.StringVar()
cat7     = tk.StringVar()
cat8     = tk.StringVar()
cat9     = tk.StringVar()

curr_cat = tk.StringVar()
prev_cat = tk.StringVar()
prevv_cat = tk.StringVar()
cos_pred  = tk.StringVar() # Cosine similarity prediction
ev_pred   = tk.StringVar()

# Run app
app = Application(master=root)
app.mainloop()