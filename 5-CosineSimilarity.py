#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy   as np
import pandas  as pd

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

import inspect
import os

# Path to current file
path = inspect.getfile(inspect.currentframe())
# Path to directory that file is in
directory = os.path.realpath(os.path.dirname(path))
# Change directory 
os.chdir(directory)

import thesisutils as tu

#%% Cosine Similarity work flow

ref_peaks = pd.read_pickle('filepath/Lab 2 - Keyser/MPhil Thesis/P27_refpeaks_standardized.pkl')

ref_peaks.dropna(inplace=True)

event_path = 'filepath/Lab 2 - Keyser/ANA_02_Jinbo 81 Carrier library/2022.10.09_27 mix/unfolded_p1/'

df = tu.eventLoader(event_path,16) # load event

fig = plt.figure(figsize=(6.26,8))
gs = gridspec.GridSpec(3,1, height_ratios = (1,1,3),
                       left = 0.01, right = 0.99)
gs2 = gridspec.GridSpecFromSubplotSpec(1, 3, width_ratios=(1,8,1),
                                       subplot_spec=gs[0])
gs0 = gridspec.GridSpecFromSubplotSpec(1, 2, wspace = 0.7, hspace = 0.3,  
                                       subplot_spec=gs[1])
gs1 = gridspec.GridSpecFromSubplotSpec(3, 3, subplot_spec=gs[2], wspace = 0.1,hspace=0.1)

axraw = fig.add_subplot(gs2[0,1])
axbar = fig.add_subplot(gs0[0,0])
axwin = fig.add_subplot(gs0[0,1])

n_ideal = 9
ax_dict = {}
for i in range(n_ideal):
    ax_dict[f'ax{i}'] = fig.add_subplot(gs1[int(i/np.sqrt(n_ideal)),
                                            int(i%np.sqrt(n_ideal))])
    ax_dict[f'ax{i}'].axis('off')


left_xs  = ref_peaks.loc[16,'left_x']
right_xs = ref_peaks.loc[16,'right_x']

left = right_xs[0]
right = left_xs[1]

axraw.plot(left,  df[left], 'o', color = tu.palette[5])
axraw.plot(right, df[right],'o', color = tu.palette[5])

tu.current_time(df,axraw, 50, 0.15,8,3, fracxoff= 0.058)
axraw.annotate('A.',(0,1.05),xycoords = 'axes fraction',
               fontsize = 9,fontweight = 'bold')

# right of the left ref peak and left of the right ref peak
chopped_signal = df[left:right]
axbar.plot(left,  chopped_signal[left], 'o', color = tu.palette[5])
axbar.plot(right-1, chopped_signal[right-1],'o', color = tu.palette[5])
tu.current_time(chopped_signal,axbar, 50, 0.15, 8,3, fracxoff = 0.5)
axbar.annotate('B.',(0,1.05),xycoords = 'axes fraction',
             fontsize = 9,fontweight = 'bold')


# get window width where barcode is
window = df[left:right]
# scale window to be from 0 to 2 (could just do to 0 to 1 but will be normalized anyways)
window = -(window-window.min())/(window.max()-window.min())*2+2
# normalize area of window
window  /= sum(window)

tu.current_time(window,axwin,20,0.01,5,4, fracyoff = 0.6)

axwin.annotate('C.',(0,1.05),xycoords = 'axes fraction',
             fontsize = 9,fontweight = 'bold')

r1 = 0.58
r2 = 0.05
r3 = 0.33
r4 = -0.4
axraw.annotate('', xy=(r1, r2), xycoords='axes fraction', xytext=(r3, r4), 
                        arrowprops=dict(arrowstyle="<-", color='k',linewidth=1.5,
                                        connectionstyle="arc3")) 
axraw.annotate('Isolate Barcode', xy=(0, 0), xycoords='axes fraction', xytext=(r3,r4+0.1), 
               rotation = 24)  
 
axbar.annotate('', xy=(1.04, 0.5), xycoords='axes fraction', xytext=(1.54, 0.5), 
                        arrowprops=dict(arrowstyle="<-", color='k',linewidth=1.5,
                                        connectionstyle="arc3")) 
axbar.annotate('Transform',xy=(1, 0.6), xycoords='axes fraction', xytext=(1.13,0.6))

ideal_barcodes = [[0,1,2],[0,2,1],[1,0,1],
                  [1,0,2],[1,1,0],[1,2,0],
                  [2,0,1],[2,1,0]]

peak_len = np.floor((len(chopped_signal))/3)
peak_x = np.linspace(-peak_len/2,peak_len/2,int(peak_len))

labels = ['D.','E.','F.','G.','H.','I.','J.','K.','L.']

for idx,barcode in enumerate(ideal_barcodes):

    # each Guassian's area is proportional to height
    ideal_y = np.concatenate((barcode[0]*np.exp(-peak_x**2/(3*peak_len)),
                              barcode[1]*np.exp(-peak_x**2/(3*peak_len)),
                              barcode[2]*np.exp(-peak_x**2/(3*peak_len))))
    
    # Make 0's 0.1's to appear better in graph
    ideal_y[ideal_y < 0.02] = 0.02

    # pad with zeroes on the right side to make arrays the same length
    if len(chopped_signal)-len(ideal_y) > 0:
        ideal_y = np.concatenate((ideal_y,np.zeros((len(chopped_signal))-len(ideal_y))))
    
    #ideal_y /= sum(ideal_y)
    
    ax_dict[f'ax{idx}'].plot(ideal_y)
    ax_dict[f'ax{idx}'].set_ylim(0,2)
    ax_dict[f'ax{idx}'].annotate(labels[idx],(0,0.88),xycoords = 'axes fraction',
                                 fontsize = 9,fontweight = 'bold')
    

plt.savefig('filepath/Lab 2 - Keyser/MPhil Thesis/5-Schematic-2.png', dpi = 300)



#%%

# Bins for making histogram
def binner(df):
    lengths = []
    for count in range(max_counts):
        lengths.append(len(df.loc[df['Close Scores']==count]))
        
    # for count in df['Close Scores'].unique():
    #     lengths_dict[count] = len(df.loc[df['Close Scores']==count])
        
    return lengths

df = pd.read_excel('filepath/Lab 2 - Keyser/MPhil Thesis/Cosine Similarity Evaluation v2.xlsx',
                   header = None)

max_counts = 7 # most in a single category

df = df[[0,1,2]]
df.columns = ['Accuracy','Close Scores','Barcode']

# Fix excel formatting
df.loc[df['Barcode'] == 3, 'Barcode'] = 'Not Readable'
df.loc[df['Barcode'] == 0, 'Barcode'] = '000'
df.loc[df['Barcode'] == 1, 'Barcode'] = '001'
df.loc[df['Barcode'] == 2, 'Barcode'] = '001'
df.loc[df['Barcode'] == 10, 'Barcode'] = '010'
df.loc[df['Barcode'] == 20, 'Barcode'] = '010'
df.loc[df['Barcode'] == 11, 'Barcode'] = '011'
df.loc[df['Barcode'] == 22, 'Barcode'] = '011'
df.loc[df['Barcode'] == 12, 'Barcode'] = '012'
df.loc[df['Barcode'] == 21, 'Barcode'] = '021'
df.loc[df['Barcode'] == 200, 'Barcode'] = '100'
df.loc[df['Barcode'] == 202, 'Barcode'] = '101'
df.loc[df['Barcode'] == 220, 'Barcode'] = '110'
df.loc[df['Barcode'] == 222, 'Barcode'] = '111'

df['Barcode'] = df['Barcode'].astype(str)

df_bestmatch = df.loc[(df['Accuracy']=='b')]
df_close     = df.loc[(df['Accuracy']=='c')]
df_wrong     = df.loc[(df['Accuracy']=='w')]
df_nomatch   = df.loc[(df['Accuracy']=='nm')]
df_notread   = df.loc[(df['Accuracy']=='nr')]
df_000       = df.loc[(df['Accuracy']=='z')]

cat_lens = []
for i in [df_bestmatch,df_close,df_wrong,df_nomatch,df_notread,df_000]:
    cat_lens.append(len(i))
    print(len(i))

dummy = []
for i in [df_bestmatch,df_close,df_wrong]:
    dummy.append(binner(i))
    print(f'Alternative suggested {sum(binner(i)[1:])} times')

# Create figure 
fig = plt.figure(figsize=(6.26,8), dpi=300)
gs = gridspec.GridSpec(2,2, hspace = 0.5, left = 0.1, right = 0.99, bottom = 0.07, top = 0.6)
ax0 = fig.add_subplot(gs[0, 0])
ax1 = fig.add_subplot(gs[0, 1])
ax2 = fig.add_subplot(gs[1, 0])
axleg = fig.add_subplot(gs[1,1])
gspie = gridspec.GridSpec(1,1,left = 0, right = 1, bottom = 0.65, top = 1)
ax3 = fig.add_subplot(gspie[0, 0])

# ax = fig.add_axes((0,0,.5,1))

ax0.bar(x=np.arange(max_counts),height=dummy[0], color = tu.palette[1],
        align = 'center',tick_label = [str(i) for i in np.arange(max_counts)])
ax1.bar(x=np.arange(max_counts),height=dummy[1], color = tu.palette[3],
        align = 'center',tick_label = [str(i) for i in np.arange(max_counts)])
ax2.bar(x=np.arange(max_counts),height=dummy[2], color = tu.palette[5],
        align = 'center',tick_label = [str(i) for i in np.arange(max_counts)])
ax3.pie(x=cat_lens, colors = [tu.palette[1],tu.palette[3],tu.palette[5],tu.palette[7],'#000000','#808080'], 
        labels= ['79','21','24','13','54','9'], frame = False)

for ax in [ax0,ax1,ax2]:
    ax.set_xlabel('# of Alternatives Suggested')
    ax.set_ylabel('Count')

ax0.annotate('B.',(0,1.05), xycoords = 'axes fraction',
             fontsize = 9, fontweight = 'bold')
ax1.annotate('C.',(0,1.05), xycoords = 'axes fraction',
             fontsize = 9, fontweight = 'bold')
ax2.annotate('D.',(0,1.05), xycoords = 'axes fraction',
             fontsize = 9, fontweight = 'bold')
ax3.annotate('A.',(-0.05,0.89), xycoords = 'axes fraction',
             fontsize = 9, fontweight = 'bold')

fig.align_ylabels([ax0,ax2])


h, l = ax3.get_legend_handles_labels()
axleg.legend(h,['Best Match','Close Score','Wrong','No Suggestion','Not Readable','000'], loc = 'lower center')
axleg.axis('off')

#plt.savefig('filepath/Lab 2 - Keyser/MPhil Thesis/5-Cosine Similarity-top.png', dpi = 300)

#%% SAMPLE EVENTS

    
event_path = 'filepath/Lab 2 - Keyser/ANA_02_Jinbo 81 Carrier library/2022.10.09_27 mix/unfolded_p1/'

# Create figure 
fig = plt.figure(figsize=(6.26,6), dpi=300)
# had left as 0.1
gs = gridspec.GridSpec(4,2, hspace = 0.5, left = 0.01, right = 0.99, bottom = 0.25, top = 0.9)
gsleg = gridspec.GridSpec(1,1)
ax0 = fig.add_subplot(gs[0, 0])
ax1 = fig.add_subplot(gs[0, 1])
ax2 = fig.add_subplot(gs[1, 0])
ax3 = fig.add_subplot(gs[1, 1])
ax4 = fig.add_subplot(gs[2, 0])
ax5 = fig.add_subplot(gs[2, 1])
ax6 = fig.add_subplot(gs[3, 0])
ax7 = fig.add_subplot(gs[3, 1])
axleg = fig.add_subplot(gsleg[0,0])

axs = [ax0,ax1,ax2,ax3,ax4,ax5,ax6,ax7]
for ax in axs:
    ax.set_ylim(-0.6,0.05)
    
texts = ['A. (102)','B. (020)','C. (101)','D. (202)','E. (001)','F. (122)','G. (021)','H. Not Readable']

tu.current_time(tu.eventLoader(event_path,41),ax0,100,0.1,8,3, fracxoff = 0.5, color = tu.palette[1])  # 102 b1
tu.current_time(tu.eventLoader(event_path,103),ax1,100,0.1,8,3, color = tu.palette[1]) # 020 b0
tu.current_time(tu.eventLoader(event_path,118),ax2,100,0.1,8,3, fracxoff = 0.5, color = tu.palette[3]) # 101 c2
tu.current_time(tu.eventLoader(event_path,44) ,ax3,100,0.1,8,3, color = tu.palette[1])  # 202 b1
tu.current_time(tu.eventLoader(event_path,109),ax4,100,0.1,8,4, color = tu.palette[7]) # 001 nm
tu.current_time(tu.eventLoader(event_path,10) ,ax5,100,0.1,8,3, color = tu.palette[5])  # 122 b5
tu.current_time(tu.eventLoader(event_path,139),ax6,100,0.1,8,3, color = tu.palette[3]) # 021 w1
tu.current_time(tu.eventLoader(event_path,183),ax7,100,0.1,8,3, color = 'k') # nr

for idx, ax in enumerate(axs):
    ax.annotate(texts[idx],(0,1.05), xycoords = 'axes fraction',
                 fontsize = 9, fontweight = 'bold')
    
axleg.legend(h,['Best Match','Close Score','Wrong','No Suggestion','Not Readable','000'], loc = 'lower center', ncol = 2)
axleg.axis('off')
    
plt.savefig('filepath/Lab 2 - Keyser/MPhil Thesis/5-Sample Events.png', dpi = 300)



#%% Barcode-level accuracy for internal use
import seaborn as sns

df = df.sort_values('Barcode')

df_by_barcode = df.loc[~df['Barcode'].isin(['Not Readable','000'])]

sns.countplot(data = df_by_barcode,x = 'Barcode', hue = 'Accuracy',
              palette = [tu.palette[1],tu.palette[3],tu.palette[5],tu.palette[7]])

ax = plt.gca()
plt.xticks(rotation =  90)

print(df.loc[(df.Accuracy == 'nm')])
print(df.loc[df['Close Scores'] > 3])

plt.figure()
sns.countplot(data = df.loc[df['Close Scores'] > 3],x = 'Barcode', hue = 'Accuracy',
              palette = [tu.palette[1],tu.palette[3],tu.palette[5],tu.palette[7]])




    
    
    
