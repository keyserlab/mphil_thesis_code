# -*- coding: utf-8 -*-

from numba import jit
import numpy as np
import time
import thesisutils2 as tu2


# Create penalty function
def favor_fragment(frag_no):
    global peak_y

    current = np.zeros(tu2.frag_len)
    peak_len = 200 # can be optimized
    peak_x = np.linspace(-peak_len/2,peak_len/2,peak_len)
    peak_y = 2*np.exp(-peak_x**2/(10*peak_len))
    for i in tu2.MTaqI_locations[frag_no]:
        for j in np.arange(-peak_len/2,peak_len/2):
            if i+j<tu2.frag_len:
                j = int(j)
                current[i+j] = max(current[i+j],peak_y[int(j+peak_len/2)])
            else:
                pass
        
    current -= 1
        
    return current


@jit(nopython = 'True')
def GP_mainloop(inputPeaks, ref_max, rem):
    max_score = (-100,-1) # score, ref_no
    
    pmin = min(inputPeaks)
    
    inputPeaks -=pmin
            
    for ref_no in range(ref_max):
        # Skip if ref_no should be skipped
        if ((ref_no in rem) or (ref_no in rem+465)):
            pass
        
        # Otherwise consider it
        else:
            score = 0
    
            for i in range(tu2.frag_len):
                score2 = 0
                for peak in inputPeaks:
                    try:
                        score2 += favorsarr[ref_no][peak-i]
                    except:
                        score2 -= 1
                if score2 > score:
                    score = score2
            
            if score > max_score[0]:
                max_score = (score,ref_no)
                        
    return max_score


# =============================================================================
# USER INPUT
# =============================================================================
input_folder   = 'filepath'
output_folder  = 'filepath'
peak_height = 1.8   # for use with peak finding algorithm
frag_mins   = [0]
frag_maxs   = [len(tu2.MTaqI_locations)] # file with name frag_max-1 must exist
#frag_maxs   = [450]
ref_maxs    = [2*len(tu2.MTaqI_locations)]
revs         = [False,True]
#scales      = [0.1*i+9.5 for i in range(10)]
scales       = [10.5] # causes kernal crash at 10.5?
rems         = [np.array([69, 119]),np.array([-777])]
# if using nadds and ndels add it to input string
#nadds        = [i for i in range(10)] 
#ndels        = [i for i in range(10)]
# =============================================================================

# create list to store penalty functions
favors = []
# make forward fragments
for counter,value in enumerate(tu2.MTaqI_locations):
    favors.append(favor_fragment(counter))
    
# make reverse fragments
for i in range(len(favors)):
    favors.append(favors[i][::-1])

# make favors into numpy array for numba    
favorsarr = np.zeros((len(favors),tu2.frag_len))
for i in range(len(favors)):
    favorsarr[i,:] = favors[i]
          
#%%

for frag_min in frag_mins:
    for frag_max in frag_maxs:
        for ref_max in ref_maxs:
            for rev in revs:
                for scale in scales:
                    for rem in rems:
                    
                        start = time.time()
    
                        inputstring = f'GPNOfrag_len={tu2.frag_len},frags={frag_min}to{frag_max},ref_max={ref_max},rev={rev},scale={scale},rem={rem}'
                        print(inputstring)
                        
                        # initialize result list
                        rl_GP = [inputstring]
                        
                        for frag_no in np.arange(frag_min,frag_max):
                            #print(frag_no)
                            if frag_no%50 == 0:
                                print(f'{frag_no} fragments categorized in {time.time()-start:.0f} seconds')
                                
                            inputPeaks = tu2.loadPeaks(frag_no,input_folder,peak_height)
                            
                            if rev == True:
                                inputPeaks = tu2.reverser(inputPeaks,tu2.frag_len*1.5)
                                
                            scalePeaks = (inputPeaks*scale).round().astype('int64')
                            
                            #scalePeaks = tu2.indel(scalePeaks,nadd,ndel)
                            
                            max_score = GP_mainloop(scalePeaks, ref_max,rem)
                                    
                            rl_GP.append((frag_no,max_score))
                            
                            #print(frag_no)
                        
                        # Save results list
                        tu2.saveResultList(rl_GP,inputstring, output_folder)
                        # Print accuracy
                        tu2.rl_accuracy(rl_GP,inputstring)
                        
                        print(f'One set: {time.time()-start:.0f} seconds ')
