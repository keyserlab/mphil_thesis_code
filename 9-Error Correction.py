#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

import inspect
import os

# Path to current file
path = inspect.getfile(inspect.currentframe())
# Path to directory that file is in
directory = os.path.realpath(os.path.dirname(path))
# Change directory 
os.chdir(directory)

import thesisutils as tu

# Plot codeword and error correcting arrows
def major_minor(ax,x,y,off):
    ax.plot(x,y,marker='.',markersize=10, color = tu.palette[3])
    circle = plt.Circle((x, y), off+0.5, color=tu.palette[2], clip_on=False)
    ax.add_patch(circle)
    for i in np.arange(-off,off+1):
        for j in np.arange(-off,off+1):
            if (i==0) and (j==0):
                pass
            else:
                pass
                #ax.plot(x+i,y+j,marker = '.', markersize = 3, color = 'k')
            if i**2+j**2<(off+0.5)**2:
                ax.annotate('', xy=(x, y), xycoords='data', xytext=(x+i, y+j), 
                            arrowprops=dict(arrowstyle="->", color=tu.palette[5],linewidth=0.5,
                                            connectionstyle=f"arc3,rad = 0.15")) 
                

fig = plt.figure(figsize=(6.26,8), dpi = 300)
gs = gridspec.GridSpec(2,1, left = 0.1, right = 0.78, hspace = 0.1, wspace = 0.3,
                       bottom = 0.05, top = 0.95)

ax0 = fig.add_subplot(gs[0,0])
ax1 = fig.add_subplot(gs[1,0])

# Positions for distance plot
cx0s = np.random.rand(6)
cx0s = np.array([0.2,0.45,0.75,0.85,0.87,0.51])
cy0s = np.random.rand(6)
cy0s = np.array([0.6,0.9,0.85,0.65,0.4,0.2])
las = ['I.','II.','III.','IV.']
las = ['(abcde)','(vwxyz)','(axzve)','(bwydy)','(zyeax)','(cvcwd)']

# Plot distance arrows
for i in range(len(cx0s)):
    for j in range(len(cy0s)):
        ax0.annotate('', xy=(cx0s[i],cy0s[i]), xycoords='data', xytext=(cx0s[j],cy0s[j]), 
                     arrowprops=dict(arrowstyle="<->", color=tu.palette[4],linewidth=1.5,
                                    connectionstyle='arc3'))
        
# Plot points and annotate
for i in zip(cx0s,cy0s,las):
    ax0.plot(i[0],i[1],marker='.',markersize=10, color = tu.palette[3])
    ax0.annotate(i[2],xy=(i[0]+0.05,i[1]+0.05),xycoords = 'data',color = 'k',fontsize=10)
        
# Offsets for error correction plot
off = 3
off2 = 3
# Positions for error correction plot
cxs = np.array([0,0,-off-off2,off+off2])
cys = np.array([off+off2,-off-off2,0,0])
# Plot codewords and error correcting arrows
for i in zip(cxs,cys):
    major_minor(ax1,i[0],i[1],off)

# major_minor(ax0, 0, off+2,off)
# major_minor(ax0, 0, -off-2,off)
# major_minor(ax0,-off-2,0,off)
# major_minor(ax0, off+2,0,off)

# major_minor(ax0, off+1, off+1,off)
# major_minor(ax0,-off-1, off+1,off)
# major_minor(ax0,-off-1,-off-1,off)
# major_minor(ax0, off+1,-off-1,off)
# major_minor(ax0, 3*(off+1)-2,0,off)

xs = np.arange(round(ax1.get_xlim()[0]),round(ax1.get_xlim()[1]))
ys = np.arange(round(ax1.get_ylim()[0]),round(ax1.get_ylim()[1]))

# ax0.set_xlim(ax1.get_xlim())
# ax0.set_ylim(ax1.get_ylim())
ax0.set_xlim(0,1.05)
ax0.set_ylim(0,1.05)

# Plot non-valid codewords
for x in xs:
    for y in ys:
        if x**2+y**2<(3*(off))**2+off2**2:
            ax1.plot(x,y, marker = '.', markersize=3, color='k')
        
# Plot valid codewords again so they are on top
for i in zip(cxs,cys):
    ax1.plot(i[0],i[1],marker='.',markersize=10, color = tu.palette[3])

# Annotate with error-correcting distance
ax1.annotate('', xy=(0, (off+off2)+off+0.5), xycoords='data', xytext=(off, (off+off2)+off+0.5), 
            arrowprops=dict(arrowstyle="-", color='k',linewidth=1.5,
                            connectionstyle=f"bar,fraction = 0.2")) 
ax1.annotate('d', xy=((0+off)/2, (off+off2)+off+1+.45), xycoords='data', horizontalalignment='center')

ax1.axis('equal')
ax1.axis('off')
ax0.axis('off')

ax0.annotate("A.",(0,1.05),xycoords = 'axes fraction',
            fontsize = 9, fontweight = 'bold')
ax1.annotate("B.",(0,1.05),xycoords = 'axes fraction',
            fontsize = 9, fontweight = 'bold')

plt.savefig('filepath/Lab 2 - Keyser/MPhil Thesis/9-ErrorCorrection.png', dpi=300)

