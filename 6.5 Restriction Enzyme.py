#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Import restriction enyzmes to be used
from Bio import SeqIO, Restriction
from Bio.Restriction import (AgeI,ApoI,BamHI,BbsI,BclI,BmtI,BsaI,BsiWI,BsrGI,BstEII,BstZ17I,DraIII,
                             EagI,EcoRI,EcoRV,HindIII,KpnI,MfeI,MluI,NcoI,NheI,NotI,NruI,NsiI,PstI,
                             PvuI,PvuII,SacI,SalI,SbfI,ScaI,SpeI,SphI,SspI,StyI)
import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.ticker as ticker

import inspect
import os

# Path to current file
path = inspect.getfile(inspect.currentframe())
# Path to directory that file is in
directory = os.path.realpath(os.path.dirname(path))
# Change directory 
os.chdir(directory)

import thesisutils as tu

# Custom restriction enzyme class
class RE:
    def __init__(self, name, nfrags, resitelen, bioRE, fraglens,under1kb, over10kb, over50kb):
        self.name      = name       # name of restriction enzyme
        self.nfrags    = nfrags     # number of fragments it cuts
        self.resitelen = resitelen  # number of bases in restriction site
        self.bioRE     = bioRE      # restriction enzyme from Biopython
        self.fraglens  = fraglens   # list of fragment lengths
        self.under1kb  = under1kb   # list of fragment lengths under 1kb
        self.over10kb  = over10kb   # list of fragment lengths over 10kb
        self.over50kb  = over50kb   # list of fragment lengths over 10kb
        
class RE2:
    def __init__(self,name1,name2,nfrags, bioRE1, bioRE2, fraglens,under1kb, over10kb, over50kb):
        self.name1     = name1      # name of first restriction enzyme
        self.name2     = name2      # name of second restriction enzyme
        self.nfrags    = nfrags     # number of fragments it cuts
        self.bioRE1    = bioRE1      # restriction enzyme from Biopython
        self.bioRE2    = bioRE2      # restriction enzyme from Biopython
        self.fraglens  = fraglens   # list of fragment lengths
        self.under1kb  = under1kb   # list of fragment lengths under 1kb
        self.over10kb  = over10kb   # list of fragment lengths over 10kb
        self.over50kb  = over50kb   # list of fragment lengths over 10kb

NEBHF_list =                         [AgeI,ApoI,BamHI,BbsI,BclI,BmtI,
                                      BsaI,BsiWI,BsrGI,BstEII,BstZ17I,
                                      DraIII,EagI,EcoRI,EcoRV,HindIII,
                                      KpnI,MfeI,MluI,NcoI,NheI,NotI,
                                      NruI,NsiI,PstI,PvuI,PvuII,SacI,
                                      SalI,SbfI,ScaI,SpeI,SphI,SspI,StyI]



NEBHF_class = []

for i in NEBHF_list:
    catalysis = i.catalyze(tu.MG1655,linear = False)
    num_frags = len(catalysis)
    fraglens  = [len(j) for j in catalysis]
    

    if len(NEBHF_class) > 0:
        # Check if site is repeated
        conflict_list = [j.bioRE.site for j in NEBHF_class]
        if i.site not in conflict_list:
            #print(j.bioRE.site,i.site)
            NEBHF_class.append(RE(str(i),num_frags,len(i.site),i,fraglens, 
                                  [j for j in fraglens if j<1000],
                                  [j for j in fraglens if j>10000],
                                  [j for j in fraglens if j>50000]))
        else:
            pass
                #print(j.bioRE.site,i.site)
              
    else: # add first restriction enzyme in list
        NEBHF_class.append(RE(str(i),num_frags,len(i.site),i,fraglens, 
                              [j for j in fraglens if j<1000],
                              [j for j in fraglens if j>10000],
                              [j for j in fraglens if j>50000]))
        

long_cutters = []
for idx, re in enumerate(NEBHF_class):
    if len(re.under1kb) < 20:
        long_cutters.append(re.bioRE)
        
# Combination of two restriction enzymes
NEBHF_2class = []
# combo 13 and 22
for idx,firstRE in enumerate(long_cutters):
    for jdx,secondRE in enumerate(long_cutters):
        # Skip if cutting twice with the same restriction enzyme
        if (firstRE == secondRE) or (jdx<idx):
            pass
        else:
            catalysis1 = firstRE.catalyze(tu.MG1655,linear = False)
            catalysis2 = []
            for j in catalysis1:
                catalysis2.append(secondRE.catalyze(j,linear=True))
                
            flattened_catalysis2 = []
            for k in catalysis2:
                for l in k:
                    flattened_catalysis2.append(l)
                    
            num_frags = len(flattened_catalysis2)
            fraglens  = [len(m) for m in flattened_catalysis2]
                    
            NEBHF_2class.append(RE2(str(firstRE),str(secondRE),num_frags,
                                    firstRE,secondRE,fraglens,
                                    [j for j in fraglens if j<1000],
                                    [j for j in fraglens if j>10000],
                                    [j for j in fraglens if j>50000]))
    


#%%


def plotRE(reno,ax, text):
    fraglens = NEBHF_class[reno].fraglens

    site     = NEBHF_class[reno].bioRE.site
    
    rarity = 1
    for base in site:
        rarity *= basedict[base]
    
    countx = tu.MG1655.count(NEBHF_class[reno].bioRE.site)
    
    binmax = int(math.ceil(max(fraglens)/1000)*1000)
    print(max(fraglens),binmax)

    bins = np.arange(0,binmax,binmax/20)
    counts,bins = np.histogram(fraglens,bins=bins)
    bin_width = bins[1] - bins[0]
    xbin = bins[:-1]+bin_width/2
    
    # Remove where there are zero counts
    zero_loc = np.where(counts == 0)[0]
    counts_mod = np.delete(counts,zero_loc)
    xbin_mod   = np.delete(xbin,zero_loc)
    
    line_height = 0.1
    ax.bar(xbin_mod,line_height*np.ones(len(xbin_mod)),bin_width, 
           bottom = np.log(counts_mod)-line_height/2, label = 'MG1655 Genome', color = tu.palette[3])
    
    coef = np.polynomial.Polynomial.fit(xbin_mod, np.log(counts_mod),deg = 1).convert().coef
    f = np.polynomial.Polynomial(coef)
    ax.plot(xbin_mod,f(xbin_mod), label = 'Best Fit', color = tu.palette[5])
    
    print(f'{rarity}, {countx},{coef[1]}')

    
    ax.set_xlabel('Fragment Length (bp)')
    ax.set_ylabel('Ln(Number of Fragments)')
    ax.ticklabel_format(style = 'scientific', scilimits= (0,0))
    
    ax.annotate(f"{text} {NEBHF_class[reno].name}",(0,1.05),xycoords = 'axes fraction',
                fontsize = 9, fontweight = 'bold')
    
    return xbin, counts
    
def plotRE2(reno,ax, text):
    fraglens = NEBHF_2class[reno].fraglens

    site     = NEBHF_class[reno].bioRE.site
    
    rarity = 1
    for base in site:
        rarity *= basedict[base]
    
    countx = tu.MG1655.count(NEBHF_class[reno].bioRE.site)
    
    binmax = int(math.ceil(max(fraglens)/1000)*1000)
    print(max(fraglens),binmax)

    bins = np.arange(0,binmax,binmax/20)
    counts,bins = np.histogram(fraglens,bins=bins)
    bin_width = bins[1] - bins[0]
    xbin = bins[:-1]+bin_width/2
    
    zero_loc = np.where(counts == 0)[0]
    counts_mod = np.delete(counts,zero_loc)
    xbin_mod   = np.delete(xbin,zero_loc)
    
    line_height = 0.1
    ax.bar(xbin_mod,line_height*np.ones(len(xbin_mod)),bin_width, 
           bottom = np.log(counts_mod)-line_height/2, color = tu.palette[3])
    
    coef = np.polynomial.Polynomial.fit(xbin_mod, np.log(counts_mod),deg = 1).convert().coef
    f = np.polynomial.Polynomial(coef)
    ax.plot(xbin_mod,f(xbin_mod), label = 'Best Fit', color = tu.palette[5])
    
    print(f'{rarity}, {countx},{coef[1]}')

    
    ax.set_xlabel('Fragment Length (bp)')
    ax.set_ylabel('Ln(Number of Fragments)')
    ax.ticklabel_format(style = 'scientific', scilimits= (0,0))
    
    ax.annotate(f"{text} {NEBHF_2class[reno].name1}+{NEBHF_2class[reno].name2}",
                (0,1.05),xycoords = 'axes fraction',
                fontsize = 9, fontweight = 'bold')
    
    return xbin, counts


fig = plt.figure(figsize=(6.26,8))
gs = gridspec.GridSpec(3,2, left = 0.1, right = 0.78, hspace = 0.45, wspace = 0.3,
                       bottom = 0.05, top = 0.95, width_ratios = (1,1))
gsleg = gridspec.GridSpec(3,1, left = 0.8, right = 0.9, hspace = 0.45, wspace = 0.4,
                          bottom = 0.05, top = 0.95)
ax0 = fig.add_subplot(gs[0,0])
ax1 = fig.add_subplot(gs[0,1])
ax2 = fig.add_subplot(gs[1,0])
ax3 = fig.add_subplot(gs[1,1])
ax4 = fig.add_subplot(gs[2,0])
ax5 = fig.add_subplot(gs[2,1])
axleg0 = fig.add_subplot(gsleg[0,0])
axleg1 = fig.add_subplot(gsleg[1,0])

basedict =	{
  'A': 1/4,'T': 1/4,'C': 1/4,'G': 1/4,
  'N': 1,
  'B': 3/4,'D': 3/4,'H': 3/4,
  'K': 1/2,'M': 1/2,'R': 1/2,'S': 1/2,'W': 1/2,'Y': 1/2,
}


for idx, re in enumerate(NEBHF_class):
    #plt.plot(idx,over50kb[idx],'^r', label = 'Frags. Over 50kb')
    ax0.plot([idx,idx],[len(re.under1kb),re.nfrags],'-k')
    ax0.plot(idx,re.nfrags,'^r', markersize=5,label = 'a')
    ax0.plot(idx,len(re.under1kb),'_b', label = 'a')
ax0.set_yscale('symlog')
ax0.set_xlabel('Restriction Enzyme Number')
ax0.set_ylabel('Number of Fragments')
ax0.xaxis.set_ticks(np.arange(0, 40, 5))
    
for idx, re in enumerate(NEBHF_2class):
    #plt.plot(idx,over50kb[idx],'^r', label = 'Frags. Over 50kb')
    ax1.plot([idx,idx],[len(re.under1kb),re.nfrags],'-k')
    ax1.plot(idx,re.nfrags,'^r', markersize=5,label = 'a')
    ax1.plot(idx,len(re.under1kb),'_b', label = 'a')
ax1.set_yscale('symlog')
ax1.set_xlabel('Combination Number')
ax1.set_ylabel('Number of Fragments')
ax1.xaxis.set_ticks(np.arange(0, 12, 2))
#ax1.xaxis.set_major_formatter(ticker.FormatStrFormatter('%0.0f'))
    
    
    
ax0.plot([0,len(NEBHF_class)],[465,465],'g',linestyle = 'dotted',label='465') # red needs to be above this line
ax0.plot([0,len(NEBHF_class)],[90,93],'r',linestyle = 'dotted',label = '90') # red needs to be above this line
ax0.plot([0,len(NEBHF_class)],[20,20],'b',linestyle = 'dotted',label = '20') # blue needs to be below this line
ax1.plot([0,len(NEBHF_2class)],[465,465],'g',linestyle = 'dotted',label ='465') # red needs to be above this line
ax1.plot([0,len(NEBHF_2class)],[90,93],'r',linestyle = 'dotted', label='90') # red needs to be above this line
ax1.plot([0,len(NEBHF_2class)],[20,20],'b',linestyle = 'dotted',label='20') # blue needs to be below this line


plotRE(8,ax2,'C.')
plotRE(18,ax3,'D.')
plotRE2(1,ax4,'E.')
plotRE2(9,ax5,'F.')


# Get legend from ax0 and then plot it in axleg
h, l = ax0.get_legend_handles_labels()
h2 = h[:2] + h[-3:] # process handles to just be the entries desired
axleg0.legend(h2,['Total\nFragments','Fragments\n< 1kb','465\nFragments','93\nFragments ','20\nFragments'], loc = 'center left')
axleg0.axis('off')

# Get legend from ax2 and then plot it in axleg
h, l = ax2.get_legend_handles_labels()
axleg1.legend(h[::-1],['MG1655\nGenome','Linear Fit'], loc = 'center left')
axleg1.axis('off')
    

ax0.annotate('A.',(0,1.05),xycoords = 'axes fraction',
            fontsize = 9, fontweight = 'bold')
ax1.annotate('B.',(0,1.05),xycoords = 'axes fraction',
            fontsize = 9, fontweight = 'bold')

fig.align_ylabels([ax0,ax2,ax4])
fig.align_ylabels([ax1,ax3,ax5])
    

for i in [8,18,5,33]:
    print(NEBHF_class[i].name)


plt.savefig('filepath/Lab 2 - Keyser/MPhil Thesis/6.5-Cutting.png',dpi=300)

    



    
